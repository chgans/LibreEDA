import qbs 1.0

Project {
    minimumQbsVersion: "1.5"
    property string leda_version_major: '0'
    property string leda_version_minor: '0'
    property string leda_version_release: '0'
    property string leda_version: leda_version_major + '.' + leda_version_minor + '.' + leda_version_release
    property string leda_compat_version_major: '0'
    property string leda_compat_version_minor: '0'
    property string leda_compat_version_release: '0'
    property string leda_compat_version: leda_compat_version_major + '.' + leda_compat_version_minor + '.' + leda_compat_version_release
    property pathList additionalPlugins: []
    property pathList additionalLibs: []
    property pathList additionalTools: []
    property pathList additionalAutotests: []


    property path leda_source_path: path

    property string leda_app_target: {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA";
        if (qbs.targetOS.contains("windows"))
            return "libreeda";
        if (qbs.targetOS.contains("linux"))
            return "libreeda";
        return ""
    }

    property string leda_app_path: {
        if (qbs.targetOS.contains("osx"))
            return "";
        if (qbs.targetOS.contains("windows"))
            return "bin"
        if (qbs.targetOS.contains("linux"))
            return "bin"
        return ""
    }

    property string leda_relative_root_path: {
        if (qbs.targetOS.contains("osx"))
            return "../../..";
        if (qbs.targetOS.contains("windows"))
            return ".."
        if (qbs.targetOS.contains("linux"))
            return ".."
        return ""
    }

    property string leda_bin_path: {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA.app/Contents/MacOS"
        if (qbs.targetOS.contains("windows"))
            return "bin"
        if (qbs.targetOS.contains("linux"))
            return "bin"
        return ""
    }

    property string leda_libexec_path: {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA.app/Contents/Resources"
        if (qbs.targetOS.contains("windows"))
            return "bin"
        if (qbs.targetOS.contains("linux"))
            return "libexec/leda"
        return ""
    }

    property string leda_library_path:  {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA.app/Contents/Frameworks"
        if (qbs.targetOS.contains("windows"))
            return "bin"
        if (qbs.targetOS.contains("linux"))
            return "lib/leda"
        return ""
    }

    property string leda_plugin_path:  {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA.app/Contents/PlugIns"
        if (qbs.targetOS.contains("windows"))
            return "lib/leda/plugins"
        if (qbs.targetOS.contains("linux"))
            return "lib/leda/plugins"
        return ""
    }

    property string leda_data_path: {
        if (qbs.targetOS.contains("osx"))
            return "Libre EDA.app/Contents/Resources"
        if (qbs.targetOS.contains("windows"))
            return "share/leda"
        if (qbs.targetOS.contains("linux"))
            return "share/leda"
        return "";
    }

    property string leda_doc_path: {
        if (qbs.targetOS.contains("osx"))
            return leda_data_path + "/doc"
        if (qbs.targetOS.contains("windows"))
            return "share/doc/leda"
        if (qbs.targetOS.contains("linux"))
            return "share/doc/leda"
        return ""
    }

    property stringList generalDefines: [
        "LIBRE_EDA",
    ]

    property bool withInstaller: qbs.buildVariant === "release"



    qbsSearchPaths: "qbs"

    references: [
        //"doc/doc.qbs",
        "src/src.qbs",
        "installer/installer.qbs",
        "installer/qtredistributable.qbs",
        "data/data.qbs",
        "ci/ci.qbs",
        "tests/tests.qbs"
    ]
}
