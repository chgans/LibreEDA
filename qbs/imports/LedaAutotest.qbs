import qbs
import qbs.FileInfo

LedaProduct {
    type: ["application", "autotest"]

    Depends { name: "Qt.test" }

    targetName: "tst_" + name.split(' ').join("")
    destinationDirectory: project.leda_bin_path
    installDir: project.leda_bin_path

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: installDir
    }

}
