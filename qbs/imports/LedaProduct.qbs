import qbs 1.0
import LedaFunctions

Product {
    property string installDir

    Depends { name: "cpp" }
    cpp.defines: project.generalDefines
    cpp.cxxLanguageVersion: "c++11"
    cpp.linkerFlags: {
        var flags = [];
        if (qbs.buildVariant == "release" && (qbs.toolchain.contains("gcc") || qbs.toolchain.contains("mingw")))
            flags.push("-s");
        return flags;
    }

    // Force libstdc++ for clang on Linux (libc++ seems broken on Ubuntu 16.04)
    cpp.cxxStandardLibrary: (qbs.targetOS.contains("osx") && qbs.toolchain.contains("clang")) ? "libc++" : "libstdc++";

    Depends { name: "Qt.core" }

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: installDir
    }
}
