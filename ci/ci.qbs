Product {
    name: "CI Scripts"

    qbs.install: false

    files: [
        "autotest.sh",
        "build.sh",
        "../.gitlab-ci.yml",
        "../.travis.yml",
        "../astyle.sh",
        "../.astylerc",
        "../cppcheck.sh",
        "clean.sh",
        "create-installer.sh",
        "deploy.sh",
    ]
}
