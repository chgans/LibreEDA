# TODO: When branch is master, installer has to be deployed on download.libre-eda.org/snapshot/YYMMDD-HHMMSS/LibreEDA-Installer-...
# Plus have a 'latest' link

profile=$1
installRoot=installer/packages/leda/data

echo "** Installing project into packaging directory..."
qbs install release profile:$profile installRoot:$installRoot
qbs install --products QtRedistributable release profile:$profile installRoot:$installRoot

echo "** Preparing for intaller..."
case $profile in
    *lnx*gcc*64*)
        host="linux-x64";
        ext=".run"
        config=config-linux.xml
        ;;
    *xcode*64*)
        host="mac-x64";
        ext=".dmg"
        config=config-mac.xml
        echo "** Copying Qt frameworks..."
        # Hack: can't get this done easily within Qbs
        libPath=$(qbs config profiles.$profile.Qt.core.libPath | cut -d'"' -f2)
        for lib in Core Gui Widgets Network PrintSupport Svg Test OpenGL Xml DBus;
        do
            framework=Qt${lib}.framework
            destdir="$installRoot/Libre EDA.app/Contents/Frameworks"
            cp -aR $libPath/$framework  "$destdir"
            rm -Rf "$destdir/$framework"/{,Versions/Current/}{Headers,*_debug,*.prl}
        done
	;;
    *msvc*64*)
        host="windows-x64";
	ext=".exe"
        config=config-windows.xml
	;;
    *)
        echo "Unsupported profile \"$profile\""
        return 1;
	;;
esac
installer=libre-eda-${host}${ext}

echo "** Creating installer..."
binarycreator -c installer/config/$config -p installer/packages/ $installer

echo "** Done:"
ls -lrth $installer
