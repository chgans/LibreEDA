
# Required for Core path test suite
qbs install release profile:$1

# 'Building' the autotest-runner product will run the autotests
qbs build -p autotest-runner release profile:$1
