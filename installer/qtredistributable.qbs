// FIXME: We should rely on global variables for path
// FIXME: Find a better way to deal with all of these,
//        or wait for QBS to have proper QtIFW support...

import LedaFunctions

Product {
    name: "QtRedistributable";
    condition: project.withInstaller
    builtByDefault: false
    qbs.install: false


    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport", "test"] }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Linux, amd64, gcc
    ////////////////////////////////////////////////////////////////////////////////////////////

    property bool isLinuxGcc: qbs.targetOS.contains("linux") && qbs.toolchain.contains("gcc");
    property bool hasEGL: LedaFunctions.versionIsAtLeast(Qt.core.version, "5.6.0")

    Group {
        name: "Qt Linux configuration"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "bin"
        files: [
            "linux-support/qt.conf"
        ]
    }

    Group {
        name: "Qt Linux libraries"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/lib"
        files: [
            Qt.core.libPath + "/" + "libicudata.so*",
            Qt.core.libPath + "/" + "libicui18n.so*",
            Qt.core.libPath + "/" + "libicuuc.so*",
            Qt.core.libPath + "/" + "libQt5Core.so*",
            Qt.core.libPath + "/" + "libQt5Gui.so*",
            Qt.core.libPath + "/" + "libQt5Widgets.so*",
            Qt.core.libPath + "/" + "libQt5Network.so*",
            Qt.core.libPath + "/" + "libQt5PrintSupport.so*",
            Qt.core.libPath + "/" + "libQt5DBus.so*",
            Qt.core.libPath + "/" + "libQt5OpenGL.so*",
            Qt.core.libPath + "/" + "libQt5CLucene.so*",
            Qt.core.libPath + "/" + "libQt5Test.so*",
            Qt.core.libPath + "/" + "libQt5SVG.so*",
            // Linux only
            Qt.core.libPath + "/" + "libQt5X11Extras.so*",
            Qt.core.libPath + "/" + "libQt5SVG.so*",
            Qt.core.libPath + "/" + "libQt5EGLDeviceIntegration.so*",
            Qt.core.libPath + "/" + "libQt5XcbQpa.so*",
        ]
    }

    Group {
        name: "Qt Linux platform plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/platforms"
        files: [
            Qt.core.pluginPath + "/platforms/" + "libqxcb.so",
            Qt.core.pluginPath + "/platforms/" + "libqlinuxfb.so",
            Qt.core.pluginPath + "/platforms/" + "libqminimal.so",
            Qt.core.pluginPath + "/platforms/" + "libqoffscreen.so"
        ]
    }

    Group {
        name: "Qt Linux EGL platform plugins"
        condition: isLinuxGcc && hasEGL
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/platforms"
        files: [
            Qt.core.pluginPath + "/platforms/" + "libqeglfs.so",
            Qt.core.pluginPath + "/platforms/" + "libqminimalegl.so",
        ]
    }

    Group {
        name: "Qt Linux XCB/GLX integration plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/xcbglintegrations"
        files: [
            Qt.core.pluginPath + "/xcbglintegrations/" + "libqxcb-glx-integration.so"
        ]
    }

    Group {
        name: "Qt Linux XCB/EGL integration plugins"
        condition: isLinuxGcc && hasEGL
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/xcbglintegrations"
        files: [
            Qt.core.pluginPath + "/xcbglintegrations/" + "libqxcb-egl-integration.so"
        ]
    }

    Group {
        name: "Qt Linux platform input context plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/platforminputcontexts"
        files: [
            Qt.core.pluginPath + "/platforminputcontexts/" + "libcomposeplatforminputcontextplugin.so",
            Qt.core.pluginPath + "/platforminputcontexts/" + "libibusplatforminputcontextplugin.so",
        ]
    }

    Group {
        name: "Qt Linux image format plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/imageformats"
        files: [
            Qt.core.pluginPath + "/imageformats/" + "libqdds.so",
            Qt.core.pluginPath + "/imageformats/" + "libqgif.so",
            Qt.core.pluginPath + "/imageformats/" + "libqicns.so",
            Qt.core.pluginPath + "/imageformats/" + "libqico.so",
            Qt.core.pluginPath + "/imageformats/" + "libqjp2.so",
            Qt.core.pluginPath + "/imageformats/" + "libqjpeg.so",
            Qt.core.pluginPath + "/imageformats/" + "libqsvg.so",
            Qt.core.pluginPath + "/imageformats/" + "libqtga.so",
            Qt.core.pluginPath + "/imageformats/" + "libqtiff.so",
            Qt.core.pluginPath + "/imageformats/" + "libqwbmp.so",
            Qt.core.pluginPath + "/imageformats/" + "libqwebp.so"
        ]
    }

    Group {
        name: "Qt Linux icon engine plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/iconengines"
        files: [
            Qt.core.pluginPath + "/iconengines/" + "libqsvgicon.so",
        ]
    }

    Group {
        name: "Qt Linux printer support plugins"
        condition: isLinuxGcc
        qbs.install: true
        qbs.installDir: "lib/Qt/plugins/printsupport"
        files: [
            Qt.core.pluginPath + "/printsupport/" + "libcupsprintersupport.so",
        ]
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MacOS, amd64, XCode 7.3
    ////////////////////////////////////////////////////////////////////////////////////////////

    property bool isMacXCode: qbs.targetOS.contains("osx") && qbs.toolchain.contains("clang");

    Group {
        name: "Qt MacOSX configuration"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_data_path
        files: [
            "macosx-support/qt.conf"
        ]
    }

    // cp -Ra Qt.core.libPath + "/" + Qt{Core,Gui,Widgets,Network,PrintSupport,Svg,Test,OpenGL,Xml}.framework leda_library_path
    Group {
        name: "Qt MacOSX libraries"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_library_path
//        files: [ "QtCore.framework/", "QtGui.framework/**", "QtWidgets.framework/**", "QtNetwork.framework/**",
//                 "QtPrintSupport.framework/**", "QtSvg.framework/**", "QtTest.framework/**", "QtOpenGL.framework/**",
//                 "QtXml.framework/**", "QtDBus.framework/**"
//        ]
//        excludeFiles: [ "**/Headers/**", "**/*.prl", "**/*_debug" ]
//        prefix: Qt.core.libPath + "/"
    }

    Group {
        name: "Qt MacOSX platform plugins"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_plugin_path + "/platforms/"
        files: [ "*.dylib" ]
        excludeFiles: [ "*_debug.dylib" ]
        prefix: Qt.core.pluginPath + "/platforms/"
    }

    Group {
        name: "Qt MacOSX icon engine plugins"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_plugin_path + "/iconengines/"
        files: [ "*.dylib" ]
        excludeFiles: [ "*_debug.dylib" ]
        prefix: Qt.core.pluginPath + "/iconengines/"
    }

    Group {
        name: "Qt MacOSX image format plugins"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_plugin_path + "/imageformats/"
        files: [ "*.dylib" ]
        excludeFiles: [ "*_debug.dylib" ]
        prefix: Qt.core.pluginPath + "/imageformats/"
    }

    Group {
        name: "Qt MacOSX print support plugins"
        condition: isMacXCode
        qbs.install: true
        qbs.installDir: project.leda_plugin_path + "/printsupport/"
        files: [ "*.dylib" ]
        excludeFiles: [ "*_debug.dylib" ]
        prefix: Qt.core.pluginPath + "/printsupport/"
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Windows, amd64, MSVC 2013
    ////////////////////////////////////////////////////////////////////////////////////////////

    property bool isWindowsMsvc: qbs.targetOS.contains("windows") && qbs.toolchain.contains("msvc");

    Group {
        name: "Qt Windows configuration"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin"
        files: [
            "windows-support/qt.conf",
        ]
    }

    Group {
        name: "Qt Windows libraries"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin"
        files: [
            Qt.core.binPath + "/" + "d3dcompiler_47.dll",
            Qt.core.binPath + "/" + "libEGL.dll",
            Qt.core.binPath + "/" + "libGLESv2.dll",
            Qt.core.binPath + "/" + "opengl32sw.dll",
            Qt.core.binPath + "/" + "Qt5Core.dll",
            Qt.core.binPath + "/" + "Qt5Gui.dll",
            Qt.core.binPath + "/" + "Qt5Widgets.dll",
            Qt.core.binPath + "/" + "Qt5Network.dll",
            Qt.core.binPath + "/" + "Qt5PrintSupport.dll",
            Qt.core.binPath + "/" + "Qt5DBus.dll",
            Qt.core.binPath + "/" + "Qt5OpenGL.dll",
            Qt.core.binPath + "/" + "Qt5CLucene.dll",
            Qt.core.binPath + "/" + "Qt5Test.dll",
            Qt.core.binPath + "/" + "Qt5SVG.dll",
        ]
    }

    Group {
        name: "Qt Windows platform plugins"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin/plugins/platforms"
        files: [
            Qt.core.pluginPath + "/platforms/" + "qwindows.dll",
            Qt.core.pluginPath + "/platforms/" + "qminimal.dll",
            Qt.core.pluginPath + "/platforms/" + "qoffscreen.dll"
        ]
    }

    Group {
        name: "Qt Windows image format plugins"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin/plugins/imageformats"
        files: [
            Qt.core.pluginPath + "/imageformats/" + "qdds.dll",
            Qt.core.pluginPath + "/imageformats/" + "qgif.dll",
            Qt.core.pluginPath + "/imageformats/" + "qicns.dll",
            Qt.core.pluginPath + "/imageformats/" + "qico.dll",
            Qt.core.pluginPath + "/imageformats/" + "qjpeg.dll",
            Qt.core.pluginPath + "/imageformats/" + "qsvg.dll",
            Qt.core.pluginPath + "/imageformats/" + "qtga.dll",
            Qt.core.pluginPath + "/imageformats/" + "qtiff.dll",
            Qt.core.pluginPath + "/imageformats/" + "qwbmp.dll",
            Qt.core.pluginPath + "/imageformats/" + "qwebp.dll"
        ]
    }

    Group {
        name: "Qt Windows icon engine plugins"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin/plugins/iconengines"
        files: [
            Qt.core.pluginPath + "/iconengines/" + "qsvgicon.dll",
        ]
    }

    Group {
        name: "Qt Windows printer support plugins"
        condition: isWindowsMsvc
        qbs.install: true
        qbs.installDir: "bin/plugins/printsupport"
        files: [
            Qt.core.pluginPath + "/printsupport/" + "windowsprintersupport.dll",
        ]
    }
}
