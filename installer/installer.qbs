import LedaFunctions

Product {
    name: "Installer";
    condition: project.withInstaller
    qbs.install: false
    builtByDefault: false;
    Depends { name: "QtRedistributable" }

    files: [
        "config/banner.png",
        "config/config-linux.xml",
        "config/config-mac.xml",
        "config/config-windows.xml",
        "config/icon.ico",
        "config/logo.png",
        "config/watermark.png",
        "packages/leda/meta/gpl-3.0.txt",
        "packages/leda/meta/package.xml",
    ]
}
