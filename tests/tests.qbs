import qbs

Project {
    name: "Autotests"
    references: [
        "SymbolEditor/SymbolEditor.qbs",
        "Core/Core.qbs",
        "LeGerber/LeGerber.qbs",
        "LeIpc7351/LeIpc7351.qbs",
    ].concat(project.additionalAutotests)

    LedaAutotestRunner {

        Depends { name: "Qt.core" }
        Depends { name: "Data" }

        // TODO: check if QT_PLUGIN_PATH is still required, should be Linux only
        // fixPath: windows ? "PATH=" ... : linux ? "QT_PLUGIN_PATH=" ... : MacOS specifics...

        // TODO: use arguments: ["-platform", "offscreen"]
        // TODO: more arguments to get test results parsed by external tools

        // "QT_DEBUG_PLUGINS=1", // Qt plugins
        // "LD_DEBUG=all", // MacOSX
        // "DYLD_PRINT_RPATHS=1", // MacOSX

        //"DYLD_FRAMEWORK_PATH=" + Qt.core.libPath + ":" + "/Users/MacMini/Projects/LibreEDA/qtc_Desktop__1f2d618d-release/install-root/Libre EDA.app/Contents/Frameworks/", // + qbs.installRoot + "/" + leda_library_path,
        //"DYLD_LIBRARY_PATH=" + Qt.core.libPath + ":" + "/Users/MacMini/Projects/LibreEDA/qtc_Desktop__1f2d618d-release/install-root/Libre EDA.app/Contents/Frameworks/",

        // Windows: Gross hack for now (autotest-runner runs test in their own bin dir, that doesn't contain Qt libs
        // FIXME: With this technique, we have to source the ci script so that it can access the PATH
        // Can we do some "set PATH=..." instead?
        property string fixWindowsPath: qbs.targetOS.contains("windows") ? "PATH=" + Qt.core.binPath.replace("/", "\\") + ";%PATH%" : ""

        environment: base.concat([// For windows
                                  fixWindowsPath,
                                  "DYLD_FRAMEWORK_PATH=" + Qt.core.libPath + ":" + qbs.installRoot + "/" + leda_library_path,
                                  "DYLD_LIBRARY_PATH=" + Qt.core.libPath + ":" + qbs.installRoot + "/" + leda_library_path,
                                  // Needed to find Qt plugins on the build machine (Linux)
                                  "QT_PLUGIN_PATH=" + Qt.core.pluginPath,
                                  // Allow to run on headless servers
                                  "QT_QPA_PLATFORM=offscreen",
                                 ])
    }
}
