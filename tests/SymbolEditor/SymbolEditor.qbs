import qbs

Project {
    name: "Autotests.SymbolEditor"

    references: [
        "symbol/symbol.qbs",
        "reader/reader.qbs",
        "writer/writer.qbs",
        "document/document.qbs",
    ]
}
