import qbs

LedaAutotest {
    name: "Autotests.SymbolEditor.Document"

    property path pluginDir: project.leda_source_path + "/src/plugins/symboleditor"

    cpp.includePaths: base.concat(pluginDir)

    Depends { name: "Qt.widgets" }
    Depends { name: "Core" }
    Depends { name: "Xdl" }

    files: [
        "tst_document.cpp",
    ]

    Group {
        name: "Plugin sources"

        files: [
            "document.h",
            "document.cpp",
            "command/additemcommand.cpp",
            "command/additemcommand.h",
            "command/aligncommand.cpp",
            "command/aligncommand.h",
            "command/clonecommand.cpp",
            "command/clonecommand.h",
            "command/command.cpp",
            "command/command.h",
            "command/distributecommand.cpp",
            "command/distributecommand.h",
            "command/movecommand.cpp",
            "command/movecommand.h",
            "command/setpropertycommand.cpp",
            "command/setpropertycommand.h",
            "command/stackordercommand.cpp",
            "command/stackordercommand.h",
        ]

        prefix: pluginDir + "/"
    }
}
