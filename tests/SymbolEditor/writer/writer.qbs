import qbs

LedaAutotest {
    name: "Autotests.SymbolEditor.XdlSymbolWriter"

    Depends { name: "Xdl" }
    Depends { name: "Qt"; submodules: ["gui"] }

    files: [
        "tst_writer.cpp",
    ]
}
