#include "xdl/symbolreader.h"
#include "xdl/symbol.h"

#include <QtTest>

using namespace SymbolEditor;

class tst_Reader : public QObject
{
    Q_OBJECT

private slots:
    void emptyDocument();
    void symbolProperties();
    void symbolPropertiesUtf8();
    void symbolIgnoresUnknownProperties();
    void circleProperties();
    void circularArcProperties();
    void ellipseProperties();
    void ellipticalArcProperties();
    void rectangleProperties();
    void polylineProperties();
    void polygonProperties();
    void pinProperties();
    void groupProperties();
    void labelProperties();

    void lineStyleProperty();
    void lineWidthProperty();
    void fillStyleProperty();
    void colorStyleProperty();
};

void tst_Reader::emptyDocument()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->name, QString(""));
    QCOMPARE(symbol->description, QString(""));
}

void tst_Reader::symbolProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<name>ANAME</name>"
                  "<label>ALABEL</label>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->name, QString("ANAME"));
    QCOMPARE(symbol->description, QString("ALABEL"));
}

void tst_Reader::symbolPropertiesUtf8()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<name>ANAME©</name>"
                  "<label>µæðϾ</label>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->name, QString("ANAME©"));
    QCOMPARE(symbol->description, QString("µæðϾ"));
}

void tst_Reader::symbolIgnoresUnknownProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<foo>ANAME©</foo>"
                  "<bar>µæðϾ</bar>"
                  "<foo>ANAME©</foo>"
                  "<bar>µæðϾ</bar>"
                  "<name>ANAME</name>"
                  "<foo>ANAME©</foo>"
                  "<bar>µæðϾ</bar>"
                  "<foo>ANAME©</foo>"
                  "<bar>µæðϾ</bar>"
                  "<label>ALABEL</label>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->name, QString("ANAME"));
    QCOMPARE(symbol->description, QString("ALABEL"));
}

// TODO: Move to common properties to itemProperties()
// but keep test for item specific default property values (eg. color)
void tst_Reader::circleProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circle>"
                  "</circle>"
                  "<circle>"
                  "<position><x>1.2</x><y>3.4</y></position>"
                  "<visible>false</visible>"
                  "<locked>true</locked>"
                  "<x-mirrored>true</x-mirrored>"
                  "<y-mirrored>true</y-mirrored>"
                  "<opacity>49.9</opacity>"
                  "<rotation>12.5</rotation>"
                  "<line-style>DashDot</line-style>"
                  "<line-width>Thinest</line-width>"
                  "<line-color>Red</line-color>"
                  "<fill-style>None</fill-style>"
                  "<fill-color>Violet</fill-color>"
                  "<radius>3.14</radius>"
                  "</circle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), Circle);
    QCOMPARE(item1->position(), QPointF(0, 0));
    QCOMPARE(item1->isVisible(), true);
    QCOMPARE(item1->isLocked(), false);
    QCOMPARE(item1->isXMirrored(), false);
    QCOMPARE(item1->isYMirrored(), false);
    QCOMPARE(item1->opacity(), 100.0);
    QCOMPARE(item1->rotation(), 0.0);
    QCOMPARE(item1->lineStyle(), SolidLine);
    QCOMPARE(item1->lineWidth(), MediumLine);
    QCOMPARE(item1->lineColor(), SecondaryContent);
    QCOMPARE(item1->fillStyle(), SolidFill);
    QCOMPARE(item1->fillColor(), BackgroundHighlight);
    auto circle1 = static_cast<CircleItem*>(item1);
    QCOMPARE(circle1->radius(), 0.0);

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), Circle);
    QCOMPARE(item2->position(), QPointF(1.2, 3.4));
    QCOMPARE(item2->isVisible(), false);
    QCOMPARE(item2->isLocked(), true);
    QCOMPARE(item2->isXMirrored(), true);
    QCOMPARE(item2->isYMirrored(), true);
    QCOMPARE(item2->opacity(), 49.9);
    QCOMPARE(item2->rotation(), 12.5);
    QCOMPARE(item2->lineStyle(), DashDotLine);
    QCOMPARE(item2->lineWidth(), ThinestLine);
    QCOMPARE(item2->lineColor(), Red);
    QCOMPARE(item2->fillStyle(), NoFill);
    QCOMPARE(item2->fillColor(), Violet);
    auto circle2 = static_cast<CircleItem*>(item2);
    QCOMPARE(circle2->radius(), 3.14);
}

void tst_Reader::circularArcProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circular-arc>"
                  "</circular-arc>"
                  "<circular-arc>"
                  "<radius>3.14</radius>"
                  "<start-angle>12.34</start-angle>"
                  "<span-angle>99.999</span-angle>"
                  "</circular-arc>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), CircularArc);
    auto arc1 = static_cast<CircularArcItem*>(item1);
    QCOMPARE(arc1->radius(), 0.0);
    QCOMPARE(arc1->startAngle(), 0.0);
    QCOMPARE(arc1->spanAngle(), 360.0);

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), CircularArc);
    auto arc2 = static_cast<CircularArcItem*>(item2);
    QCOMPARE(arc2->radius(), 3.14);
    QCOMPARE(arc2->startAngle(), 12.34);
    QCOMPARE(arc2->spanAngle(), 99.999);
}

void tst_Reader::ellipseProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<ellipse>"
                  "</ellipse>"
                  "<ellipse>"
                  "<x-radius>3.14</x-radius>"
                  "<y-radius>6.28</y-radius>"
                  "</ellipse>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), Ellipse);
    auto arc1 = static_cast<EllipseItem*>(item1);
    QCOMPARE(arc1->xRadius(), 0.0);
    QCOMPARE(arc1->yRadius(), 0.0);

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), Ellipse);
    auto arc2 = static_cast<EllipseItem*>(item2);
    QCOMPARE(arc2->xRadius(), 3.14);
    QCOMPARE(arc2->yRadius(), 6.28);
}

void tst_Reader::ellipticalArcProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<elliptical-arc>"
                  "</elliptical-arc>"
                  "<elliptical-arc>"
                  "<x-radius>3.14</x-radius>"
                  "<y-radius>6.28</y-radius>"
                  "<start-angle>12.34</start-angle>"
                  "<span-angle>99.999</span-angle>"
                  "</elliptical-arc>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), EllipticalArc);
    auto arc1 = static_cast<EllipticalArcItem*>(item1);
    QCOMPARE(arc1->xRadius(), 0.0);
    QCOMPARE(arc1->yRadius(), 0.0);
    QCOMPARE(arc1->startAngle(), 0.0);
    QCOMPARE(arc1->spanAngle(), 360.0);

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), EllipticalArc);
    auto arc2 = static_cast<EllipticalArcItem*>(item2);
    QCOMPARE(arc2->xRadius(), 3.14);
    QCOMPARE(arc2->yRadius(), 6.28);
    QCOMPARE(arc2->startAngle(), 12.34);
    QCOMPARE(arc2->spanAngle(), 99.999);
}

void tst_Reader::rectangleProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<rectangle>"
                  "</rectangle>"
                  "<rectangle>"
                  "<width>3.14</width>"
                  "<height>6.28</height>"
                  "</rectangle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), Rectangle);
    auto rect1 = static_cast<RectangleItem*>(item1);
    QCOMPARE(rect1->width(), 0.0);
    QCOMPARE(rect1->height(), 0.0);

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), Rectangle);
    auto rect2 = static_cast<RectangleItem*>(item2);
    QCOMPARE(rect2->width(), 3.14);
    QCOMPARE(rect2->height(), 6.28);
}

void tst_Reader::polylineProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<polyline>"
                  "</polyline>"
                  "<polyline>"
                  "<vertices>"
                  "<point><x>1.1</x><y>1.2</y></point>"
                  "<point><x>2.1</x><y>2.2</y></point>"
                  "<point><x>3.1</x><y>3.2</y></point>"
                  "</vertices>"
                  "</polyline>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), Polyline);
    auto poly1 = static_cast<PolylineItem*>(item1);
    QCOMPARE(poly1->vertices(), QList<QPointF>());

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), Polyline);
    auto poly2 = static_cast<PolylineItem*>(item2);
    QCOMPARE(poly2->vertices(), QList<QPointF>() << QPointF(1.1, 1.2) << QPointF(2.1, 2.2) << QPointF(3.1, 3.2));
}

void tst_Reader::polygonProperties()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<polygon>"
                  "</polygon>"
                  "<polygon>"
                  "<vertices>"
                  "<point><x>1.1</x><y>1.2</y></point>"
                  "<point><x>2.1</x><y>2.2</y></point>"
                  "<point><x>3.1</x><y>3.2</y></point>"
                  "</vertices>"
                  "</polygon>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 2);

    auto item1 = symbol->drawingItems.value(0);
    QCOMPARE(item1->type(), Polygon);
    auto poly1 = static_cast<PolygonItem*>(item1);
    QCOMPARE(poly1->vertices(), QList<QPointF>());

    auto item2 = symbol->drawingItems.value(1);
    QCOMPARE(item2->type(), Polygon);
    auto poly2 = static_cast<PolygonItem*>(item2);
    QCOMPARE(poly2->vertices(), QList<QPointF>() << QPointF(1.1, 1.2) << QPointF(2.1, 2.2) << QPointF(3.1, 3.2));
}

void tst_Reader::pinProperties()
{

}

void tst_Reader::groupProperties()
{

}

void tst_Reader::labelProperties()
{

}

void tst_Reader::lineStyleProperty()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circle>"
                  "<line-style>None</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>Solid</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>Dash</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>Dot</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>DashDot</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>DashDotDot</line-style>"
                  "</circle>"
                  "<circle>"
                  "<line-style>FooBar</line-style>"
                  "</circle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 7);

    QCOMPARE(symbol->drawingItems.value(0)->lineStyle(), NoLine);
    QCOMPARE(symbol->drawingItems.value(1)->lineStyle(), SolidLine);
    QCOMPARE(symbol->drawingItems.value(2)->lineStyle(), DashLine);
    QCOMPARE(symbol->drawingItems.value(3)->lineStyle(), DotLine);
    QCOMPARE(symbol->drawingItems.value(4)->lineStyle(), DashDotLine);
    QCOMPARE(symbol->drawingItems.value(5)->lineStyle(), DashDotDotLine);
    QCOMPARE(symbol->drawingItems.value(6)->lineStyle(), SolidLine);
}

void tst_Reader::lineWidthProperty()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circle>"
                  "<line-width>Thinest</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Thiner</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Thin</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>SlightlyThin</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Medium</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>SlightlyThick</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Thick</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Thicker</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>Thickest</line-width>"
                  "</circle>"
                  "<circle>"
                  "<line-width>FooBar</line-width>"
                  "</circle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 10);

    QCOMPARE(symbol->drawingItems.value(0)->lineWidth(), ThinestLine);
    QCOMPARE(symbol->drawingItems.value(1)->lineWidth(), ThinerLine);
    QCOMPARE(symbol->drawingItems.value(2)->lineWidth(), ThinLine);
    QCOMPARE(symbol->drawingItems.value(3)->lineWidth(), SlightlyThinLine);
    QCOMPARE(symbol->drawingItems.value(4)->lineWidth(), MediumLine);
    QCOMPARE(symbol->drawingItems.value(5)->lineWidth(), SlightlyThickLine);
    QCOMPARE(symbol->drawingItems.value(6)->lineWidth(), ThickLine);
    QCOMPARE(symbol->drawingItems.value(7)->lineWidth(), ThickerLine);
    QCOMPARE(symbol->drawingItems.value(8)->lineWidth(), ThickestLine);
    QCOMPARE(symbol->drawingItems.value(9)->lineWidth(), MediumLine);
}

void tst_Reader::fillStyleProperty()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circle>"
                  "<fill-style>None</fill-style>"
                  "</circle>"
                  "<circle>"
                  "<fill-style>Solid</fill-style>"
                  "</circle>"
                  "<circle>"
                  "<fill-style>FooBar</fill-style>"
                  "</circle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 3);

    QCOMPARE(symbol->drawingItems.value(0)->fillStyle(), NoFill);
    QCOMPARE(symbol->drawingItems.value(1)->fillStyle(), SolidFill);
    QCOMPARE(symbol->drawingItems.value(2)->fillStyle(), SolidFill);
}

void tst_Reader::colorStyleProperty()
{
    QString input("<?xml version='1.0' encoding='UTF-8'?>"
                  "<symbol xmlns='http://www.leda.org/xdl'>"
                  "<drawing>"
                  "<circle>"
                  "<fill-color>EmphasisedContent</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>PrimaryContent</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>SecondaryContent</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>BackgroundHighlight</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Background</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Yellow</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Orange</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Red</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Magenta</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Violet</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Blue</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Cyan</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>Green</fill-color>"
                  "</circle>"
                  "<circle>"
                  "<fill-color>FooBar</fill-color>"
                  "<line-color>FooBar</line-color>"
                  "</circle>"
                  "</drawing>"
                  "</symbol>"
                  );
    QByteArray bytes = input.toUtf8();
    QBuffer buffer(&bytes);
    buffer.open(QBuffer::ReadOnly);

    Reader reader;
    QCOMPARE(reader.read(&buffer), true);

    Symbol *symbol = reader.symbol();
    QVERIFY(symbol != nullptr);
    QCOMPARE(symbol->drawingItems.count(), 14);

    QCOMPARE(symbol->drawingItems.value(0)->fillColor(), EmphasisedContent);
    QCOMPARE(symbol->drawingItems.value(1)->fillColor(), PrimaryContent);
    QCOMPARE(symbol->drawingItems.value(2)->fillColor(), SecondaryContent);
    QCOMPARE(symbol->drawingItems.value(3)->fillColor(), BackgroundHighlight);
    QCOMPARE(symbol->drawingItems.value(4)->fillColor(), Background);
    QCOMPARE(symbol->drawingItems.value(5)->fillColor(), Yellow);
    QCOMPARE(symbol->drawingItems.value(6)->fillColor(), Orange);
    QCOMPARE(symbol->drawingItems.value(7)->fillColor(), Red);
    QCOMPARE(symbol->drawingItems.value(8)->fillColor(), Magenta);
    QCOMPARE(symbol->drawingItems.value(9)->fillColor(), Violet);
    QCOMPARE(symbol->drawingItems.value(10)->fillColor(), Blue);
    QCOMPARE(symbol->drawingItems.value(11)->fillColor(), Cyan);
    QCOMPARE(symbol->drawingItems.value(12)->fillColor(), Green);
    QCOMPARE(symbol->drawingItems.value(13)->fillColor(), BackgroundHighlight);
    QCOMPARE(symbol->drawingItems.value(13)->lineColor(), SecondaryContent);
}

QTEST_MAIN(tst_Reader)

#include "tst_reader.moc"
