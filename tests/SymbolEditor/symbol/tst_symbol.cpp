
#include "xdl/symbol.h"

#include <QtTest>

using namespace SymbolEditor;

class tst_Symbol : public QObject
{
    Q_OBJECT

public:
    tst_Symbol();
    virtual ~tst_Symbol();

private slots:
    void symbolProperties();

    void circleProperties();
    void circularArcProperties();
    void ellipseProperties();
    void ellipticalArcProperties();
    void rectangleProperties();
    void polylineProperties();
    void polygonProperties();
    void pinProperties();
    void groupProperties();
    void labelProperties();

    void testBooleanProperties();
    void testRealProperties();

    void testColorProperty();
    void testLineStyleProperty();
    void testLineWidthProperty();
    void testFillStyleProperty();
};


tst_Symbol::tst_Symbol()
{

}

tst_Symbol::~tst_Symbol()
{

}

void tst_Symbol::symbolProperties()
{
    auto symbol = new Symbol();
    QVERIFY(symbol->name == QString());
    QVERIFY(symbol->description == QString());
    QVERIFY(symbol->drawingItems.count() == 0);
}

void tst_Symbol::circleProperties()
{
    auto item = new CircleItem();

    QCOMPARE(1 + item->radius(), 1.0);
    item->setRadius(2.3);
    QCOMPARE(item->radius(), 2.3);
    item->setProperty(RadiusProperty, 3.1);
    QCOMPARE(item->radius(), 3.1);
    QCOMPARE(item->property(RadiusProperty).toReal(), 3.1);
}

void tst_Symbol::circularArcProperties()
{
    auto item = new CircularArcItem();

    QCOMPARE(1 + item->radius(), 1.0);
    item->setRadius(2.3);
    QCOMPARE(item->radius(), 2.3);
    item->setProperty(RadiusProperty, 3.1);
    QCOMPARE(item->radius(), 3.1);
    QCOMPARE(item->property(RadiusProperty).toReal(), 3.1);

    QCOMPARE(1 + item->startAngle(), 1.0);
    item->setStartAngle(2.3);
    QCOMPARE(item->startAngle(), 2.3);
    item->setProperty(StartAngleProperty, 3.1);
    QCOMPARE(item->startAngle(), 3.1);
    QCOMPARE(item->property(StartAngleProperty).toReal(), 3.1);

    QCOMPARE(item->spanAngle(), 360.0);
    item->setSpanAngle(2.3);
    QCOMPARE(item->spanAngle(), 2.3);
    item->setProperty(SpanAngleProperty, 3.1);
    QCOMPARE(item->spanAngle(), 3.1);
    QCOMPARE(item->property(SpanAngleProperty).toReal(), 3.1);
}

void tst_Symbol::ellipseProperties()
{
    auto item = new EllipseItem();

    QCOMPARE(1 + item->xRadius(), 1.0);
    item->setXRadius(2.3);
    QCOMPARE(item->xRadius(), 2.3);
    item->setProperty(XRadiusProperty, 3.1);
    QCOMPARE(item->xRadius(), 3.1);
    QCOMPARE(item->property(XRadiusProperty).toReal(), 3.1);

    QCOMPARE(1 + item->yRadius(), 1.0);
    item->setYRadius(2.3);
    QCOMPARE(item->yRadius(), 2.3);
    item->setProperty(YRadiusProperty, 3.1);
    QCOMPARE(item->yRadius(), 3.1);
    QCOMPARE(item->property(YRadiusProperty).toReal(), 3.1);
}

void tst_Symbol::ellipticalArcProperties()
{
    auto item = new EllipticalArcItem();

    QCOMPARE(1 + item->xRadius(), 1.0);
    item->setXRadius(2.3);
    QCOMPARE(item->xRadius(), 2.3);
    item->setProperty(XRadiusProperty, 3.1);
    QCOMPARE(item->xRadius(), 3.1);
    QCOMPARE(item->property(XRadiusProperty).toReal(), 3.1);

    QCOMPARE(1 + item->yRadius(), 1.0);
    item->setYRadius(2.3);
    QCOMPARE(item->yRadius(), 2.3);
    item->setProperty(YRadiusProperty, 3.1);
    QCOMPARE(item->yRadius(), 3.1);
    QCOMPARE(item->property(YRadiusProperty).toReal(), 3.1);

    QCOMPARE(1 + item->startAngle(), 1.0);
    item->setStartAngle(2.3);
    QCOMPARE(item->startAngle(), 2.3);
    item->setProperty(StartAngleProperty, 3.1);
    QCOMPARE(item->startAngle(), 3.1);
    QCOMPARE(item->property(StartAngleProperty).toReal(), 3.1);

    QCOMPARE(item->spanAngle(), 360.0);
    item->setSpanAngle(2.3);
    QCOMPARE(item->spanAngle(), 2.3);
    item->setProperty(SpanAngleProperty, 3.1);
    QCOMPARE(item->spanAngle(), 3.1);
    QCOMPARE(item->property(SpanAngleProperty).toReal(), 3.1);
}

void tst_Symbol::rectangleProperties()
{
    auto item = new RectangleItem();

    QCOMPARE(1 + item->width(), 1.0);
    item->setWidth(2.3);
    QCOMPARE(item->width(), 2.3);
    item->setProperty(WidthProperty, 3.1);
    QCOMPARE(item->width(), 3.1);
    QCOMPARE(item->property(WidthProperty).toReal(), 3.1);

    QCOMPARE(1 + item->height(), 1.0);
    item->setHeight(2.3);
    QCOMPARE(item->height(), 2.3);
    item->setProperty(HeightProperty, 3.1);
    QCOMPARE(item->height(), 3.1);
    QCOMPARE(item->property(HeightProperty).toReal(), 3.1);
}

void tst_Symbol::polylineProperties()
{
    auto item = new PolylineItem();

    QList<QPointF> vertices;
    QCOMPARE(item->vertices(), vertices);

    vertices << QPointF(1, 2) << QPointF(3, 4);
    item->setVertices(vertices);
    QCOMPARE(item->vertices(), vertices);
    QCOMPARE(item->property(VerticesProperty), QVariant::fromValue(vertices));
}

void tst_Symbol::polygonProperties()
{
    auto item = new PolygonItem();

    QList<QPointF> vertices;
    QCOMPARE(item->vertices(), vertices);

    vertices << QPointF(1, 2) << QPointF(3, 4);
    item->setVertices(vertices);
    QCOMPARE(item->vertices(), vertices);
    QCOMPARE(item->property(VerticesProperty), QVariant::fromValue(vertices));
}

void tst_Symbol::pinProperties()
{
    //auto item = new PinItem();
}

void tst_Symbol::groupProperties()
{
    //auto item = new ItemGroup();
}

void tst_Symbol::labelProperties()
{
    //auto item = new LabelItem();

}

void tst_Symbol::testColorProperty()
{
    auto item = new RectangleItem();

    QCOMPARE(item->lineColor(), SecondaryContent);
    item->setLineColor(PrimaryContent);
    QCOMPARE(item->lineColor(), PrimaryContent);

    item->setProperty(LineColorProperty, QVariant::fromValue(Green));
    QCOMPARE(item->lineColor(), Green);
    QCOMPARE(item->property(LineColorProperty), QVariant::fromValue(Green));

    QCOMPARE(item->fillColor(), BackgroundHighlight);
    item->setFillColor(Green);
    QCOMPARE(item->fillColor(), Green);

    item->setProperty(FillColorProperty, QVariant::fromValue(Red));
    QCOMPARE(item->fillColor(), Red);
    QCOMPARE(item->property(FillColorProperty), QVariant::fromValue(Red));
}

void tst_Symbol::testLineStyleProperty()
{
    auto item = new RectangleItem();

    QCOMPARE(item->lineStyle(), SolidLine);
    item->setLineStyle(DashLine);
    QCOMPARE(item->lineStyle(), DashLine);

    item->setProperty(LineStyleProperty, QVariant::fromValue(DashDotDotLine));
    QCOMPARE(item->lineStyle(), DashDotDotLine);
    QCOMPARE(item->property(LineStyleProperty), QVariant::fromValue(DashDotDotLine));
}

void tst_Symbol::testLineWidthProperty()
{
    auto item = new CircularArcItem();

    QCOMPARE(item->lineWidth(), MediumLine);
    item->setLineWidth(ThinestLine);
    QCOMPARE(item->lineWidth(), ThinestLine);

    item->setProperty(LineWidthProperty, QVariant::fromValue(ThickestLine));
    QCOMPARE(item->lineWidth(), ThickestLine);
    QCOMPARE(item->property(LineWidthProperty), QVariant::fromValue(ThickestLine));

}

void tst_Symbol::testFillStyleProperty()
{
    auto item = new PolygonItem();

    QCOMPARE(item->fillStyle(), SolidFill);
    item->setFillStyle(NoFill);
    QCOMPARE(item->fillStyle(), NoFill);

    item->setProperty(FillStyleProperty, QVariant::fromValue(SolidFill));
    QCOMPARE(item->fillStyle(), SolidFill);
    QCOMPARE(item->property(FillStyleProperty), QVariant::fromValue(SolidFill));

}

void tst_Symbol::testBooleanProperties()
{
    auto item = new EllipseItem();

    QCOMPARE(item->position(), QPointF(0, 0));
    item->setPosition(QPointF(2.3, 3.1));
    QCOMPARE(item->position(), QPointF(2.3, 3.1));
    item->setProperty(PositionProperty, QPointF(2.3, 3.1));
    QCOMPARE(item->position(), QPointF(2.3, 3.1));
    QCOMPARE(item->property(PositionProperty).toPointF(), QPointF(2.3, 3.1));

    QCOMPARE(item->isVisible(), true);
    item->setVisible(false);
    QCOMPARE(item->isVisible(), false);
    item->setProperty(VisibilityProperty, true);
    QCOMPARE(item->isVisible(), true);
    QCOMPARE(item->property(VisibilityProperty).toBool(), true);

    QCOMPARE(item->isLocked(), false);
    item->setLocked(true);
    QCOMPARE(item->isLocked(), true);
    item->setProperty(LockedProperty, false);
    QCOMPARE(item->isLocked(), false);
    QCOMPARE(item->property(LockedProperty).toBool(), false);

    QCOMPARE(item->isXMirrored(), false);
    item->setXMirrored(true);
    QCOMPARE(item->isXMirrored(), true);
    item->setProperty(XMirroredProperty, false);
    QCOMPARE(item->isXMirrored(), false);
    QCOMPARE(item->property(XMirroredProperty).toBool(), false);

    QCOMPARE(item->isYMirrored(), false);
    item->setYMirrored(true);
    QCOMPARE(item->isYMirrored(), true);
    item->setProperty(YMirroredProperty, false);
    QCOMPARE(item->isYMirrored(), false);
    QCOMPARE(item->property(YMirroredProperty).toBool(), false);

    QCOMPARE(item->opacity(), 100.0);
    item->setOpacity(49.9);
    QCOMPARE(item->opacity(), 49.9);
    item->setProperty(OpacityProperty, 42.5);
    QCOMPARE(item->opacity(), 42.5);
    QCOMPARE(item->property(OpacityProperty).toReal(), 42.5);

    QCOMPARE(item->rotation(), 0.0);
    item->setRotation(49.9);
    QCOMPARE(item->rotation(), 49.9);
    item->setProperty(RotationProperty, 42.5);
    QCOMPARE(item->rotation(), 42.5);
    QCOMPARE(item->property(RotationProperty).toReal(), 42.5);
}

void tst_Symbol::testRealProperties()
{
    auto item = new PolylineItem();

    QCOMPARE(item->opacity(), 100.0);
    item->setOpacity(49.9);
    QCOMPARE(item->opacity(), 49.9);
    item->setProperty(OpacityProperty, 42.5);
    QCOMPARE(item->opacity(), 42.5);
    QCOMPARE(item->property(OpacityProperty).toReal(), 42.5);

    QCOMPARE(item->rotation(), 0.0);
    item->setRotation(49.9);
    QCOMPARE(item->rotation(), 49.9);
    item->setProperty(RotationProperty, 42.5);
    QCOMPARE(item->rotation(), 42.5);
    QCOMPARE(item->property(RotationProperty).toReal(), 42.5);
}

QTEST_MAIN(tst_Symbol)

#include "tst_symbol.moc"
