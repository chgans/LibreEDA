TEMPLATE = app

TARGET = test-ipc7351-document

CONFIG += console c++11
QT += widgets testlib

SOURCES += main.cpp \
    TestDocument.cpp \
    TestPadStack.cpp \
    TestDocumentObject.cpp \
    TestDocumentObjectListener.cpp \
    DocumentObjectMock.cpp \
    TestDocumentStreamWriter.cpp \
    TestDocumentStreamReader.cpp \
    DocumentObjectFactoryMock.cpp

HEADERS += \
    TestDocument.h \
    TestPadStack.h \
    TestDocumentObject.h \
    TestDocumentObjectListener.h \
    DocumentObjectMock.h \
    TestDocumentStreamWriter.h \
    TestDocumentStreamReader.h \
    DocumentObjectFactoryMock.h

include(../../src/lib/LeIpc7351/LeIpc7351.pri)
