#include <QtTest>

#include <iostream>

#include "DocumentObjectMock.h"

#include "TestDocument.h"
#include "TestDocumentObject.h"
#include "TestDocumentObjectListener.h"
#include "TestDocumentStreamReader.h"
#include "TestDocumentStreamWriter.h"
#include "TestPadStack.h"

int main(int argc, char *argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    qRegisterMetaType<DocumentObjectMock0*>();
    qRegisterMetaType<DocumentObjectMock1*>();
    qRegisterMetaType<DocumentObjectMock2*>();
    qRegisterMetaType<DocumentObjectMock3*>();
    qRegisterMetaType<DocumentObjectMock4*>();
    qRegisterMetaType<DocumentObjectMock1::MockEnum>();
    qRegisterMetaType<DocumentObjectMock1::MockFlags>();
    qRegisterMetaType<QList<QString>>();
    qRegisterMetaType<QList<int>>();
    qRegisterMetaType<QList<double>>();

    int failures = 0;

    {
        TestDocumentObject test;
        failures += QTest::qExec(&test);
    }
    {
        TestDocumentObjectListener test;
        failures += QTest::qExec(&test);
    }
    {
        TestDocument test;
        failures += QTest::qExec(&test);
    }
    {
        TestDocumentStreamWriter test;
        failures += QTest::qExec(&test);
    }
    {
        TestDocumentStreamReader test;
        failures += QTest::qExec(&test);
    }
    {
        TestPadStack test;
        failures += QTest::qExec(&test);
    }

    std::cout << std::endl;
    std::cout << "Total failures: " << failures;
    std::cout << std::endl;
    std::cout << std::endl;
    return failures;
}
