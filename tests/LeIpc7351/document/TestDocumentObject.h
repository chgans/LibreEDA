#ifndef TESTDOCUMENTOBJECT_H
#define TESTDOCUMENTOBJECT_H

#include <QObject>

class TestDocumentObject : public QObject
{
    Q_OBJECT

private slots:
    void testProperties();
};

#endif // TESTDOCUMENTOBJECT_H
