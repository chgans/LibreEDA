#ifndef TESTPADSTACK_H
#define TESTPADSTACK_H

#include <QObject>

class PadStackElement;

class TestPadStack : public QObject
{
    Q_OBJECT
public:
    explicit TestPadStack(QObject *parent = 0);

private slots:
    void testCreatePadStackElement();
    void testCreatePadStack();

private:
    PadStackElement *createElement();
};

#endif // TESTPADSTACK_H
