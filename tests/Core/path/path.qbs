import qbs

LedaAutotest {
    name: "Autotests.Core.Path"

    Depends { name: "Core" }

    files: [
        "tst_path.cpp",
    ]
}
