import qbs

LedaAutotest {
    name: "Autotests.LeGerber.Parser"

    Depends { name: "LeGerber" }

    files: [
        "parsertest.cpp",
        "parsertest.h",
    ]
}
