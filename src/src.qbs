import qbs

Project {
    name: "Sources"
    references: [
        "libs",
        "plugins",
        "apps",
        "thirdparty",
    ]
}
