#ifndef PACKAGEDETAILMODEL_H
#define PACKAGEDETAILMODEL_H

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Package;
}

class PackageDetailModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    PackageDetailModel(QObject *parent = nullptr);

    void setPackage(Ipc2581b::Package *package);

private:
    Ipc2581b::Package *m_package;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

#endif // PACKAGEDETAILMODEL_H