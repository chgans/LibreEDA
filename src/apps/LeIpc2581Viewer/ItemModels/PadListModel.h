#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Pad;
}

class PadListModelPrivate;
class PadListModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(PadListModel)
    QScopedPointer<PadListModelPrivate> const d_ptr;

public:
    enum
    {
        PadstackColumn = 0,
        UsageColumn,
        ComponentColumn,
        PinColumn,
        TransformColumn,
        LocationColumn,
        ColumnCount
    };

    explicit PadListModel(QObject *parent = 0);
    ~PadListModel();

    void addPad(const Ipc2581b::Pad *pad);

public slots:
    void clear();

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};
