#pragma once

#include <QAbstractTableModel>

namespace Ipc2581b
{
    class Package;
}

class ComponentListModel;
class Document;

class PackageListModelPrivate;
class PackageListModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_DECLARE_PRIVATE(PackageListModel)
    QScopedPointer<PackageListModelPrivate> const d_ptr;

    Q_DISABLE_COPY(PackageListModel)
    explicit PackageListModel(Document *document,
                              const QList<Ipc2581b::Package *> &packageList);
    void documentInitialised();
    friend class Document;

public:
    enum
    {
        NameColumn = 0,
        PackageTypeColumn,
        PinOneColumn,
        PinOneOrientationColumn,
        HeightColumn,
        PickupPointColumn,
        PinCountColumn,
        ColumnCount
    };
    enum
    {
        ComponentListModelRole = Qt::UserRole + 1,
        PinListModelRole,
        OutlineRole,
        LandPatternRole,
        SilkScreenRole,
        AssemblyDrawingRole
    };

    ~PackageListModel();

    QAbstractItemModel *components(int row) const;

private:

    // QAbstractTableModel Interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

};

Q_DECLARE_METATYPE(PackageListModel*)
