#include "PinDetailModel.h"
#include "enumtranslator.h"
#include "Pin.h"

enum
{
    PinNumberRow = 0,
    PinNameRow,
    PinCadTypeRow,
    PinElecTypeRow,
    PinMountTypeRow,
    PinLocationRow,
    RowCount
};

enum
{
    PropertyNameColumn = 0,
    PropertyValueColumn,
    ColumnCount
};

PinDetailModel::PinDetailModel(QObject *parent):
    QAbstractTableModel(parent),
    m_pin(nullptr)
{

}

void PinDetailModel::setPin(Ipc2581b::Pin *pin)
{
    beginResetModel();
    m_pin = pin;
    endResetModel();
}


int PinDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (m_pin != nullptr)
        return RowCount;
    return 0;
}

int PinDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant PinDetailModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    switch (index.column())
    {
        case PropertyNameColumn:
            switch (index.row())
            {
                case PinNumberRow:
                    return QStringLiteral("Number");
                case PinNameRow:
                    return QStringLiteral("Name");
                case PinCadTypeRow:
                    return QStringLiteral("CAD Type");
                case PinElecTypeRow:
                    return QStringLiteral("Elec/Mech Type");
                case PinMountTypeRow:
                    return QStringLiteral("Mounting Type");
                case PinLocationRow:
                    return QStringLiteral("Location");
                default:
                    return QVariant();
            }
        case PropertyValueColumn:
            switch (index.row())
            {
                case PinNumberRow:
                    return m_pin->number;
                case PinNameRow:
                    if (m_pin->nameOptional.hasValue)
                        return m_pin->nameOptional.value;
                    return QVariant();
                case PinCadTypeRow:
                    return Ipc2581b::EnumTranslator::cadPinTypeText(m_pin->type);
                case PinElecTypeRow:
                    if (m_pin->electricalTypeOptional.hasValue)
                        return Ipc2581b::EnumTranslator::electricalPinTypeText(m_pin->electricalTypeOptional.value);
                    return QVariant();
                case PinMountTypeRow:
                    if (m_pin->mountTypeOptional.hasValue)
                        return Ipc2581b::EnumTranslator::mountingPinTypeText(m_pin->mountTypeOptional.value);
                    return QVariant();
                case PinLocationRow:
                    if (m_pin->locationOptional.hasValue)
                        return QString("[%1, %2]")
                                .arg(m_pin->locationOptional.value->x)
                                .arg(m_pin->locationOptional.value->y);
                    return QVariant();
                default:
                    return QVariant();
            }
        default:
            return QVariant();
    }
}

QVariant PinDetailModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (section == PropertyNameColumn)
        return QStringLiteral("Property");

    if (section == PropertyValueColumn)
        return QStringLiteral("Value");

    return QVariant();
}
