#include "PackageDetailModel.h"
#include "enumtranslator.h"
#include "Package.h"

enum
{
    PackageNameRow = 0,
    PackageTypeRow,
    PinOneRow,
    PinOneOrientationRow,
    PackageHeightRow,
    CommentRow,
    PickupPointRow,
    RowCount
};

enum
{
    PropertyNameColumn = 0,
    PropertyValueColumn,
    ColumnCount
};

PackageDetailModel::PackageDetailModel(QObject *parent):
    QAbstractTableModel(parent),
    m_package(nullptr)
{

}

void PackageDetailModel::setPackage(Ipc2581b::Package *package)
{
    beginResetModel();
    m_package = package;
    endResetModel();
}

int PackageDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (m_package != nullptr)
        return RowCount;
    return 0;
}

int PackageDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return ColumnCount;
}

QVariant PackageDetailModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    switch (index.column())
    {
        case PropertyNameColumn:
            switch (index.row())
            {
                case PackageNameRow:
                    return QStringLiteral("Name");
                case PackageTypeRow:
                    return QStringLiteral("Type");
                case PinOneRow:
                    return QStringLiteral("Pin1");
                case PinOneOrientationRow:
                    return QStringLiteral("Pin1 Orientation");
                case PackageHeightRow:
                    return QStringLiteral("Height");
                case CommentRow:
                    return QStringLiteral("Comment");
                case PickupPointRow:
                    return QStringLiteral("Pickup point");
                default:
                    return QVariant();
            }
        case PropertyValueColumn:
            switch (index.row())
            {
                case PackageNameRow:
                    return m_package->name;
                case PackageTypeRow:
                    return Ipc2581b::EnumTranslator::packageTypeText(m_package->type);
                case PinOneRow:
                    if (m_package->pinOneOptional.hasValue)
                        return m_package->pinOneOptional.value;
                    return QVariant();
                case PinOneOrientationRow:
                    if (m_package->pinOneOrientationOptional.hasValue)
                        return Ipc2581b::EnumTranslator::pinOneOrientationText(m_package->pinOneOrientationOptional.value);
                    return QVariant();
                case PackageHeightRow:
                    if (m_package->heightOptional.hasValue)
                        return m_package->heightOptional.value;
                    return QVariant();
                case CommentRow:
                    if (m_package->commentOptional.hasValue)
                        return m_package->commentOptional.value;
                    return QVariant();
                case PickupPointRow:
                    if (m_package->pickupPointOptional.hasValue)
                        return QString("[%1, %2]")
                                .arg(m_package->pickupPointOptional.value->x)
                                .arg(m_package->pickupPointOptional.value->y);
                    return QVariant();
                default:
                    return QVariant();
            }
        default:
            return QVariant();
    }
}

QVariant PackageDetailModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (section == PropertyNameColumn)
        return QStringLiteral("Property");

    if (section == PropertyValueColumn)
        return QStringLiteral("Value");

    return QVariant();
}
