#ifndef NULLVIEWER_H
#define NULLVIEWER_H

#include "iviewer.h"

class QWidget;

class NullViewer : public IViewer
{
public:
    explicit NullViewer(QObject *parent = nullptr);

private:
    QWidget *m_centralWidget;

    // IViewer interface
public:
    virtual QWidget *centralWidget() const override;
    virtual void enableAntiAlias(bool enabled) override;
    virtual void enableTransparentLayers(bool enabled) override;
    virtual void enableDropShadowEffect(bool enabled) override;
    virtual void enableOpenGL(bool enabled) override;
    virtual void setLevelOfDetails(LevelOfDetails lod) override;
    virtual void activate(QMainWindow *mainWindow) override;
    virtual void desactivate(QMainWindow *mainWindow) override;
};

#endif // NULLVIEWER_H
