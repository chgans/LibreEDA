#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setObjectName("app");
    app.setApplicationName("LeIpc2581Viewer");
    app.setApplicationVersion("0.0");
    app.setApplicationDisplayName("IPC 2581 Viewer");
    app.setOrganizationName("LibreEDA");
    app.setOrganizationDomain("www.libre-eda.org");

    MainWindow window;
    window.setObjectName("mainWindow");
    window.show();

    return app.exec();
}
