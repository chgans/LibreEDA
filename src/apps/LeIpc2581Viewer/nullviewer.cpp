#include "nullviewer.h"
#include <QWidget>

NullViewer::NullViewer(QObject *parent):
    IViewer(parent),
    m_centralWidget(new QWidget)
{

}

QWidget *NullViewer::centralWidget() const
{
    return m_centralWidget;
}

void NullViewer::enableAntiAlias(bool enabled)
{
    Q_UNUSED(enabled);
}

void NullViewer::enableTransparentLayers(bool enabled)
{
    Q_UNUSED(enabled);
}

void NullViewer::enableDropShadowEffect(bool enabled)
{
    Q_UNUSED(enabled);
}

void NullViewer::enableOpenGL(bool enabled)
{
    Q_UNUSED(enabled);
}

void NullViewer::setLevelOfDetails(LevelOfDetails lod)
{
    Q_UNUSED(lod);
}

void NullViewer::activate(QMainWindow *mainWindow)
{
    Q_UNUSED(mainWindow)
}

void NullViewer::desactivate(QMainWindow *mainWindow)
{
    Q_UNUSED(mainWindow)
}
