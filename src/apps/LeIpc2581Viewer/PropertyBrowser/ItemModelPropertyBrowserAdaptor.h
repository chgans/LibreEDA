#pragma once

#include <QObject>
#include <QModelIndex>

class QAbstractItemModel;
class QModelIndex;

class QtAbstractPropertyBrowser;
class QtVariantEditorFactory;
class QtVariantPropertyManager;

class ItemModelPropertyBrowserAdaptor: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool readOnly READ isReadOnly WRITE setReadOnly NOTIFY readOnlyChanged)

public:
    explicit ItemModelPropertyBrowserAdaptor(QObject *parent = 0);

    bool isReadOnly() const;

signals:
    void readOnlyChanged(bool readOnly);

public slots:
    void setItemModel(QAbstractItemModel *model);
    void setModelIndex(const QModelIndex &index);
    void setPropertyBrowser(QtAbstractPropertyBrowser *browser);    
    void setReadOnly(bool readOnly);

private:
    QAbstractItemModel *m_model;
    QModelIndex m_index;
    QtAbstractPropertyBrowser *m_browser;
    QtVariantPropertyManager *m_manager;
    QtVariantEditorFactory *m_factory;
    bool m_readOnly;

    void clear();
    void populate();
};
