#ifndef PADSTACKDEFVIEWER_H
#define PADSTACKDEFVIEWER_H

#include "iviewer.h"

#include "PadUse.h"

#include <QColor>
#include <QList>
#include <QMap>

class QGraphicsItemGroup;
class QGraphicsItem;
class QGraphicsPathItem;
class QListView;
class QTableView;
class QStackedWidget;

namespace Ipc2581b
{
    class Ipc2581;
    class PadstackDef;
    class Layer;
}

class PadstackDefListModel;
class LayerListModel;
class LayerListWidget;
class PadUseListModel;
class PadstackPadDetailModel;
class PadstackHoleDetailModel;
class GraphicsScene;
class GraphicsView;
class GraphicsItemFactory;

class PadstackDefViewer : public IViewer
{
    Q_OBJECT
public:
    explicit PadstackDefViewer(QObject *parent = 0);

private slots:
    void setCurrentPadstackDef(Ipc2581b::PadstackDef *padstackDef);
    void setCurrentLayer(const Ipc2581b::Layer *layer);

private:
    void clear();

    void updateDetailView(const Ipc2581b::Layer *layer, Ipc2581b::PadUse use);

    Ipc2581b::Ipc2581 *m_ipc;
    QList<Ipc2581b::PadstackDef *> m_padstackDefList;
    PadstackDefListModel *m_padstackDefListModel;
    QListView *m_padstackDefListView;
    Ipc2581b::PadstackDef *m_currentPadstackDef;

    GraphicsScene *m_graphicsScene;
    GraphicsView *m_graphicsView;
    GraphicsItemFactory *m_graphicsItemFactory;
    //QList<QGraphicsItemGroup* > m_graphicsLayerList;
    //QGraphicsItemGroup *m_graphicsHoleLayer;
    //QGraphicsItemGroup *m_graphicsHighlightLayer;
    //QGraphicsPathItem *m_graphicsShapeLayer;

    LayerListWidget *m_layerListWidget;
    LayerListModel *m_layerListModel;
    QList<Ipc2581b::Layer *> m_allLayerList;
    QMap<QString, Ipc2581b::Layer *> m_allLayerMap;
    QList<QColor> m_usedLayerColorList;
    const Ipc2581b::Layer *m_currentLayer;

    PadUseListModel *m_padUseListModel;
    QListView *m_padUseListView;

    PadstackPadDetailModel *m_padstackPadDefDetailModel;
    QTableView *m_padstackPadDefDetailView;

    PadstackHoleDetailModel *m_padstackHoleDefDetailModel;
    QTableView *m_padstackHoleDefDetailView;

    QMap<const Ipc2581b::Layer*, QMap<Ipc2581b::PadUse, QGraphicsItem *> > m_graphicsItemMap;

    QWidget *m_navigationWidget;
    QWidget *m_auxiliaryWidget;
    QStackedWidget *m_detailViewStackedWidget;

    // IViewer interface
public:
    virtual void setDocument(Ipc2581b::Ipc2581 *doc) override;
    virtual QWidget *centralWidget() const override;
    virtual QWidget *navigationWidget() const override;
    virtual QWidget *auxiliaryWidget() const override;
    virtual void enableAntiAlias(bool enabled) override;
    virtual void enableTransparentLayers(bool enabled) override;
    virtual void enableDropShadowEffect(bool enabled) override;
    virtual void enableOpenGL(bool enabled) override;
    virtual void setLevelOfDetails(LevelOfDetails lod) override;
};

#endif // PADSTACKDEFVIEWER_H
