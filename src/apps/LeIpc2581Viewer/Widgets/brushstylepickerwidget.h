#ifndef BRUSHSTYLEPICKERWIDGET_H
#define BRUSHSTYLEPICKERWIDGET_H

#include <QMap>
#include <QWidget>

class BrushRenderFrame;

class BrushStylePickerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BrushStylePickerWidget(QWidget *parent = 0);

    void setSelectedBrushStyle(Qt::BrushStyle style);
    Qt::BrushStyle selectedBrushStyle() const;
    Qt::BrushStyle hoveredBrushStyle() const;
    void setBrushColor(const QColor &color);

signals:
    void brushStyleClicked(Qt::BrushStyle style);
    void brushStyleDoubleClicked(Qt::BrushStyle style);
    void brushStyleHovered(Qt::BrushStyle style);

public slots:

private:
    static const QSize g_iconSize;
    Qt::BrushStyle m_selectedBrushStyle;
    Qt::BrushStyle m_hoveredBrushStyle;
    QColor m_brushColor;
    QMap<Qt::BrushStyle, BrushRenderFrame *> m_styleToFrame;
    BrushRenderFrame *createFrame(Qt::BrushStyle style);
    void setHoveredBrushStyle(Qt::BrushStyle style);

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    virtual void enterEvent(QEvent *event) override;
    virtual void leaveEvent(QEvent *event) override;
};

#endif // BRUSHSTYLEPICKERWIDGET_H
