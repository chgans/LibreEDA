#pragma once

#include <QComboBox>

class ColorComboBox : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(QColor currentColor READ currentColor NOTIFY currentColorChanged)
    Q_PROPERTY(QList<QColor> colors READ colors WRITE setColors)

public:
    explicit ColorComboBox(QWidget *parent = nullptr);
    ~ColorComboBox();

    void setCurrentColor(const QColor &col);
    QColor currentColor() const;

    void setColors(const QList<QColor> &colors);
    QList<QColor> colors() const;

signals:
    void activated(const QColor &col);
    void highlighted(const QColor &col);
    void currentColorChanged(const QColor &currentColor);

private slots:
    void onActivated(int index);
    void onHighlighted(int index);
    void onCurrentIndexChanged(int index);

private:
    Q_DISABLE_COPY(ColorComboBox)
    QList<QColor> m_colorList;
    QColor m_paintedColor;

    // QComboBox Interface
protected:
   virtual void paintEvent(QPaintEvent *event) override;
};
