#pragma once

#include <QMap>
#include <QWidget>

class BrushRenderFrame;
class QLabel;

class ColorPickerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ColorPickerWidget(QWidget *parent = 0);

    void setLabelVisible(bool visible);
    bool labelVisible() const;

    QColor selectedColor() const;
    void setSelectedColor(const QColor &color);
    QColor hoveredColor() const;

signals:
    void colorClicked(const QColor &color);
    void colorHovered(const QColor &color);
    void colorDoubleClicked(const QColor &color);

public slots:

private:
    static const QSize g_iconSize;
    QColor m_selectedColor;
    QColor m_hoveredColor;
    QMap<QString, BrushRenderFrame *> m_colorToFrame;
    QList<QLabel *> m_labelList;
    BrushRenderFrame *createFrame(const QColor &color, const QString &toolTip);
    void setHoveredColor(const QColor &color);
    QColor darkest(const QColor &color);
    QColor darker(const QColor &color);
    QColor nominal(const QColor &color);
    QColor lighter(const QColor &color);
    QColor lightest(const QColor &color);

    // QWidget interface
public:
    virtual QSize sizeHint() const override;

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
    virtual void enterEvent(QEvent *event) override;
    virtual void leaveEvent(QEvent *event) override;
};
