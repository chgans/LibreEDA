#pragma once

#include <QWidget>

class ColorPickerWidget;

class ModelItemColorEditor : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor USER true)

public:
    explicit ModelItemColorEditor(QWidget *parent);

    QColor color() const;
    void setColor(QColor color);

private:
    ColorPickerWidget *m_picker;
    QColor m_bgColor;

private slots:
    void updateBgColor(const QColor &color);
    void closeEditor();

    // QWidget interface
protected:
    virtual void showEvent(QShowEvent *event) override;
    virtual void moveEvent(QMoveEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;
};

