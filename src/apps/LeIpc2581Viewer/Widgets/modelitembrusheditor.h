#ifndef MODELITEMBRUSHSTYLEEDITOR_H
#define MODELITEMBRUSHSTYLEEDITOR_H

#include <QWidget>
#include <QPainterPath>

class QPushButton;

class BrushStylePickerWidget;
class ColorPickerWidget;

class ModelItemBrushEditor : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QBrush brush READ brush WRITE setBrush USER true)

public:
    explicit ModelItemBrushEditor(QWidget *parent = 0);

    QBrush selectedBrush() const;
    QBrush brush() const;
    void setBrush(const QBrush &brush);

    void setPath(const QPainterPath &path);

signals:
    void accepted();
    void rejected();

private slots:
    void accept();
    void setCurrentBrushStyle(Qt::BrushStyle style);
    void setCurrentBrushColor(const QColor &color);

private:
    //QDialogButtonBox *m_buttonBox;
    bool m_accepted;
    QPushButton *m_okButton;
    QPushButton *m_cancelButton;
    BrushStylePickerWidget *m_brushPicker;
    ColorPickerWidget *m_colorPicker;
    QWidget *m_popUpWidget;
    QBrush m_brush;
    QBrush m_currentBrush;
    QPainterPath m_path;

    void setupWidget();

    // QWidget interface
protected:
    virtual void showEvent(QShowEvent *event) override;
    virtual void hideEvent(QHideEvent *event) override;
    virtual void moveEvent(QMoveEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;
};

#endif // MODELITEMBRUSHSTYLEEDITOR_H
