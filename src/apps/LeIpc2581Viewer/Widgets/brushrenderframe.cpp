#include "brushrenderframe.h"

#include <QPainter>

BrushRenderFrame::BrushRenderFrame(QWidget *parent):
    QFrame(parent)
{

}

BrushRenderFrame::~BrushRenderFrame()
{

}

QBrush BrushRenderFrame::brush() const
{
    return m_brush;
}

void BrushRenderFrame::setBrush(QBrush brush)
{
    if (m_brush == brush)
        return;

    m_brush = brush;
    update();
    emit brushChanged(brush);
}


void BrushRenderFrame::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QRect paintedRect = frameRect().adjusted(2, 2, -2, -2);

    // Draw the chess pattern for transparency
    const int s = 8;
    QRect r(0, 0, s, s);
    r.translate(paintedRect.topLeft());
    for (int col = 0; col < (width()/s+1); col++)
    {
        for (int row = 0; row < (height()/s+1); row++)
        {
            if ((row%2 == 0 && col%2 == 0) || (row%2 == 1 && col%2 == 1))
                continue;
            painter.fillRect(r.translated(col*s, row*s), QColor("#dddddd"));
        }
    }

    // Fill with the "real" brush
    painter.setBrush(m_brush);
    painter.setPen(Qt::NoPen);
    painter.drawRect(paintedRect);

    // Keep QFrame effects
    QFrame::paintEvent(event);
}
