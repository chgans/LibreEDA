import qbs

LedaProduct {
    type: ["application"]
    name: "LeIpc2581Viewer"
    targetName: "le-ipc2581-viewer"
    version: project.leda_version

    installDir: project.leda_bin_path

    Depends { name: "Qt"; submodules: ["core", "widgets", "network", "printsupport"] }
    Depends { name: "LeIpc2581" }
    Depends { name: "LeGraphicsView" }
    Depends { name: "QtPropertyBrowser" }
    Depends { name: "Data" }

    // FIXME: should rely on leda_library_path (LibreEDA.qbs)
    cpp.rpaths: qbs.targetOS.contains("osx") ? ["@executable_path/../Frameworks"] : ["$ORIGIN/../lib/leda", "$ORIGIN/../lib/Qt/lib"]

    //cpp.defines: base.concat(["RELATIVE_PLUGIN_PATH=\" + "\""])
    cpp.includePaths: [sourceDirectory]

    files : [
        "LeIpc2581Viewer.qrc",
        "Document.cpp",
        "Document.h",
//        "EcadDatabase.cpp",
//        "EcadDatabase.h",
        "GraphicsItemFactory.cpp",
        "GraphicsItemFactory.h",
        "graphicslayersetdialog.cpp",
        "graphicslayersetdialog.h",
        "iviewer.cpp",
        "iviewer.h",
        "LayerListWidget.cpp",
        "LayerListWidget.h",
        "layerviewer.cpp",
        "layerviewer.h",
        "main.cpp",
        "mainwindow.cpp",
        "mainwindow.h",
        "nullviewer.cpp",
        "nullviewer.h",
//        "packageviewer.cpp",
//        "packageviewer.h",
//        "padstackdefviewer.cpp",
//        "padstackdefviewer.h",
        "StringFormatter.h",
        "ItemModels/ComponentListModel.cpp",
        "ItemModels/ComponentListModel.h",
        "ItemModels/FeatureSetListModel.cpp",
        "ItemModels/FeatureSetListModel.h",
        "ItemModels/HoleListModel.cpp",
        "ItemModels/HoleListModel.h",
        "ItemModels/ItemModel.h",
        "ItemModels/LayerListModel.cpp",
        "ItemModels/LayerListModel.h",
        "ItemModels/LayerSetModel.cpp",
        "ItemModels/LayerSetModel.h",
        "ItemModels/LayerStackupModel.cpp",
        "ItemModels/LayerStackupModel.h",
        "ItemModels/MultiColumnFilterProxyModel.cpp",
        "ItemModels/MultiColumnFilterProxyModel.h",
        "ItemModels/NetListModel.cpp",
        "ItemModels/NetListModel.h",
//        "ItemModels/PackageDetailModel.cpp",
//        "ItemModels/PackageDetailModel.h",
        "ItemModels/PackageListModel.cpp",
        "ItemModels/PackageListModel.h",
        "ItemModels/PadListModel.cpp",
        "ItemModels/PadListModel.h",
        //"ItemModels/padstackdeflistmodel.cpp",
        //"ItemModels/padstackdeflistmodel.h",
        //"ItemModels/PadstackHoleDetailModel.cpp",
        //"ItemModels/PadstackHoleDetailModel.h",
        //"ItemModels/PadstackPadDetailModel.cpp",
        //"ItemModels/PadstackPadDetailModel.h",
        //"ItemModels/PadUseListModel.cpp",
        //"ItemModels/PadUseListModel.h",
        //"ItemModels/PinDetailModel.cpp",
        //"ItemModels/PinDetailModel.h",
        //"ItemModels/PinListModel.cpp",
        //"ItemModels/PinListModel.h",
        "ItemModels/SpecificationTreeModel.cpp",
        "ItemModels/SpecificationTreeModel.h",
        "PropertyBrowser/ItemModelPropertyBrowserAdaptor.cpp",
        "PropertyBrowser/ItemModelPropertyBrowserAdaptor.h",
        "PropertyBrowser/PropertyManager.cpp",
        "PropertyBrowser/PropertyManager.h",
        "Widgets/brushrenderframe.cpp",
        "Widgets/brushrenderframe.h",
        "Widgets/brushstylecombobox.cpp",
        "Widgets/brushstylecombobox.h",
        "Widgets/brushstylepickerwidget.cpp",
        "Widgets/brushstylepickerwidget.h",
        "Widgets/colorcombobox.cpp",
        "Widgets/colorcombobox.h",
        "Widgets/ColorPickerWidget.cpp",
        "Widgets/ColorPickerWidget.h",
        "Widgets/modelitembrushdelegate.cpp",
        "Widgets/modelitembrushdelegate.h",
        "Widgets/modelitembrusheditor.cpp",
        "Widgets/modelitembrusheditor.h",
        "Widgets/ModelItemCheckBoxDelegate.cpp",
        "Widgets/ModelItemCheckBoxDelegate.h",
        "Widgets/modelitemcoloreditor.cpp",
        "Widgets/modelitemcoloreditor.h",
        "Widgets/penstylecombobox.cpp",
        "Widgets/penstylecombobox.h",
    ]
}
