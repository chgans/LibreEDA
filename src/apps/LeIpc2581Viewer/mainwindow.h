#pragma once

#include <QMainWindow>
#include "iviewer.h"

namespace Ipc2581b
{
    class Ipc2581;
}

class IViewer;
class PackageViewer;
class PadstackDefViewer;
class LayerViewer;
class NullViewer;

class QStackedWidget;
class QToolBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void requestOpenFile();

    void enableAntiAlias(bool enabled);
    void enableTransparentLayers(bool enabled);
    void enableDropShadowEffect(bool enabled);
    void enableOpenGL(bool enabled);
    void setLevelOfDetails(LevelOfDetails lod);

private:
    Ipc2581b::Ipc2581 *m_ipc = nullptr;

    LayerViewer *m_layerViewer = nullptr;

    bool m_antiAliasEnabled = false;
    bool m_transparentLayersEnabled = false;
    bool m_dropShadowEffectEnabled = false;
    bool m_openGLEnabled = false;
    LevelOfDetails m_levelOfDetails = LevelOfDetails::High;

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
};
