#-------------------------------------------------
#
# Project created by QtCreator 2016-10-24T18:50:12
#
#-------------------------------------------------

QT       += core gui widgets xml
CONFIG += c++11

TARGET = le-ipc2581b-viewer
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    layerviewer.cpp \
    iviewer.cpp \
    LayerListWidget.cpp \
    nullviewer.cpp \
    GraphicsItemFactory.cpp \
    graphicslayersetdialog.cpp \
    Widgets/brushrenderframe.cpp \
    Widgets/brushstylecombobox.cpp \
    Widgets/brushstylepickerwidget.cpp \
    Widgets/colorcombobox.cpp \
    Widgets/ColorPickerWidget.cpp \
    Widgets/modelitembrushdelegate.cpp \
    Widgets/modelitembrusheditor.cpp \
    Widgets/ModelItemCheckBoxDelegate.cpp \
    Widgets/modelitemcoloreditor.cpp \
    Widgets/penstylecombobox.cpp \
    PropertyBrowser/ItemModelPropertyBrowserAdaptor.cpp \
    ItemModels/LayerSetModel.cpp \
    ItemModels/ComponentListModel.cpp \
    ItemModels/PackageListModel.cpp \
    ItemModels/NetListModel.cpp \
    ItemModels/PadListModel.cpp \
    ItemModels/HoleListModel.cpp \
    Document.cpp \
    ItemModels/LayerListModel.cpp \
    ItemModels/FeatureSetListModel.cpp \
    PropertyBrowser/PropertyManager.cpp \
    ItemModels/MultiColumnFilterProxyModel.cpp \
    ItemModels/SpecificationTreeModel.cpp \
    ItemModels/LayerStackupModel.cpp

HEADERS  += mainwindow.h \
    layerviewer.h \
    iviewer.h \
    LayerListWidget.h \
    nullviewer.h \
    GraphicsItemFactory.h \
    graphicslayersetdialog.h \
    Widgets/brushrenderframe.h \
    Widgets/brushstylecombobox.h \
    Widgets/brushstylepickerwidget.h \
    Widgets/colorcombobox.h \
    Widgets/ColorPickerWidget.h \
    Widgets/modelitembrushdelegate.h \
    Widgets/modelitembrusheditor.h \
    Widgets/ModelItemCheckBoxDelegate.h \
    Widgets/modelitemcoloreditor.h \
    Widgets/penstylecombobox.h \
    PropertyBrowser/ItemModelPropertyBrowserAdaptor.h \
    ItemModels/LayerSetModel.h \
    ItemModels/ComponentListModel.h \
    ItemModels/PackageListModel.h \
    ItemModels/NetListModel.h \
    ItemModels/PadListModel.h \
    ItemModels/HoleListModel.h \
    Document.h \
    ItemModels/LayerListModel.h \
    ItemModels/FeatureSetListModel.h \
    StringFormatter.h \
    PropertyBrowser/PropertyManager.h \
    ItemModels/MultiColumnFilterProxyModel.h \
    ItemModels/SpecificationTreeModel.h \
    ItemModels/LayerStackupModel.h \
    ItemModels/ItemModel.h

RESOURCES += \
    le-gerber-viewer.qrc

include(../lib/qtpropertybrowser/qtpropertybrowser.pri)
include(../lib/LeGraphicsView/LeGraphicsView.pri)
include(../lib/LeIpc2581/LeIpc2581.pri)
