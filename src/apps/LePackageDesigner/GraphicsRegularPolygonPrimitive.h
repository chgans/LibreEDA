#pragma once

#include "LeGraphicsView/LeGraphicsItem.h"

#include "LeDocumentObject/IDocumentObjectListener.h"

class RegularPolygonPrimitive;
class LeGraphicsHandleItem;


class GraphicsRegularPolygonPrimitive : public LeGraphicsItem, public LDO::IDocumentObjectListener
{
    Q_OBJECT

public:
    GraphicsRegularPolygonPrimitive(RegularPolygonPrimitive *primitive);
    ~GraphicsRegularPolygonPrimitive();


private:
    void updateGeometry();
    void addHandles();
    RegularPolygonPrimitive *m_primitive;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;
protected:
    virtual QPointF handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value) override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const LDO::IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object, const QString &name, const QVariant &newValue) override;

};

