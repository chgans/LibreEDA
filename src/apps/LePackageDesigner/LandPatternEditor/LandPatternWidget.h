#pragma once

#include "EditorWidget.h"

#include "LeDocumentObject/IDocumentObjectListener.h"

class AbstractTask;
class LandPattern;

namespace LDO {
    class Document;
}

class LeGraphicsView;
class LeGraphicsScene;
class LeGraphicsItemLayer;

class QSettings;

class LandPatternWidget : public EditorWidget, public LDO::IDocumentObjectListener
{
    Q_OBJECT
public:
    explicit LandPatternWidget(QWidget *parent = 0);

signals:

public slots:
    void startTask(AbstractTask *task);
    void restoreGeometryAndState(QSettings &settings);
    void saveGeometryAndState(QSettings &settings);

    LeGraphicsScene *scene() const;
    LeGraphicsView *view() const;

private:
    void setupScene();
    void setupView();
    void addObject(LDO::IDocumentObject *object);
    void removeObject(LDO::IDocumentObject *object);

private:
    LeGraphicsScene *m_scene = nullptr;
    LeGraphicsView *m_view = nullptr;
    LeGraphicsItemLayer *m_mcadLayer = nullptr;

    MainWindow *m_gui = nullptr;
    LDO::Document *m_document = nullptr;
    LandPattern *m_landPattern = nullptr;

    // EditorWidget interface
public:
    virtual void edit(MainWindow *gui, LDO::Document *document, LDO::IDocumentObject *object) override;
    virtual LDO::IDocumentObject *object() const override;
    virtual LDO::Document *document() const override;
    virtual QList<AbstractTask *> tasks() const override;
    virtual QString title() const override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToBeInserted(LDO::IDocumentObject *parent,
                                                 LDO::IDocumentObject *child,
                                                 int index) override;
    virtual void documentObjectInserted(LDO::IDocumentObject *parent,
                                        LDO::IDocumentObject *child,
                                        int index) override;
    virtual void documentObjectAboutToBeRemoved(LDO::IDocumentObject *parent,
                                                LDO::IDocumentObject *child,
                                                int index) override;
    virtual void documentObjectRemoved(LDO::IDocumentObject *parent,
                                       LDO::IDocumentObject *child,
                                       int index) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &value) override;
};
