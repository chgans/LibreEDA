#ifndef RENUMBERTERMINALSTASK_H
#define RENUMBERTERMINALSTASK_H

#include "AbstractTask.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

#include <QLineF>

class QGraphicsItem;

class LeGraphicsLabelItem;
class LeGraphicsScene;
class LeGraphicsView;

class QtVariantPropertyManager;
class QtVariantEditorFactory;

class TerminalItem;

class RenumberTerminalsTask : public AbstractTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT
public:
    explicit RenumberTerminalsTask(QObject *parent = 0);
    ~RenumberTerminalsTask();

public slots:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

private:
    QList<QGraphicsItem*> recursiveChildItems(QGraphicsItem *parentItem) const;

private:
    QtVariantPropertyManager *m_propertyManager = nullptr;
    QtVariantEditorFactory *m_propertyEditorFactory = nullptr;

    QList<TerminalItem*> m_terminals;
    QList<TerminalItem*> m_toRenumber;
    QList<TerminalItem*> m_renumbered;
    QList<TerminalItem*> m_candidates;
    QList<QString> m_originalNumbers;
    QList<QLineF> m_hitPath;

    // LeGraphicsSceneEventFilter interface
    void detachPropertyManager();

    void detachScene();

    void revertNumbering();
    void applyNumbering();

    void initialiseNumbering();

    void attachPropertyManager();

    void attachScene();

public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};

#endif // RENUMBERTERMINALSTASK_H
