#pragma once

#include "LeGraphicsView/LeGraphicsItem.h"

#include "LeDocumentObject/IDocumentObjectListener.h"

class ArcOfCircle;
class LeGraphicsHandleItem;


class GraphicsArcOfCircle : public LeGraphicsItem, public LDO::IDocumentObjectListener
{
    Q_OBJECT

public:
    GraphicsArcOfCircle(ArcOfCircle *primitive);
    ~GraphicsArcOfCircle();


private:
    void updateGeometry();
    void addHandles();
    ArcOfCircle *m_primitive;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // LeGraphicsItem interface
public:
    virtual LeGraphicsItem *clone() const override;
protected:
    virtual QPointF handlePositionChangeFilter(LeGraphicsHandleRole role, const QPointF &value) override;

    // IDocumentObjectListener interface
public:
    virtual void documentObjectAboutToChangeProperty(const LDO::IDocumentObject *object, const QString &name, const QVariant &oldValue) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object, const QString &name, const QVariant &newValue) override;

};

