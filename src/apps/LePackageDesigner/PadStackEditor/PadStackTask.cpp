#include "PadStackTask.h"
#include "PadStackWidget.h"

PadStackTask::PadStackTask(PadStackWidget *editor)
    : AbstractTask(editor)
    , m_editor(editor)
{

}

PadStackWidget *PadStackTask::editor() const
{
    return m_editor;
}

LeGraphicsScene *PadStackTask::scene() const
{
    return m_editor->scene();
}
