#ifndef PADSTACKWIDGET_H
#define PADSTACKWIDGET_H

#include "EditorWidget.h"

#include "LeIpc7351/LeIpc7351.h"

#include "LeDocumentObject/IDocumentObjectListener.h"

#include <QList>
#include <QMap>

namespace LDO {
    class Document;
}

class LeGraphicsLayerStackModel;
class PadStack;
class PadStackElement;
class Primitive;

class LeGraphicsView;
class LeGraphicsScene;
class LeGraphicsItem;
class LeGraphicsItemLayer;

class QGraphicsItem;
class QSettings;
class QTableView;
class QToolBar;

class PadStackWidget : public EditorWidget, public LDO::IDocumentObjectListener
{
    Q_OBJECT

public:
    explicit PadStackWidget(QWidget *parent = 0);

    LeGraphicsScene *scene() const;
    LeGraphicsView *view() const;
    PadStackElement *currentElement() const;

    void restoreGeometryAndState(QSettings &settings);
    void saveGeometryAndState(QSettings &settings);

signals:

public slots:

private:
    void setupScene();
    LeGraphicsItemLayer *addLayer(PadStackElement *element, const QString &fullName, const QString &shortName, const QString &tinyName, const QColor &color);
    void setupView();
    void setPadStack(PadStack *padStack);
    void createTasks();
    void setupToolBar();
    void setupLayerBar();
    void taskActionTriggered();

    void applyDocumentSelection(const QList<LDO::IDocumentObject*> &current,
                                     const QList<LDO::IDocumentObject*> &previous);
    void applySceneSelection();

    void addShapeItem(PadStackElement *element);
    void removeShapeItem(PadStackElement *element);

private:
    LeGraphicsScene *m_scene = nullptr;
    LeGraphicsView *m_view = nullptr;
    QMap<PadStackElement*, LeGraphicsItemLayer*> m_elementToLayer;
    QMap<LeGraphicsItemLayer*, PadStackElement*> m_layerToElement;
    QMap<QGraphicsItem*, Primitive*> m_sceneItemToDocumentPrimitive;
    QMap<Primitive*, QGraphicsItem*> m_documentPrimitiveToSceneItem;
    //QList<LeGraphicsItemLayer*> m_layerList;
    MainWindow *m_gui = nullptr;
    LDO::Document *m_document = nullptr;
    PadStack *m_padStack = nullptr;
    QToolBar *m_toolBar = nullptr;
    LeGraphicsLayerStackModel *m_layerStackModel = nullptr;
    QTableView *m_layerStackView = nullptr;
    QList<AbstractTask *> m_placeTasks;

    // IDocumentObjectListener interface
    void setupLayout();

public:
    virtual void documentObjectAboutToBeInserted(LDO::IDocumentObject *parent,
                                                 LDO::IDocumentObject *child,
                                                 int index) override;
    virtual void documentObjectInserted(LDO::IDocumentObject *parent,
                                        LDO::IDocumentObject *child,
                                        int index) override;
    virtual void documentObjectAboutToBeRemoved(LDO::IDocumentObject *parent,
                                                LDO::IDocumentObject *child,
                                                int index) override;
    virtual void documentObjectRemoved(LDO::IDocumentObject *parent,
                                       LDO::IDocumentObject *child,
                                       int index) override;
    virtual void documentObjectAboutToChangeProperty(const LDO::IDocumentObject *object,
                                                     const QString &name,
                                                     const QVariant &value) override;
    virtual void documentObjectPropertyChanged(const LDO::IDocumentObject *object,
                                               const QString &name,
                                               const QVariant &value) override;

    // EditorWidget interface
public:
    virtual void edit(MainWindow *gui, LDO::Document *document, LDO::IDocumentObject *object) override;
    virtual LDO::IDocumentObject *object() const override;
    virtual LDO::Document *document() const override;
    virtual QList<AbstractTask *> tasks() const override;
    virtual QString title() const override;
};

#endif // PADSTACKWIDGET_H
