#pragma once

#include "AbstractTask.h"

class GraphicsPadStackElementItem;

class PadStackWidget;

class PadStackTask : public AbstractTask
{
    Q_OBJECT

public:
    explicit PadStackTask(PadStackWidget *editor);

    PadStackWidget *editor() const;
    LeGraphicsScene *scene() const;

private:
    PadStackWidget *m_editor;
};

