#pragma once

#include "PadStackTask.h"

#include "LeIpc7351/Primitives/ButterflyPrimitive.h"

#include "LeGraphicsView/LeGraphicsSceneEventFilter.h"

#include <QPointF>
#include <QLineF>

class ButterflyPrimitive;
class GraphicsButterflyPrimitive;

class PlaceButterflyPrimitiveTask: public PadStackTask, public LeGraphicsSceneEventFilter
{
    Q_OBJECT

    Q_PROPERTY(ButterflyPrimitive::Shape shape READ shape WRITE setShape NOTIFY shapeChanged)

public:
    explicit PlaceButterflyPrimitiveTask(PadStackWidget *editor = nullptr);
    ~PlaceButterflyPrimitiveTask();

    ButterflyPrimitive::Shape shape() const;

signals:
    void shapeChanged(ButterflyPrimitive::Shape shape);

public slots:
    void setShape(ButterflyPrimitive::Shape shape);

private:
    enum
    {
        WaitForP1,
        WaitForP2,
        Finished
    };
    int m_state = WaitForP1;
    ButterflyPrimitive *m_primitive = nullptr;
    GraphicsButterflyPrimitive *m_item = nullptr;
    QLineF m_radiusLine;

    // AbstractTask interface
public slots:
    virtual void start() override;
    virtual void accept() override;
    virtual void reject() override;

    // LeGraphicsSceneEventFilter interface
public:
    virtual void drawBackground(QPainter *painter, const QRectF &rect) override;
    virtual void drawForeground(QPainter *painter, const QRectF &rect) override;
    virtual bool keyPressEvent(QKeyEvent *event) override;
    virtual bool keyReleaseEvent(QKeyEvent *event) override;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
};
