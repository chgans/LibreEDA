#include "PlaceTrianglePrimitive.h"

#include "PadStackWidget.h"
#include "Gui.h"
#include "GraphicsTrianglePrimitive.h"

#include "LeIpc7351/PadStackElement.h"
#include "LeIpc7351/Primitives/TrianglePrimitive.h"

#include "LeGraphicsView/LeGraphicsItem.h"
#include "LeGraphicsView/LeGraphicsItemLayer.h"
#include "LeGraphicsView/LeGraphicsScene.h"
#include "LeGraphicsView/LeGraphicsStyle.h"
#include "LeGraphicsView/LeGraphicsPalette.h"

#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QRectF>

PlaceTrianglePrimitiveTask::PlaceTrianglePrimitiveTask(PadStackWidget *editor)
    : PadStackTask(editor)
    , LeGraphicsSceneEventFilter()
{
    setTile("Triangular pad");
    setDescription("Place a tringular pad");
    setIcon(Gui::icon("primitive-shape-triangle"));
    setShortcut(QKeySequence("p,t"));
}


void PlaceTrianglePrimitiveTask::start()
{
    m_primitive = new TrianglePrimitive();
    m_item = new GraphicsTrianglePrimitive(m_primitive);
    m_state = WaitForP1;
    scene()->setEventFilter(this);
    emit started(this);
}

void PlaceTrianglePrimitiveTask::accept()
{
    Q_ASSERT(m_state == Finished);

    scene()->currentLayer()->removeFromLayer(m_item);
    delete m_item;
    m_item = nullptr;
    editor()->currentElement()->setPrimitive(m_primitive); // FIXME
    scene()->setEventFilter(nullptr);
    emit accepted(this);
}

void PlaceTrianglePrimitiveTask::reject()
{
    if (m_state != WaitForP1)
    {
        scene()->currentLayer()->removeFromLayer(m_item);
    }
    delete m_item;
    m_item = nullptr;
    delete m_primitive;
    m_primitive = nullptr;
    scene()->setEventFilter(nullptr);
    emit rejected(this);
}

void PlaceTrianglePrimitiveTask::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter)
    Q_UNUSED(rect)
}

void PlaceTrianglePrimitiveTask::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

    if (m_item == nullptr)
        return;

    painter->setBrush(Qt::NoBrush);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0));
    painter->drawLine(m_radiusLine);
    painter->setPen(QPen(scene()->graphicsPalette().color(LeGraphicsPalette::Highlight), 0, Qt::DotLine));
    QRectF bRect = m_item->boundingRect();
    bRect.moveCenter(m_item->pos());
    painter->drawRect(bRect);
}

bool PlaceTrianglePrimitiveTask::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceTrianglePrimitiveTask::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceTrianglePrimitiveTask::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}

bool PlaceTrianglePrimitiveTask::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_state == WaitForP2)
    {
        m_radiusLine.setP2(event->scenePos());
        m_primitive->setSize(QSizeF(qAbs(m_radiusLine.dx())*2.0,
                                    qAbs(m_radiusLine.dy())*2.0));
    }
    return true;
}

bool PlaceTrianglePrimitiveTask::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_state == WaitForP1)
    {
        m_primitive->setPosition(event->scenePos());
        scene()->currentLayer()->addToLayer(m_item);
        m_radiusLine.setP1(event->scenePos());
        m_state = WaitForP2;
        return true;
    }
    else if (m_state == WaitForP2)
    {
        m_state = Finished;
        accept();
    }
    return true;
}

bool PlaceTrianglePrimitiveTask::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
    return true;
}
