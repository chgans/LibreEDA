#ifndef PTHPADCALCULATORWIDGET_H
#define PTHPADCALCULATORWIDGET_H

#include <QWidget>

class QDoubleSpinBox;

namespace Ui {
    class PthPadCalculatorWidget;
}
class PthPadCalculator;

class PthPadCalculatorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PthPadCalculatorWidget(QWidget *parent = 0);
    ~PthPadCalculatorWidget();

private:
    Ui::PthPadCalculatorWidget *m_ui;
    PthPadCalculator *m_calculator;
    void bind(const QString &name, QDoubleSpinBox *spinBox, qreal value);
    void showResult();
};

#endif // PTHPADCALCULATORWIDGET_H
