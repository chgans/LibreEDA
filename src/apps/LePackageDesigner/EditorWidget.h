#ifndef EDITORWIDGET_H
#define EDITORWIDGET_H

#include <QWidget>

class LeGraphicsView;

namespace LDO {
    class Document;
    class IDocumentObject;
}
class MainWindow;
class AbstractTask;

class EditorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit EditorWidget(QWidget *parent = 0);

    virtual void edit(MainWindow *app, LDO::Document *document, LDO::IDocumentObject *object) = 0;

    virtual LDO::IDocumentObject *object() const = 0;
    virtual LDO::Document *document() const = 0;

    virtual QList<AbstractTask*> tasks() const = 0;
    virtual QString title() const = 0;

signals:
    void taskStarted(AbstractTask *task);
    void taskFinished(AbstractTask *task);

public slots:
};

#endif // EDITORWIDGET_H
