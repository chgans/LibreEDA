#include "GraphicsView.h"

#include <QGraphicsScene>
#include <QGraphicsPathItem>
#include <QWheelEvent>

GraphicsView::GraphicsView(QWidget *parent):
    QGraphicsView(parent)
{

}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
	static const qreal zoomInFactor = 1.25;
	static const qreal zoomOutFactor = 1 / zoomInFactor;

	const QPointF oldPos = mapToScene(event->pos());

	qreal zoomFactor;
	if (event->angleDelta().y() > 0)
		zoomFactor = zoomInFactor;
	else
		zoomFactor = zoomOutFactor;
	scale(zoomFactor, zoomFactor);

	const QPointF newPos = mapToScene(event->pos());

	const QPointF delta = newPos - oldPos;
	translate(delta.x(), delta.y());
}
