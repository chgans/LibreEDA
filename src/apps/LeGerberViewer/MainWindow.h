#pragma once

#include <QMainWindow>

class QFileSystemModel;
class QComboBox;

namespace LeGerber
{
	class GraphicsScene;
}

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void onFileSystemViewClicked(const QModelIndex &index);
    void onFileSystemFilterChanged(const QString &text);
    void onViewportUpdateModeComboBoxCurrentIndexChanged(int index);

private:
	static QStringList gerberNameFilters();
	Ui::MainWindow *ui;
	QFileSystemModel *m_fileSystemModel;
	LeGerber::GraphicsScene *m_scene;
	QAction *m_showOutlineAction;
	QAction *m_antialiasingAction;
	QAction *m_openglAction;
    QComboBox *m_viewportUpdateModeComboBox;
};
