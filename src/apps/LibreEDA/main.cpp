
#include "mainwindow.h"
#include "core/core.h"
#include "core/extension/pluginmanager.h"

#include <QApplication>

#include <QtDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("LibreEDA");
    QCoreApplication::setOrganizationDomain("libreeda.org");
    QCoreApplication::setApplicationName("leda");

    QIcon::setThemeName("breeze");

    Core::initialise();

    // Loading plugins
    PluginManager::setPluginIID("org.libre-eda.leda.plugin");
    QStringList pluginPaths;
    pluginPaths << Core::pluginPath();
    PluginManager::setPluginPaths(pluginPaths);
    PluginManager::discoverPlugins();
    PluginManager::loadPlugins();

    // Go!
    MainWindow w;
    w.readSettings();
    w.show();

    return a.exec();
}
