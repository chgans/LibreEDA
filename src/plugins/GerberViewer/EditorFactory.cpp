#include "EditorFactory.h"
#include "Editor.h"
#include "OldGraphicsView/Settings.h"

#include "core/core.h"

namespace GerberViewer
{

    EditorFactory::EditorFactory(QObject *parent) :
        IEditorFactory(parent)
    {
        setId(GERBERVIEWER_ID);
        setDisplayName("Gerber viewer");

        // FIXME: We support a plethora of extensions
        // consider using glob patterns or proper mime types
        QStringList extensions;
        extensions << "gbr" << "gtl" << "gbl"
                << "gts" << "gbs" << "gto"
                << "gbo" << "gtp" << "gbp"
                << "gko" << "gm1" << "ger"
                << "gpb" << "gpt" << "gvp"
                << "gbx";
        for (int i = 0; i < 9; i++)
        {
            extensions.append(QString("g%1").arg(i));
            extensions.append(QString("gd%1").arg(i));
            extensions.append(QString("gg%1").arg(i));
            extensions.append(QString("gl%1").arg(i));
            extensions.append(QString("gm%1").arg(i));
            extensions.append(QString("gp%1").arg(i));
        }
        setFileExtensions(extensions);
    }

    void EditorFactory::applySettings()
    {
        LeGraphicsView::Settings settings;
        settings.load(Core::settings());
        for (auto editor : m_editors)
        {
            editor->applySettings(settings);
        }
    }

    IEditor *EditorFactory::createEditor()
    {
        auto editor = new Editor();
        m_editors.append(editor);
        connect(editor, &QObject::destroyed,
                this, [this, editor](QObject *)
        {
            m_editors.removeOne(editor);
        });

        LeGraphicsView::Settings settings;
        settings.load(Core::settings());
        editor->applySettings(settings);

        return editor;
    }

}
