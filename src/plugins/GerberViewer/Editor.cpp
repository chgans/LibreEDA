#include "Editor.h"
#include "Document.h"

#include "LeGerber/graphicsscene.h"

#include "OldGraphicsView/View.h"
#include "OldGraphicsView/Palette.h"
#include "OldGraphicsView/PaletteLoader.h"
#include "OldGraphicsView/Settings.h"

#include "core/core.h"

namespace GerberViewer
{

    Editor::Editor(QObject *parent) :
        IEditor(parent),
        m_document(nullptr),
        m_view(new LeGraphicsView::View)
    {
        setWidget(m_view);
    }

    Editor::~Editor()
    {
    }

    void Editor::applySettings(const LeGraphicsView::Settings &settings)
    {
        //m_document->scene()->applySettings(settings);
        m_view->applySettings(settings);

        // FIXME
        LeGraphicsView::PaletteLoader paletteLoader;
        paletteLoader.setPath(Core::dataPath("/settings/symboleditor"));
        paletteLoader.loadPalettes();
        LeGraphicsView::Palette palette = paletteLoader.palette(settings.paletteName);
        //m_document->scene()->setPalette(palette);
        m_view->setPalette(palette);
    }

    bool Editor::open(QString *errorString, const QString &fileName)
    {
        m_document = new Document(this);

        if (!m_document->open(errorString, fileName))
        {
            return false;
        }

        m_document->scene()->setPalette(m_view->palette());
        m_view->setScene(m_document->scene());
        m_view->fitInView(m_document->scene()->sceneRect(), Qt::KeepAspectRatio);
        return true;
    }

    IDocument *Editor::document() const
    {
        return m_document;
    }

    QIcon Editor::icon() const
    {
        return QIcon(":/icons/gerberviewer.png");
    }

    QString Editor::displayName() const
    {
        return m_document->displayName();
    }

    void Editor::activate(QMainWindow *win)
    {
        Q_UNUSED(win);
    }

    void Editor::desactivate(QMainWindow *win)
    {
        Q_UNUSED(win);
    }

}
