#pragma once

#include "GerberViewer.h"
#include "core/extension/iplugin.h"

namespace GerberViewer
{

    class EditorFactory;
    class AppearanceSettingsPage;

    class GERBERVIEWER_EXPORT Plugin : public IPlugin
    {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.libre-eda.leda.plugin" FILE "GerberViewer.json")

    public:
        explicit Plugin(QObject *parent = nullptr);
        ~Plugin();

        bool initialize(const QStringList &arguments, QString *errorString);
        void extensionsInitialized();
        void shutdown();

    public slots:
        void applySettings();

    private:
        static void registerMetaTypes();
        EditorFactory *m_editorFactory;
        AppearanceSettingsPage *m_settingsPage;
    };

}
