
#pragma once

#include <QtCore/qglobal.h>

#if defined(GERBERVIEWER_LIBRARY)
#  define GERBERVIEWER_EXPORT Q_DECL_EXPORT
#else
#  define GERBERVIEWER_EXPORT Q_DECL_IMPORT
#endif

namespace GerberViewer
{
    const char GERBERVIEWER_ID[] = "leda.gerberviewer";
    const char GERBERVIEWER_EXT[] = "LeGerber";
}