import qbs 1.0

LedaPlugin {
    name: "GerberViewer"

    cpp.defines: base.concat(["GERBERVIEWER_PLUGIN"])

    Depends { name: "Qt"; submodules: ["core", "gui"] }
    Depends { name: "Core" }
    Depends { name: "OldGraphicsView" }
    Depends { name: "LeGerber" }

    files: [
        "Document.cpp",
        "Document.h",
        "GerberViewer.h",
        "GerberViewer.qrc",
        "Plugin.h",
        "Plugin.cpp",
        "EditorFactory.h",
        "EditorFactory.cpp",
        "Editor.h",
        "Editor.cpp",
    ]
}
