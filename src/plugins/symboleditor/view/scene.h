#pragma once

#include <QGraphicsScene>

#include "OldGraphicsView/Scene.h"

namespace SymbolEditor
{

    class Item;
    class GraphicsItem;

    class Scene : public LeGraphicsView::Scene
    {
        Q_OBJECT

    public:
        explicit Scene(QObject *parent = nullptr);
        ~Scene();

        QList<GraphicsItem *> selectedObjects();
        GraphicsItem *itemForDocumentId(quint64 id) const;
        quint64 documentIdForItem(GraphicsItem *item) const;

        // These should be part of the model API, but the model doesn't have spatial query capabilities
        QList<GraphicsItem *> documentItems(const QPainterPath &path, Qt::ItemSelectionMode mode, Qt::SortOrder order);
        GraphicsItem *lowestObscuringItem(GraphicsItem *item);
        GraphicsItem *highestObscuringItem(GraphicsItem *item);
        GraphicsItem *highestObscuredItem(GraphicsItem *item);
        GraphicsItem *lowestObscuredItem(GraphicsItem *item);

    public slots:
        void addDocumentItem(quint64 id, const Item *item);
        void updateDocumentItemProperty(quint64 itemId, quint64 propertyId, const QVariant &value);
        void removeDocumentItem(quint64 id);

    private:
        QMap<quint64, GraphicsItem *> m_itemMap;
        QPen makePen(const Item *item);
        QBrush makeBrush(const Item *item);
    };

}
