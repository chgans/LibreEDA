#include "snap/nosnapstrategy.h"

namespace SymbolEditor
{

    NoSnapStrategy::NoSnapStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setLabel("No <b>S</b>nap");
        setShortcut(QKeySequence("s,f"));
        // TODO: Set display name "Free"
        setIcon(QIcon(":/icons/snap/snap-free.svg")); // FIXME: use theme
        updateAction();
    }

    SnapResult NoSnapStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        Q_UNUSED(maxDistance);

        SnapResult result;
        result.pos = mousePos;
        result.distance = 0;
        result.item = nullptr;

        return result;
    }
}
