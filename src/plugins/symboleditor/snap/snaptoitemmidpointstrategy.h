#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{

    class SnapToItemMidPointStrategy: public AbstractSnapStrategy
    {
    public:
        explicit SnapToItemMidPointStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
