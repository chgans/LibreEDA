#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{
    class SnapToGridStrategy: public AbstractSnapStrategy
    {
    public:
        explicit SnapToGridStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
