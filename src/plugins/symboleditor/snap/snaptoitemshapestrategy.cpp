#include "snap/snaptoitemshapestrategy.h"
#include "item/item.h"

#include <QMultiMap>
#include <QtGlobal>

namespace SymbolEditor
{

    SnapToItemShapeStrategy::SnapToItemShapeStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setName("shape");
        setLabel("<b>S</b>nap to <b>S</b>hape");
        setShortcut(QKeySequence("s,s"));
        setIcon(QIcon(":/icons/snap/snap-shape.svg"));
        updateAction();
    }

    SnapResult SnapToItemShapeStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        SnapCandidates candidates;

        for (auto item: items(mousePos, maxDistance))
        {
            candidates.insert(item, item->nearestPoints(mousePos));
        }

        return AbstractSnapStrategy::snap(mousePos, candidates);
    }
}
