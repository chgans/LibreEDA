#include "snap/snaptoitemendpointstrategy.h"
#include "item/item.h"

#include <QMultiMap>
#include <QtGlobal>

namespace SymbolEditor
{

    SnapToItemEndPointStrategy::SnapToItemEndPointStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setName("end");
        setLabel("<b>S</b>nap to <b>E</b>nd-points");
        setShortcut(QKeySequence("s,e"));
        setIcon(QIcon(":/icons/snap/snap-end-point.svg"));
        updateAction();
    }

    SnapResult SnapToItemEndPointStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        SnapCandidates candidates;

        for (auto item: items(mousePos, maxDistance))
        {
            candidates.insert(item, item->endPoints());
        }

        return AbstractSnapStrategy::snap(mousePos, candidates);
    }
}
