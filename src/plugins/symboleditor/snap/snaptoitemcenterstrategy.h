#pragma once

#include "abstractsnapstrategy.h"

namespace SymbolEditor
{

    class SnapToItemCenterStrategy: public AbstractSnapStrategy
    {
    public:
        explicit SnapToItemCenterStrategy(QObject *parent = nullptr);

        // AbstractSnapStrategy interface
    public:
        SnapResult snap(QPointF mousePos, qreal maxDistance) override;
    };

}
