#include "snap/snaptoitemmidpointstrategy.h"
#include "item/item.h"

#include <QMultiMap>
#include <QtGlobal>

namespace SymbolEditor
{

    SnapToItemMidPointStrategy::SnapToItemMidPointStrategy(QObject *parent):
        AbstractSnapStrategy(parent)
    {
        setName("mid");
        setLabel("<b>S</b>nap to <b>M</b>id-points");
        setShortcut(QKeySequence("s,m"));
        setIcon(QIcon(":/icons/snap/snap-mid-point.svg"));
        updateAction();
    }

    SnapResult SnapToItemMidPointStrategy::snap(QPointF mousePos, qreal maxDistance)
    {
        SnapCandidates candidates;

        for (auto item: items(mousePos, maxDistance))
        {
            candidates.insert(item, item->midPoints());
        }

        return AbstractSnapStrategy::snap(mousePos, candidates);
    }
}
