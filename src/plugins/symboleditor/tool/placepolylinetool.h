#pragma once

#include "tool/placementtool.h"

namespace SymbolEditor
{

    class GraphicsPolylineItem;

    class PlacePolyineTool : public PlacementTool
    {
        Q_OBJECT

    public:
        explicit PlacePolyineTool(QObject *parent = nullptr);

    private:
        GraphicsPolylineItem *m_item;

    private:
        void setP1(const QPointF &pos);
        void setP2(const QPointF &pos);

        // PlacementTool interface
    public:
        GraphicsItem *beginInsert(const QPointF &pos) override;
        void addPoint(int idx, const QPointF &pos) override;
        void freezePoint(int idx, const QPointF &pos) override;
        bool removePoint(int idx, const QPointF &pos) override;
        void movePoint(int idx, const QPointF &pos) override;
        void endInsert(const QPointF &pos) override;
        void cancelInsert() override;
    };

}
