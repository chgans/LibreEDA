#pragma once

#include "tool/interactivetool.h"

#include <QPointF>

class QRubberBand;

namespace SymbolEditor
{

    class DragSelectTool: public InteractiveTool
    {
    public:
        DragSelectTool(QObject *parent = nullptr);

    private:
        QPointF m_pressPosition;
        QPointF m_lastPosition;
        QRubberBand *m_rubberBand;

        // InteractiveTool interface
    public:
        void mousePressEvent(QMouseEvent *event) override;
        void mouseMoveEvent(QMouseEvent *event) override;
        void mouseReleaseEvent(QMouseEvent *event) override;

        // Tool interface
    public:
        void activate() override;
        void desactivate() override;
    };

}
