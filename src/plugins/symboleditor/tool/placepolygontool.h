#pragma once

#include "tool/placementtool.h"

namespace SymbolEditor
{

    class GraphicsPolygonItem;

    class PlacePolygonTool : public PlacementTool
    {
        Q_OBJECT

    public:
        explicit PlacePolygonTool(QObject *parent = nullptr);
        ~PlacePolygonTool();

    private:
        GraphicsPolygonItem *m_item;

        // PlacementTool interface
    public:
        GraphicsItem *beginInsert(const QPointF &pos) override;
        void addPoint(int idx, const QPointF &pos) override;
        void freezePoint(int idx, const QPointF &pos) override;
        bool removePoint(int idx, const QPointF &pos) override;
        void movePoint(int idx, const QPointF &pos) override;
        void endInsert(const QPointF &pos) override;
        void cancelInsert() override;
    };

}
