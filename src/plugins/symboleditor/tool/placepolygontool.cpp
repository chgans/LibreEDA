#include "tool/placepolygontool.h"
#include "item/polygonitem.h"
#include "xdl/symbol.h"
#include "command/additemcommand.h"

#include <QPolygonF>
#include <QAction>

namespace SymbolEditor
{

    PlacePolygonTool::PlacePolygonTool(QObject *parent):
        PlacementTool(parent),
        m_item(nullptr)
    {
        QAction *action = new QAction(QIcon::fromTheme("draw-polygon"),
                                      "<b>P</b>lace a Poly<b>g</b>on <i>p,g</i>", nullptr);
        action->setShortcut(QKeySequence("p,g"));
        setAction(action);
        setToolGroup("interactive-tools");
    }

    PlacePolygonTool::~PlacePolygonTool()
    {

    }

    GraphicsItem *PlacePolygonTool::beginInsert(const QPointF &pos)
    {
        m_item = new GraphicsPolygonItem();
        m_item->setPos(pos);
        return m_item;
    }

    void PlacePolygonTool::addPoint(int idx, const QPointF &pos)
    {
        Q_UNUSED(idx);
        Q_UNUSED(pos);
        QPointF itemPos = m_item->mapFromScene(pos);
        m_item->addPoint(itemPos);
    }

    void PlacePolygonTool::freezePoint(int idx, const QPointF &pos)
    {
        Q_UNUSED(pos);
        Q_UNUSED(idx);
    }

    bool PlacePolygonTool::removePoint(int idx, const QPointF &pos)
    {
        Q_UNUSED(pos);
        QPolygonF poly = m_item->polygon();
        QPointF point = poly[idx];
        poly.removeAt(idx);
        if (poly.count() > 1)
        {
            poly[idx - 1] = point;
            m_item->setPolygon(poly);
            return true; // Keep going
        }
        else
        {
            return false;    // Remove and delete polygon
        }
    }

    void PlacePolygonTool::movePoint(int idx, const QPointF &pos)
    {
        QPointF itemPos = m_item->mapFromScene(pos);
        m_item->movePoint(idx, itemPos);
    }

    void PlacePolygonTool::endInsert(const QPointF &pos)
    {
        Q_UNUSED(pos);

        QList<QPointF> vertices = m_item->polygon().toList();

        auto command = new AddItemCommand();
        command->setItemType(Polygon);
        command->setItemProperty(PositionProperty, m_item->pos());
        command->setItemProperty(VerticesProperty, QVariant::fromValue<QList<QPointF>>(vertices));
        command->setItemProperty(RotationProperty, m_item->rotation());
        command->setItemProperty(OpacityProperty, m_item->opacity()*100);
        command->setItemProperty(XMirroredProperty, m_item->isXMirrored());
        command->setItemProperty(YMirroredProperty, m_item->isYMirrored());
        command->setItemProperty(LockedProperty, !m_item->isEnabled());
        command->setItemProperty(VisibilityProperty, m_item->isVisible());
        command->setItemProperty(LineColorProperty, m_item->lineColor());
        command->setItemProperty(LineWidthProperty, m_item->lineWidth());
        command->setItemProperty(LineStyleProperty, m_item->lineStyle());
        command->setItemProperty(FillColorProperty, m_item->fillColor());
        command->setItemProperty(FillStyleProperty, m_item->fillStyle());
        emit commandRequested(command);

        delete m_item;
        m_item = nullptr;

        resetTool();
    }

    void PlacePolygonTool::cancelInsert()
    {
    }

}
