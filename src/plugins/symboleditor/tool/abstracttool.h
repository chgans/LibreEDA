#pragma once

#include "symboleditor_global.h"

#include <QObject>
#include <QIcon>
#include <QKeySequence>

class QGraphicsSceneMouseEvent;
class QAction;

namespace SymbolEditor
{

    class UndoCommand;
    class Scene;

    class AbstractTool : public QObject
    {
        Q_OBJECT

    public:
        AbstractTool(QObject *parent);
        ~AbstractTool();

        virtual void activate(Scene *scene) = 0;
        virtual void desactivate() = 0;

        QAction *action() const;

        QList<QWidget *> widgets() const;

        UndoCommand *nextPendingCommand();
        bool hasPendingCommand() const;

        virtual bool mouseClickEvent(QGraphicsSceneMouseEvent *event);
        virtual bool mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
        virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    signals:
        void newCommandAvailable();

    private:
        QAction *m_action;
        QString m_label;
        QIcon m_icon;
        QKeySequence m_shortcut;
        Scene *m_scene;
        QList<UndoCommand *> m_pendingCommands;
        QList<QWidget *> m_widgets;

    protected:
        void setLabel(const QString &label);
        void setShortcut(const QKeySequence &shortcut);
        void setIcon(const QIcon &icon);
        void updateAction();
        void setWidgets(const QList<QWidget *> &widgets);

        void setScene(Scene *scene);
        Scene *scene() const;

        void appendCommand(UndoCommand *command);
    };

}

