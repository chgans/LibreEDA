#pragma once

#include "tool/placementtool.h"

namespace SymbolEditor
{

    class GraphicsCircularArcItem;

    class PlaceArcTool : public PlacementTool
    {
        Q_OBJECT

    public:
        explicit PlaceArcTool(QObject *parent = nullptr);
        ~PlaceArcTool();

    private:
        GraphicsCircularArcItem *m_item;

        // PlacementTool interface
    public:
        GraphicsItem *beginInsert(const QPointF &pos) override;
        void addPoint(int idx, const QPointF &pos) override;
        void freezePoint(int idx, const QPointF &pos) override;
        bool removePoint(int idx, const QPointF &pos) override;
        void movePoint(int idx, const QPointF &pos) override;
        void endInsert(const QPointF &pos) override;
        void cancelInsert() override;
    };

}
