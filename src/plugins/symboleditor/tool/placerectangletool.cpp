#include "placerectangletool.h"
#include "item/rectangleitem.h"
#include "command/additemcommand.h"
#include "view/scene.h"

#include <QGraphicsSceneMouseEvent>

namespace SymbolEditor
{

    PlaceRectangleTool::PlaceRectangleTool(QObject *parent):
        AbstractTool(parent),
        m_state(WaitingForActivation),
        m_item(nullptr)
    {
        setLabel("<b>P</b>lace a <b>R</b>ectangle");
        setShortcut(QKeySequence("p,r"));
        setIcon(QIcon::fromTheme("draw-rectangle"));
        updateAction();
    }

    PlaceRectangleTool::~PlaceRectangleTool()
    {
    }

    void PlaceRectangleTool::addItem()
    {
        m_item = new GraphicsRectangleItem();
        m_item->setEnabled(false);
        scene()->addItem(m_item);
    }

    void PlaceRectangleTool::updateItem()
    {
        auto rect = QRectF(m_p1, m_p2).normalized();
        m_item->setProperty(PositionProperty, rect.topLeft());
        m_item->setProperty(WidthProperty, rect.width());
        m_item->setProperty(HeightProperty, rect.height());
    }

    void PlaceRectangleTool::removeItem()
    {
        scene()->removeItem(m_item);
        delete m_item;
    }

    bool PlaceRectangleTool::mouseClickEvent(QGraphicsSceneMouseEvent *event)
    {
        switch (m_state)
        {
            case WaitingForActivation:
                break;
            case WaitingForFirstPoint:
            {
                m_p1 = m_p2 = event->scenePos();
                addItem();
                updateItem();
                m_state = WaitingForSecondPoint;
                break;
            }
            case WaitingForSecondPoint:
            {
                m_p2 = event->scenePos();
                updateItem();
                m_item->setEnabled(true);

                auto command = new AddItemCommand();
                command->setItemType(Rectangle); // FIXME: typeForDocument()
                command->setItemProperties(m_item->properties());
                appendCommand(command);

                removeItem();
                m_state = WaitingForFirstPoint;
                break;
            }
        }
        return true;
    }

    bool PlaceRectangleTool::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        switch (m_state)
        {
            case WaitingForActivation:
                break;
            case WaitingForFirstPoint:
                break;
            case WaitingForSecondPoint:
                m_p2 = event->scenePos();
                updateItem();
                break;
        }
        return true;
    }

    void PlaceRectangleTool::activate(Scene *scene)
    {
        if (m_state != WaitingForActivation)
        {
            desactivate();
        }
        setScene(scene);
        m_state = WaitingForFirstPoint;
    }

    void PlaceRectangleTool::desactivate()
    {
        switch (m_state)
        {
            case WaitingForActivation:
            case WaitingForFirstPoint:
                break;
            case WaitingForSecondPoint:
                removeItem();
                break;
        }
        setScene(nullptr);
        m_state = WaitingForActivation;
    }

}
