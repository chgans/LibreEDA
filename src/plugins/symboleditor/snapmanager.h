#pragma once

#include "snap/abstractsnapstrategy.h"
#include "OldGraphicsView/Settings.h"
#include <QObject>
#include <QMap>
#include <QPointF>

class QAction;
class QActionGroup;
class QGraphicsSimpleTextItem;

namespace SymbolEditor
{
    class Scene;
    class View;
    class GraphicsItem;
    class AbstractSnapStrategy;

    class SnapManager : public QObject
    {
        Q_OBJECT
        Q_PROPERTY(int snapRange READ snapRange WRITE setSnapRange NOTIFY snapRangeChanged)

    public:
        explicit SnapManager(QObject *parent = nullptr);
        ~SnapManager();

        QList<QAction *> actions() const;

        int snapRange() const;

    public slots:
        void setScene(Scene *scene);
        void setView(View *view);
        void applySettings(const LeGraphicsView::Settings &settings);
        void setSnapRange(int snapRange);

    signals:
        void snapRangeChanged(int snapRange);

    private:
        QActionGroup *m_actionGroup = nullptr;
        QMap<QAction *, AbstractSnapStrategy*> m_actionToStrategy;
        QList<QAction *> m_actions;
        QList<AbstractSnapStrategy *> m_strategies;
        AbstractSnapStrategy *m_activeStrategy = nullptr;
        AbstractSnapStrategy *m_defaultStrategy = nullptr;
        Scene *m_scene = nullptr;
        View *m_view = nullptr;
        SnapResult m_result;
        QGraphicsSimpleTextItem *m_indicatorItem;
        int m_snapRange;

        void addHighlightEffect(GraphicsItem *item);
        void removeHighlightEffect(GraphicsItem *item);

    private slots:
        void setActiveStrategy(AbstractSnapStrategy *strategy);

        // QObject interface
    protected:
        bool eventFilter(QObject *watched, QEvent *event) override;
    };

}

