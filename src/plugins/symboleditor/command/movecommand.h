#pragma once

#include "command.h"

#include <QList>
#include <QPointF>

namespace SymbolEditor
{

    class TranslateCommand: public UndoCommand
    {
    public:
        TranslateCommand(UndoCommand *parent = nullptr);

        QList<quint64> itemIdList;
        QPointF amount;

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

}
