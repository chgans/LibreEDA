#include "distributecommand.h"
#include "document.h"

#include <QDebug>

namespace SymbolEditor
{

    DistributeCommand::DistributeCommand(UndoCommand *parent):
        UndoCommand(parent)
    {
    }

    DistributeCommand::~DistributeCommand()
    {
    }

    void DistributeCommand::setItems(const QList<quint64> &idList)
    {
        m_itemIdList = idList;
    }

    void DistributeCommand::setDistribution(Distribution distribution)
    {
        m_distribution = distribution;
    }

    void DistributeCommand::setDistance(qreal distance)
    {
        m_distance = distance;
    }

    QRectF DistributeCommand::alignmentRect(const Item *item)
    {
        // FIXME: Should be return item->mapToModel(item->outline()).boundingRect();
        QTransform xform;
        xform.translate(item->position().x(), item->position().y());
        xform.rotate(item->rotation());
        if (item->isXMirrored())
        {
            xform.scale(-1, 1);
        }
        if (item->isYMirrored())
        {
            xform.scale(1, -1);
        }
        return xform.map(item->outline()).boundingRect();
    }

    void DistributeCommand::undo()
    {
        if (m_itemIdList.count() < 2)
        {
            return;
        }

        for (int i = 1; i < m_itemIdList.count(); i++)
        {
            auto itemId = m_itemIdList.value(i);
            auto item = document()->item(itemId);
            auto delta = m_deltaMap.value(itemId);
            auto pos = item->position() - delta;
            document()->setItemProperty(itemId, PositionProperty, pos);
        }
    }

    void DistributeCommand::redo()
    {
        setText(QString("Distribute %1 item(s)").arg(m_itemIdList.count()));

        if (m_itemIdList.count() < 2)
        {
            return;
        }

        auto refItemId = m_itemIdList.first();
        auto refItem = document()->item(refItemId);
        auto refRect = alignmentRect(refItem);

        for (int i = 1; i < m_itemIdList.count(); i++)
        {
            auto itemId = m_itemIdList.value(i);
            auto item = document()->item(itemId);
            auto itemRect = alignmentRect(item);

            qreal distance = i * m_distance;
            QPointF delta;
            switch (m_distribution)
            {
                case DistributeLeftBorders:
                    delta.setX(distance + refRect.left() - itemRect.left());
                    break;
                case DistributeHCenters:
                    delta.setX(distance + refRect.center().x() - itemRect.center().x());
                    break;
                case DistributeHGaps:
                    delta.setX(distance + refRect.right() - itemRect.left());
                    break;
                case DistributeRightBorders:
                    delta.setX(distance + refRect.right() - itemRect.right());
                    break;
                case DistributeTopBorders:
                    delta.setY(distance + refRect.top() - itemRect.top());
                    break;
                case DistributeVCenters:
                    delta.setY(distance + refRect.center().y() - itemRect.center().y());
                    break;
                case DistributeVGaps:
                    delta.setY(distance + refRect.bottom() - itemRect.top());
                    break;
                case DistributeBottomBorders:
                    delta.setY(distance + refRect.bottom() - itemRect.bottom());
                    break;
            }

            m_deltaMap.insert(itemId, delta);
            auto pos = item->position() + delta;
            document()->setItemProperty(itemId, PositionProperty, pos);
        }
    }

}
