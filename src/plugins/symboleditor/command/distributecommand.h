#pragma once

#include "command.h"

#include <QMap>

namespace SymbolEditor {

    class Item;

    class DistributeCommand : public UndoCommand
    {
    public:
        DistributeCommand(UndoCommand *parent = nullptr);
        ~DistributeCommand();

        void setItems(const QList<quint64> &idList);
        void setDistribution(Distribution distribution);
        void setDistance(qreal distance);

    private:
        QList<quint64> m_itemIdList;
        Distribution m_distribution;
        qreal m_distance;
        QMap<quint64, QPointF> m_deltaMap;
        static QRectF alignmentRect(const Item *item);

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

} // namespace SymbolEditor

