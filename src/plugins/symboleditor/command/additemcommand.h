#pragma once

#include "command.h"
#include "xdl/symbol.h"

#include <QUndoCommand>
#include <QVariant>


namespace SymbolEditor
{

    class AddItemCommand: public UndoCommand
    {
    public:
        AddItemCommand(UndoCommand *parent = nullptr);

        void setItemType(ItemType type);
        void setItemProperty(PropertyId id, const QVariant &value);
        void setItemProperties(const QMap<PropertyId, QVariant> &properties);

    private:
        ItemType m_type;
        QMap<PropertyId, QVariant> m_properties;
        quint64 m_id;

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

}
