#pragma once

#include "command.h"

#include <QMap>

namespace SymbolEditor {

    class Item;

    class AlignCommand : public UndoCommand
    {
    public:
        AlignCommand(UndoCommand *parent = nullptr);
        ~AlignCommand();

        void setItems(const QList<quint64> &idList);
        void setAlignment(Qt::Alignment alignment);

    private:
        Qt::Alignment m_alignment;
        QList<quint64> m_itemIdList;
        QMap<quint64, QPointF> m_deltaMap;
        static QRectF alignmentRect(const Item *item);

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

} // namespace SymbolEditor

