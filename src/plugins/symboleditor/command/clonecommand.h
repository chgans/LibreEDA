#pragma once

#include "command.h"

#include <QList>
#include <QPointF>

namespace SymbolEditor
{

    class CloneCommand: public UndoCommand
    {
    public:
        CloneCommand(UndoCommand *parent = nullptr);

        QList<quint64> itemIdList;
        QPointF translation;

    private:
        QList<quint64> cloneIdList;

        // QUndoCommand interface
    public:
        void undo() override;
        void redo() override;
    };

}
