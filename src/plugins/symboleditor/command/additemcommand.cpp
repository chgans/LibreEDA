#include "additemcommand.h"
#include "document.h"

namespace SymbolEditor
{

    AddItemCommand::AddItemCommand(UndoCommand *parent):
        UndoCommand(parent),
        m_id(0)
    {

    }

    void AddItemCommand::undo()
    {
        document()->removeItem(m_id);
    }

    void AddItemCommand::redo()
    {
        m_id = document()->addItem(m_type, m_id);
        setText(QString("Add %1").arg(document()->friendlyItemTypeName(m_id)));
        for (auto propId: m_properties.keys())
        {
            document()->setItemProperty(m_id, propId, m_properties.value(propId));
        }
    }

    void AddItemCommand::setItemType(ItemType type)
    {
        m_type = type;
    }

    void AddItemCommand::setItemProperty(PropertyId id, const QVariant &value)
    {
        m_properties[id] = value;
    }

    void AddItemCommand::setItemProperties(const QMap<PropertyId, QVariant> &properties)
    {
        m_properties = properties;
    }

}
