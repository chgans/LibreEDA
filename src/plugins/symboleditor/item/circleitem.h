#pragma once

#include "item/item.h"

namespace SymbolEditor
{

    class GraphicsCircleItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + Circle };

        enum HandleId
        {
            RadiusHandle = 0,
            NbHandles
        };

        explicit GraphicsCircleItem(GraphicsItem *parent = nullptr);
        ~GraphicsCircleItem();

        qreal radius() const;
        void setRadius(qreal length);

    private:
        qreal m_radius;

        // SchItem interface
    public:
        GraphicsItem *clone() override;
        void itemNotification(IObservableItem *item) override;

        QList<PropertyId> propertyIdList() const override;
        void setProperty(PropertyId id, const QVariant &value) override;
        QVariant property(PropertyId id) const override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
        int type() const override { return Type; }


        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    };

}
