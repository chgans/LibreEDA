#pragma once

#include "item/item.h"

namespace SymbolEditor
{

    class GraphicsPolylineItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + Polyline };

        enum HandleId
        {
            P1Handle = 0,
            P2Handle,
            NbHandles
        };
        explicit GraphicsPolylineItem(GraphicsItem *parent = nullptr);

        QLineF line() const;
        void setLine(const QLineF &line);

    private:
        QLineF m_line;

        // SchItem interface
    public:
        GraphicsItem *clone() override;
        void setProperty(PropertyId id, const QVariant &value) override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;

        // IGraphicsItemObserver
    public:
        void itemNotification(IObservableItem *item) override;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
        int type() const override { return Type; }

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    };

}
