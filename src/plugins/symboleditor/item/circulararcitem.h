#pragma once

#include "item/item.h"

// TODO: Pie vs chord vs whole shape
// TODO: keep ratio (ellipse vs circle)

namespace SymbolEditor
{

    class GraphicsCircularArcItem : public GraphicsItem
    {
    public:
        enum { Type = ModelType + CircularArc };

        enum HandleId
        {
            RadiusHandle = 0,
            StartAngleHandle,
            SpanAngleHandle
        };

        explicit GraphicsCircularArcItem(GraphicsItem *parent = nullptr);
        ~GraphicsCircularArcItem();

        qreal radius() const;
        void setRadius(qreal radius);
        qreal startAngle() const;
        void setStartAngle(qreal angle);
        qreal spanAngle() const;
        void setSpanAngle(qreal angle);

    private:
        void addHandles();
        void updateHandles();
        QPointF pointAt(qreal angle) const;
        qreal angleAt(const QPointF &pos) const;
        QRectF rect() const;
        qreal m_radius;
        qreal m_startAngle;
        qreal m_spanAngle;
        bool isFullCircle() const;
        mutable QRectF m_boundingRect;

        // QGraphicsItem interface
    public:
        QRectF boundingRect() const override;
        QPainterPath shape() const override;
        void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
        int type() const override { return Type; }

        // QGraphicsItem interface
    protected:
        QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;

        // IGraphicsItemObserver interface
    public:
        void itemNotification(IObservableItem *item) override;

        // SchItem interface
    public:
        GraphicsItem *clone() override;
        void setProperty(PropertyId id, const QVariant &value) override;

        QList<QPointF> endPoints() const override;
        QList<QPointF> midPoints() const override;
        QList<QPointF> centerPoints() const override;
        QList<QPointF> nearestPoints(QPointF pos) const override;
    };

}
