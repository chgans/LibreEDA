#include "dockwidget.h"
#include "OldGraphicsView/Settings.h"

namespace SymbolEditor
{

    DockWidget::DockWidget(QWidget *parent, Qt::WindowFlags flags):
        QDockWidget(parent, flags)
    {

    }

    void DockWidget::applySettings(const LeGraphicsView::Settings &settings)
    {
        Q_UNUSED(settings);
    }

}
