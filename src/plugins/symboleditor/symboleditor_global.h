#ifndef LOGVIEWER_GLOBAL_H
#define LOGVIEWER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SYMBOLEDITOR_LIBRARY)
#  define SYMBOLEDITOR_EXPORT Q_DECL_EXPORT
#else
#  define SYMBOLEDITOR_EXPORT Q_DECL_IMPORT
#endif


enum Distribution
{
    DistributeLeftBorders,
    DistributeHCenters,
    DistributeHGaps,
    DistributeRightBorders,
    DistributeTopBorders,
    DistributeVCenters,
    DistributeVGaps,
    DistributeBottomBorders
};

enum StackOrderOperation
{
    StackToTop = 0,
    StackUp,
    StackDown,
    StackToBottom
};

#endif // LOGVIEWER_GLOBAL_H
