#ifndef LOGVIEWER_GLOBAL_H
#define LOGVIEWER_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LOGVIEWER_LIBRARY)
#  define LOGVIEWER_EXPORT Q_DECL_EXPORT
#else
#  define LOGVIEWER_EXPORT Q_DECL_IMPORT
#endif

#endif // LOGVIEWER_GLOBAL_H
