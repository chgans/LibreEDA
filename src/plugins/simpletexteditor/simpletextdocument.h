#ifndef SIMPLETEXTDOCUMENT_H
#define SIMPLETEXTDOCUMENT_H

#include "simpletexteditor_global.h"

#include "core/editormanager/idocument.h"

class QTextDocument;

class SIMPLETEXTEDITOR_EXPORT SimpleTextDocument : public IDocument
{
    Q_OBJECT

public:
    SimpleTextDocument();
    bool load(QString *errorString, const QString &fileName);

    QTextDocument *textDocument();

    // IDocument interface
public:
    bool save(QString *errorString, const QString &fileName);
    void render(QPainter *painter);

private:
    QTextDocument *m_textDocument;
};

#endif // SIMPLETEXTDOCUMENT_H
