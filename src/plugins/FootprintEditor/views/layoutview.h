
#pragma once

#include "OldGraphicsView/View.h"

namespace FootprintEditor
{

    class Scene;
    class GraphicsItem;

    class LayoutView : public LeGraphicsView::View
    {
        Q_OBJECT
    public:
        explicit LayoutView(Scene *scene, QWidget *parent = nullptr);

        Scene *layoutScene() const;

        virtual QColor colorForItem(const GraphicsItem *item) const;
        virtual qreal opacityForItem(const GraphicsItem *item) const;
        virtual bool shouldPaintItem(const GraphicsItem *item) const;
    signals:

    public slots:

    };

}
