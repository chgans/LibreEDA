#include "editor.h"
#include "document.h"
#include "editorwidget.h"
#include "scene.h"
#include "views/mainview.h"
#include "items/graphicsitem.h"

#include "OldGraphicsView/Settings.h"
#include "OldGraphicsView/Palette.h"
#include "OldGraphicsView/PaletteLoader.h"

#include "core/core.h"

#include <QFileInfo>

namespace FootprintEditor
{

    PcbEditor::PcbEditor(QObject *parent):
        IEditor(parent),
        m_document(nullptr)
    {
        setWidget(new PcbEditorWidget());
    }

    PcbEditor::~PcbEditor()
    {

    }

    bool PcbEditor::open(QString *errorString, const QString &fileName)
    {
        m_document = new PcbDocument();
        m_document->setFilePath(fileName);
        QFileInfo fileInfo(fileName);
        m_document->setDisplayName(fileInfo.baseName());
        bool result = m_document->load(errorString, m_document->filePath());
        if (!result)
        {
            return false;
        }
        for (GraphicsItem *item : m_document->items())
        {
            pcbWidget()->scene()->addItem(item);
        }
        QRectF rect = QRectF(QPointF(0, 0), m_document->boardSize());
        rect.moveTop(-rect.center().y());
        rect.moveLeft(-rect.center().x());
        pcbWidget()->scene()->setSceneRect(rect);

        // FIXME
        LeGraphicsView::Settings settings;
        settings.load(Core::settings());

        pcbWidget()->scene()->applySettings(settings);
        pcbWidget()->graphicsView()->applySettings(settings);

        LeGraphicsView::PaletteLoader paletteLoader;
        paletteLoader.setPath(Core::dataPath("/settings/symboleditor"));
        paletteLoader.loadPalettes();
        LeGraphicsView::Palette palette = paletteLoader.palette(settings.paletteName);
        pcbWidget()->graphicsView()->setPalette(palette);
        pcbWidget()->scene()->setPalette(palette);

        return true;
    }

    IDocument *PcbEditor::document() const
    {
        return m_document;
    }

    void PcbEditor::saveState(QSettings *settings) const
    {
        Q_UNUSED(settings);
    }

    bool PcbEditor::restoreState(QSettings *settings)
    {
        Q_UNUSED(settings);
        return true;
    }

    PcbEditorWidget *PcbEditor::pcbWidget() const
    {
        return static_cast<PcbEditorWidget *>(widget());
    }

    QIcon PcbEditor::icon() const
    {
        return QIcon(":/icons/footprint.png");
    }

    QString PcbEditor::displayName() const
    {
        return m_document->displayName();
    }

    void PcbEditor::activate(QMainWindow *mainWindow)
    {
        pcbWidget()->activate(mainWindow);
    }

    void PcbEditor::desactivate(QMainWindow *mainWindow)
    {
        pcbWidget()->desactivate(mainWindow);
    }

}