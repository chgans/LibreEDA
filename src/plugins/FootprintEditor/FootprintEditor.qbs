import qbs 1.0

LedaPlugin {
    name: "FootprintEditor"

    cpp.defines: base.concat(["FOOTPRINTEDITOR_LIBRARY",])

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    //Depends { name: "Utils" }
    Depends { name: "Core" }
    Depends { name: "OldGraphicsView" }

    files: [
        "constants.h",
        "designlayer.cpp",
        "designlayer.h",
        "designlayermanager.cpp",
        "designlayermanager.h",
        "designlayerset.cpp",
        "designlayerset.h",
        "document.cpp",
        "document.h",
        "editor.cpp",
        "editor.h",
        "editorfactory.cpp",
        "editorfactory.h",
        "editorwidget.cpp",
        "editorwidget.h",
        "footprinteditor.qrc",
        "footprinteditor_global.h",
        "layerbar.cpp",
        "layerbar.h",
        "pcbpalette.cpp",
        "pcbpalette.h",
        "pcbpalettemanager.cpp",
        "pcbpalettemanager.h",
        "pcbpalettesettingsdialog.cpp",
        "pcbpalettesettingsdialog.h",
        "pcbpalettesettingsdialog.ui",
        "pcbphysicalboard.cpp",
        "pcbphysicalboard.h",
        "pcbphysicallayer.cpp",
        "pcbphysicallayer.h",
        "pcbsettingspage.cpp",
        "pcbsettingspage.h",
        "plugin.cpp",
        "plugin.h",
        "primitive.cpp",
        "primitive.h",
        "scene.cpp",
        "scene.h",
        "insight/insightdockwidget.cpp",
        "insight/insightdockwidget.h",
        "insight/insightheadsupwidget.cpp",
        "insight/insightheadsupwidget.h",
        "insight/insightlenswidget.cpp",
        "insight/insightlenswidget.h",
        "items/graphicsitem.cpp",
        "items/graphicsitem.h",
        "items/graphicsline.cpp",
        "items/graphicsline.h",
        "items/graphicsrect.cpp",
        "items/graphicsrect.h",
        "views/layoutview.cpp",
        "views/layoutview.h",
        "views/mainview.cpp",
        "views/mainview.h",
        "views/overview.cpp",
        "views/overview.h",
    ]
}
