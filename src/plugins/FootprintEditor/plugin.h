
#pragma once

#include "footprinteditor_global.h"

#include "core/extension/iplugin.h"

namespace FootprintEditor
{

    class FOOTPRINTEDITOR_EXPORT PcbEditorPlugin : public IPlugin
    {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.libre-eda.leda.plugin" FILE "FootprintEditor.json")

    public:
        explicit PcbEditorPlugin(QObject *parent = nullptr);
        ~PcbEditorPlugin();

        bool initialize(const QStringList &arguments, QString *errorString);
        void extensionsInitialized();
        void shutdown();
    };

}