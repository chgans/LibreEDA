
#pragma once

#include <QDialog>

namespace FootprintEditor
{

    namespace Ui {
        class PcbPaletteSettingsDialog;
    }

    class PcbPaletteSettingsDialog : public QDialog
    {
        Q_OBJECT

    public:
        explicit PcbPaletteSettingsDialog(QWidget *parent = nullptr);
        ~PcbPaletteSettingsDialog();

    private:
        Ui::PcbPaletteSettingsDialog *ui;
    };

}