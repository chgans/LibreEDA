
#pragma once

#include "footprinteditor_global.h"

#include "core/editormanager/ieditor.h"

namespace FootprintEditor
{

    class PcbDocument;
    class PcbEditorWidget;

    class FOOTPRINTEDITOR_EXPORT PcbEditor: public IEditor
    {
        Q_OBJECT
    public:
        explicit PcbEditor(QObject *parent = nullptr);
        ~PcbEditor();

        // IEditor interface
    public:
        bool open(QString *errorString, const QString &fileName);
        IDocument *document() const;
        void saveState(QSettings *settings) const;
        bool restoreState(QSettings *settings);
        QIcon icon() const;
        QString displayName() const;
        void activate(QMainWindow *mainWindow);
        void desactivate(QMainWindow *mainWindow);

    private:
        PcbDocument *m_document = nullptr;
        PcbEditorWidget *pcbWidget() const;

    };

}