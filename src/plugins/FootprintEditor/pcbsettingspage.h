
#pragma once

#include "footprinteditor_global.h"

#include "core/settings/isettingspage.h"

namespace FootprintEditor
{

    class FOOTPRINTEDITOR_EXPORT PcbSettingsPage: public ISettingsPage
    {
        Q_OBJECT
    public:
        explicit PcbSettingsPage(QObject *parent = nullptr);

        // ISettingsPage interface
    public:
        QWidget *widget();
        void apply();
        void finish();
    };

}