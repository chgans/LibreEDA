
#pragma once

#include <QList>
#include "pcbphysicallayer.h"

namespace FootprintEditor
{

class PcbPhysicalBoard
{
public:
    PcbPhysicalBoard();

    // shape
    // cutoutShapes
    // layer stack

    void setLayerStack(const QList<PcbPhysicalLayer> &stack);
    QList<PcbPhysicalLayer> layerStack() const;

protected:
    QList<PcbPhysicalLayer> m_layerStack;
};

}