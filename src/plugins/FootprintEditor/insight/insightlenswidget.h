
#pragma once

#include <QFrame>

namespace LeGraphicsView
{
    class View;
}

namespace FootprintEditor
{

    // TODO: Don't use QFrame, use QWidget (see InsightHeadsUpWidget)
    class InsightLensWidget : public QFrame
    {
        Q_OBJECT

    public:
        enum LensShape
        {
            SquareLens = 0,
            RoundLens = 1
        };

        explicit InsightLensWidget(QWidget *parent = nullptr);

        // TODO: Q_PROPERTY

        LensShape lensShape() const;
        QSize lensSize() const;
        int lensZoomLevel() const;
        bool mouseTracking() const;
        bool isLensEnabled() const;

        void moveLensToMousePosition();
        void moveLensContentToMousePosition();

        void setBuddyView(LeGraphicsView::View *view);
        LeGraphicsView::View *buddyView() const;

    signals:

    public slots:
        void setLensShape(LensShape shape);
        void toggleLensShape();
        void setLensSize(const QSize &size);
        void setLensZoomLevel(int percent);
        void setMouseTracking(bool enable);
        bool toggleMouseTracking();
        void setLensEnabled(bool enable);
        bool toggleLensEnabled();

    protected slots:
        void synchroniseViewSettings();

    protected:
        //bool event(QEvent *event);
        bool eventFilter(QObject *watched, QEvent *event);
        void paintEvent(QPaintEvent *event);
        bool handleWheelEvent(QWheelEvent *event);

        //void applyViewSettings();
        void setLineColor(QColor color);
        QColor lineColor() const;

    private:
        LensShape m_shape;
        QSize m_size;
        bool m_mouseTracking;
        bool m_enabled;
        int m_zoomLevel;
        LeGraphicsView::View *m_view;
        LeGraphicsView::View *m_buddyView;

        void fixupPaintingArtifacts();
    };

}
