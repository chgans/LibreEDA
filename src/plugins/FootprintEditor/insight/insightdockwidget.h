
#pragma once

#include <QDockWidget>

namespace LeGraphicsView
{
    class NavigationView;
}

namespace FootprintEditor
{
    class MainView;

    class InsightDockWidget : public QDockWidget
    {
        Q_OBJECT
    public:
        explicit InsightDockWidget(QWidget *parent = nullptr);

        void attachView(MainView *view);

    signals:

    public slots:

    private:
        MainView *m_mainView;
        LeGraphicsView::NavigationView *m_navigationView;
    };

}
