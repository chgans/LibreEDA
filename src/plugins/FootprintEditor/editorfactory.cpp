#include "editorfactory.h"
#include "constants.h"
#include "editorwidget.h"
#include "editor.h"

namespace FootprintEditor
{

    PcbEditorFactory::PcbEditorFactory(QObject *parent):
        IEditorFactory(parent)
    {
        setId(FOOTPRINTEDITOR_ID);
        setFileExtensions(QStringList() << FOOTPRINTEDITOR_EXT);
        setDisplayName("Footprint editor");
    }

    PcbEditorFactory::~PcbEditorFactory()
    {

    }



    IEditor *PcbEditorFactory::createEditor()
    {
        return new PcbEditor();
    }

}
