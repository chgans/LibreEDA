#ifndef CORE_H
#define CORE_H

#include "core_global.h"

#include <QDateTime>
#include <QIcon>
#include <QString>
#include <QUrl>
#include <QVersionNumber>

class QSettings;

class CORE_EXPORT Core
{

public:
    static Core *instance();

    static QSettings *settings();

    static QVersionNumber version();
    static QVersionNumber compatibilityVersion();
    static QString sourceId();
    static bool sourceIdIsTainted();
    static QUrl browsableSourceUrl();
    static QString buildId();
    static QDateTime buildDateTime();
    static QString compilerId();
    static QVersionNumber qtVersion();
    static QVersionNumber qtCompatibilityVersion();

    static QString sourcePath();
    static QString rootPath();
    static QString applicationPath();
    static QString binaryPath();
    static QString privateBinaryPath();
    static QString libraryPath();
    static QString pluginPath();
    static QString dataPath(const QString &suffix = QString());
    static QString documentationPath();

    static void initialise(const QString &rootPath = QString());


    static QIcon icon(const QString &name);

private:
    Core();
    static Core *m_instance;
    static QSettings *m_settings;
    static QString m_rootPath;
};

#endif // CORE_H
