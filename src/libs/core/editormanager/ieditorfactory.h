#ifndef IEDITORFACTORY_H
#define IEDITORFACTORY_H

#include "core/core_global.h"

#include <QObject>

class IEditor;

class CORE_EXPORT IEditorFactory : public QObject
{
    Q_OBJECT

public:
    explicit IEditorFactory(QObject *parent = nullptr);
    ~IEditorFactory();

    QString displayName() const;
    void setDisplayName(const QString &displayName);

    QString id() const;
    void setId(const QString &id);

    virtual IEditor *createEditor() = 0;

    QStringList fileExtensions() const;
    void setFileExtensions(const QStringList &fileExtensions);
private:
    QString m_id;
    QString m_displayName;
    QStringList m_fileExtensions;
};

#endif // IEDITORFACTORY_H
