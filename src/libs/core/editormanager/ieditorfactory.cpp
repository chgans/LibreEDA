#include "ieditorfactory.h"

/*!
  \class IEditorFactory
  \preliminary
  \inmodule LibreEDA
  \ingroup LeEditorManager
  \brief The IEditorFactory class ...
  */

IEditorFactory::IEditorFactory(QObject *parent) : QObject(parent)
{

}

IEditorFactory::~IEditorFactory()
{

}

QString IEditorFactory::displayName() const
{
    return m_displayName;
}

void IEditorFactory::setDisplayName(const QString &displayName)
{
    m_displayName = displayName;
}

QString IEditorFactory::id() const
{
    return m_id;
}

void IEditorFactory::setId(const QString &id)
{
    m_id = id;
}

QStringList IEditorFactory::fileExtensions() const
{
    return m_fileExtensions;
}

void IEditorFactory::setFileExtensions(const QStringList &fileExtension)
{
    m_fileExtensions = fileExtension;
}


