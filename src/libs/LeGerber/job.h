#pragma once

#include "liblegerber_global.h"

#include <QString>
#include <QImage>
#include <QList>
#include <QScopedPointer>

class QIODevice;

#include "graphicsscene.h"

namespace LeGerber
{

	class Command;
	class Object;

	class LEGERBER_EXPORT Job // Gerber Runner?
	{
	public:
		explicit Job(const QString &inputFilePath = QString(),
		             const QString &outputFilePath = QString());
		~Job();

		void setInputFilePath(const QString &path);
		QString inputFilePath() const;
		void setOutputFilePath(const QString &path);
		QString outputFilePath() const;

		QImage output() const;

		bool run();
		bool isError() const;
		QString errorMessage() const;
		QStringList warningMessages() const;
		int warningMessageCount() const;

		int inputByteCount() const;
		qint64 readingTime() const;
		qint64 parsingTime() const;
		qint64 processingTime() const;
		qint64 layingOutTime() const;
		qint64 printingTime() const;
		qint64 savingTime() const;
		int outputByteCount() const;

	protected:
		bool open();
		bool parse();
		bool process();
		bool layout();
		bool print();
		bool save();

		void clear();
		void warning(const QString &text);
		void warning(const QStringList &texts);
		void error(const QString &text);

	private:
		qint64 m_readingTime;
		qint64 m_parsingTime;
		qint64 m_processingTime;
		qint64 m_layingOutTime;
		qint64 m_printingTime;
		qint64 m_savingTime;
		int m_inByteCount;
		int m_outByteCount;
		QString m_inputFilePath;
		QString m_outputFilePath;
		QByteArray m_data;
		QList<Command*> m_commands;
		QList<Object*> m_objects;
		QScopedPointer<GraphicsScene> m_scene;
		QImage m_image;
		QString m_error;
		QStringList m_warnings;
	};

}