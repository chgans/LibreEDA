#include "graphicsitem.h"
#include "graphicsscene.h"

namespace LeGerber
{

	GraphicsItem::GraphicsItem(QGraphicsItem *parent):
	    QGraphicsPathItem(parent),
	    m_polarity(DarkPolarity)
	{
		setPen(Qt::NoPen);
		setBrush(Qt::darkCyan);
	}

	void GraphicsItem::setPolarity(Polarity polarity)
	{
		if (m_polarity == polarity)
			return;

		m_polarity = polarity;
		update(boundingRect());
	}

	Polarity GraphicsItem::polarity() const
	{
		return m_polarity;
	}

	QPen GraphicsItem::penForPolarity(Polarity polarity) const
	{
		GraphicsScene *gScene = static_cast<GraphicsScene*>(scene());
		if (polarity == DarkPolarity)
			return gScene->darkPen();
		if (polarity == ClearPolarity)
			return gScene->clearPen();
		return QPen(Qt::NoPen);
	}

	QBrush GraphicsItem::brushForPolarity(Polarity polarity) const
	{
		GraphicsScene *gScene = static_cast<GraphicsScene*>(scene());
		if (polarity == DarkPolarity)
			return gScene->darkBrush();
		if (polarity == ClearPolarity)
			return gScene->clearBrush();
		return QBrush(Qt::NoBrush);
	}

	void GraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
	{
		//    QPen newPen = penForPolarity(m_polarity);
		//    if (pen() != newPen)
		//        setPen(newPen);

		//    QBrush newBrush = brushForPolarity(m_polarity);
		//    if (brush() != newBrush)
		//        setBrush(newBrush);

		QGraphicsPathItem::paint(painter, option, widget);
	}

	int GraphicsItem::type() const
	{
		return Type;
	}

}