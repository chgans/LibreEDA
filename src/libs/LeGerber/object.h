#pragma once

#include "liblegerber_global.h"

#include <QPainterPath>
#include <QBrush>
#include <QPen>

#include "gerber.h"

namespace LeGerber
{

	class LEGERBER_EXPORT Object
	{
	public:
		Object();

		Polarity polarity;
		QPainterPath painterPath;
	};

}