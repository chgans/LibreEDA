#pragma once

#include "liblegerber_global.h"

#include <QString>

namespace LeGerber
{

	class Statement
	{
	public:
		explicit Statement(int start, int length):
		    start(start), length(length)
		{}
		int start;
		int length;
	};

}