#pragma once

#include "liblegerber_global.h"

#include <QGraphicsScene>

#include "OldGraphicsView/Scene.h"

#include "gerber.h"
#include "object.h"

namespace LeGerber
{

    class LEGERBER_EXPORT GraphicsScene : public LeGraphicsView::Scene
	{
		Q_OBJECT

		Q_PROPERTY(QBrush darkBrush READ darkBrush WRITE setDarkBrush NOTIFY darkBrushChanged)
		Q_PROPERTY(QPen darkPen READ darkPen WRITE setDarkPen NOTIFY darkPenChanged)
		Q_PROPERTY(QBrush clearBrush READ clearBrush WRITE setClearBrush NOTIFY clearBrushChanged)
		Q_PROPERTY(QPen clearPen READ clearPen WRITE setClearPen NOTIFY clearPenChanged)

	public:
		GraphicsScene(QObject *parent = nullptr);

		// DarkRenderingHint
		//  - RenderDarkUsingPathIntersection
		//  - RenderDarkUsingBackgroundBrush

		// bool cosmeticOutline

		void addGerberObject(const Object *object);

		QBrush darkBrush() const;
		QPen darkPen() const;
		QBrush clearBrush() const;
		QPen clearPen() const;

	public slots:
		void setDarkBrush(const QBrush &brush);
		void setDarkPen(const QPen &pen);
		void setClearBrush(const QBrush &brush);
		void setClearPen(const QPen &pen);

	signals:
		void darkBrushChanged(const QBrush &brush);
		void darkPenChanged(const QPen &pen);
		void clearBrushChanged(const QBrush &brush);
		void clearPenChanged(const QPen &pen);

	private:
		QBrush m_darkBrush;
		QPen m_darkPen;
		QBrush m_clearBrush;
		QPen m_clearPen;
	};

}
