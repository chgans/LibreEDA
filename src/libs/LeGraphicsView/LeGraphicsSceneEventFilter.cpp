#include "LeGraphicsSceneEventFilter.h"

#include <QtGlobal>

LeGraphicsSceneEventFilter::LeGraphicsSceneEventFilter()
{

}

LeGraphicsSceneEventFilter::~LeGraphicsSceneEventFilter()
{

}

void LeGraphicsSceneEventFilter::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter);
    Q_UNUSED(rect);
}

void LeGraphicsSceneEventFilter::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter);
    Q_UNUSED(rect);
}

bool LeGraphicsSceneEventFilter::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool LeGraphicsSceneEventFilter::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool LeGraphicsSceneEventFilter::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool LeGraphicsSceneEventFilter::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool LeGraphicsSceneEventFilter::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}

bool LeGraphicsSceneEventFilter::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    return false;
}
