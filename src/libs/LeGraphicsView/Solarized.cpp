#include "Solarized.h"


const QColor Solarized::background("#002b36");
const QColor Solarized::backgroundHighlight("#073642");

const QColor Solarized::foreground("#fdf6e3");
const QColor Solarized::foregroundHighlight("#eee8d5");

const QColor Solarized::primaryContent("#839496");
const QColor Solarized::secondaryContent("#586e75");
const QColor Solarized::emphasisedContent("#93a1a1");

const QColor Solarized::yellow("#b58900");
const QColor Solarized::orange("#cb4b16");
const QColor Solarized::red("#dc322f");
const QColor Solarized::magenta("#d33682");
const QColor Solarized::violet("#6c71c4");
const QColor Solarized::blue("#268bd2");
const QColor Solarized::cyan("#2aa198");
const QColor Solarized::green("#859900");


const QColor Solarized::base03("#002b36");
const QColor Solarized::base02("#073642");
const QColor Solarized::base01("#586e75");
const QColor Solarized::base00("#657b83");
const QColor Solarized::base0("#839496");
const QColor Solarized::base1("#93a1a1");
const QColor Solarized::base2("#eee8d5");
const QColor Solarized::base3("#fdf6e3");

QList<QColor> Solarized::colorList()
{
    static QList<QColor> colors;
    if (colors.count() == 0)
        colors << Solarized::base03
               << Solarized::base02
               << Solarized::base01
               << Solarized::base00
               << Solarized::base0
               << Solarized::base1
               << Solarized::base2
               << Solarized::base3
               << Solarized::yellow
               << Solarized::orange
               << Solarized::red
               << Solarized::magenta
               << Solarized::violet
               << Solarized::blue
               << Solarized::cyan
               << Solarized::green;
    return colors;
}

QColor Solarized::accent(int index)
{
    switch (index % 8)
    {
        case 0:
            return yellow;
        case 1:
            return orange;
        case 2:
            return red;
        case 3:
            return magenta;
        case 4:
            return violet;
        case 5:
            return blue;
        case 6:
            return cyan;
        case 7:
            return green;
        default:
            return QColor();
    }
}
