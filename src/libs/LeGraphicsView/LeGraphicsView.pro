#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T11:33:10
#
#-------------------------------------------------

QT       += widgets

TARGET = LeGraphicsView
TEMPLATE = lib
CONFIG += staticlib c++11

QMAKE_CXXFLAGS += -Wall

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += LeGraphicsView.cpp \
    Solarized.cpp \
    LeGraphicsItem.cpp \
    LeGraphicsStyle.cpp \
    LeGraphicsScene.cpp \
    LeGraphicsPalette.cpp \
    LeGraphicsRuler.cpp \
    LeGraphicsRulerBar.cpp \
    LeGraphicsSceneEventFilter.cpp \
    LeGraphicsHandleItem.cpp \
    LeGraphicsFeatureOption.cpp \
    LeGraphicsItemLayer.cpp

HEADERS += LeGraphicsView.h \
    Solarized.h \
    LeGraphicsItem.h \
    LeGraphicsStyle.h \
    LeGraphicsScene.h \
    LeGraphicsPalette.h \
    LeGraphicsRuler.h \
    LeGraphicsRulerBar.h \
    LeGraphicsSceneEventFilter.h \
    LeGraphicsHandleItem.h \
    LeGraphics.h \
    LeGraphicsFeatureOption.h \
    LeGraphicsItemLayer.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    LeGraphicsView.pri
