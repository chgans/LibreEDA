#include "LeGraphicsHandleItem.h"
#include "LeGraphicsScene.h"
#include "LeGraphicsStyle.h"
#include "LeGraphicsItemLayer.h"

#include <QStyleOptionGraphicsItem>

LeGraphicsHandleItem::LeGraphicsHandleItem(LeGraphicsHandleRole role)
    : LeGraphicsItem(GftNone) // FIXME: Shouldn't be needed, handle item is not a feature item
    , m_role(role)
{
    setFlag(ItemIgnoresTransformations);
    setFlag(ItemIsMovable);
    setFlag(ItemIsSelectable, false);
    setEnabled(true);
    //setCacheMode(DeviceCoordinateCache); to allow painting with item's rotation
}

void LeGraphicsHandleItem::setHandleRole(LeGraphicsHandleRole role)
{
    if (m_role == role)
        return;

    prepareGeometryChange();
    m_role = role;
}

LeGraphicsHandleRole LeGraphicsHandleItem::handleRole() const
{
    return m_role;
}

QRectF LeGraphicsHandleItem::boundingRect() const
{
    return graphicsScene()->graphicsStyle()->handleBoundingRect(m_role);
}

void LeGraphicsHandleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)

    GraphicsStyleOption itemOption;
    itemOption.exposedRect = option->exposedRect;
    itemOption.palette = layer()->graphicsPalette(featureType());
    itemOption.state = state();
    auto style = graphicsScene()->graphicsStyle();
    // FIXME: rotation depends on ancestor rotation and mirror, plus the view as well
    // that can be rotated and mirrored too.
    style->drawHandle(&itemOption, painter, m_role, -parentItem()->rotation());
}

LeGraphicsItem *LeGraphicsHandleItem::clone() const
{
    return nullptr;
}

void LeGraphicsHandleItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    LeGraphicsItem::mousePressEvent(event);
}

void LeGraphicsHandleItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    LeGraphicsItem::mouseMoveEvent(event);
}

void LeGraphicsHandleItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    LeGraphicsItem::mouseReleaseEvent(event);
}
