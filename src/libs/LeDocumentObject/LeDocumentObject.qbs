import qbs 1.0

LedaLibrary {
    name: "LeDocumentObject"
    cpp.defines: base.concat([
                                 "LDO_LIBRARY",
                             ])

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    Depends { name: "QtPropertyBrowser" }
    Depends { name: "Core" }

    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
        Depends { name: "QtPropertyBrowser" }
    }

    files: [
        "Document.cpp",
        "Document.h",
        "DocumentObjectInspector.cpp",
        "DocumentObjectInspector.h",
        "DocumentObjectTreeModel.cpp",
        "DocumentObjectTreeModel.h",
        "DocumentTreeModel.cpp",
        "DocumentTreeModel.h",
        "DocumentStreamReader.cpp",
        "DocumentStreamReader.h",
        "DocumentStreamWriter.cpp",
        "DocumentStreamWriter.h",
        "IDocumentObject.cpp",
        "IDocumentObject.h",
        "IDocumentObjectFactory.cpp",
        "IDocumentObjectFactory.h",
        "IDocumentObjectListener.cpp",
        "IDocumentObjectListener.h",
        "LeDocumentObject_global.h",
        "ObjectPropertyBrowser.cpp",
        "ObjectPropertyBrowser.h",
    ]
}
