#pragma once

#include "LeDocumentObject_global.h"

#include <QObject>

namespace LDO
{

    class IDocumentObject;

    class LDO_EXPORT IDocumentObjectFactory : public QObject
    {
        Q_OBJECT

    public:
        explicit IDocumentObjectFactory(QObject *parent = nullptr);
        ~IDocumentObjectFactory();

        virtual QList<int> objectTypeIds() const = 0;
        virtual IDocumentObject *createObject(int typeId) const = 0;
        virtual QString typeName(int typeId) const = 0;

        virtual QString enumIconName(int typeId, int value) const;
        virtual QString iconName(int typeId, const IDocumentObject *object) const;
    };

}
