
#pragma once

#include "LeGraphicsView.h"

#include <QRectF>
#include <QColor>

class QPainter;

namespace LeGraphicsView
{

    class OLDGRAPHICSVIEW_EXPORT GraphicsCrossHair
    {
    public:
        // Move to a more global namespace
        enum Style
        {
            NoCrossHair = 0,
            SmallCrossHair,
            LargeCrossHair
        };

        GraphicsCrossHair();
        virtual ~GraphicsCrossHair();

        void setPos(const QPointF &pos);
        QPointF pos() const;

        void setStyle(Style style);
        Style style() const;

        void setColor(QColor color);
        QColor color() const;

        virtual void paint(QPainter *painter, const QRectF &exposedRect);

    private:
        QPointF m_pos;
        Style m_style;
        QColor m_color;

        static const qreal CrossRadiusPx;
        static const qreal SmallCrossLengthPx;
    };

}
