#pragma once

#include "LeGraphicsView.h"

#include <QColor>
#include <QRectF>
#include <QLineF>
#include <QVector>

class QPainter;

namespace LeGraphicsView
{

    class GridModel;

    class OLDGRAPHICSVIEW_EXPORT GraphicsGrid
    {
    public:
        GraphicsGrid();
        virtual ~GraphicsGrid();

        void setGridModel(GridModel *model);

        void setGridColor(const QColor &color);
        QColor gridColor() const;

        virtual void paint(QPainter *painter, const QRectF &exposedRect);

    protected:

    private:
        GridModel *m_model;
        QColor m_color;
        QVector<QLineF> m_gridLines;
        QVector<QPointF> m_gridPoints;
        void update();
    };

}
