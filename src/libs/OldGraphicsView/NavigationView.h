#pragma once

#include "LeGraphicsView.h"

#include "View.h"

namespace LeGraphicsView
{

    class OLDGRAPHICSVIEW_EXPORT NavigationView : public View
    {
        Q_OBJECT

    public:
        NavigationView(QWidget *parent = nullptr);
        ~NavigationView();

        void setMainView(View *mainView);
        View *mainView() const;

    protected slots:
        virtual void synchroniseSettings();

    private:
        enum Move
        {
            NoMove = 0,
            MoveRect,
            MoveTopLeft,
            MoveTopRight,
            MoveBottomRight,
            MoveBottomLeft
        };
        View *m_mainView;
        QRectF m_visibleSceneRect;
        Move m_move;
        QPoint m_moveOrigin;

        static const qreal GrabDistance;
        bool canGrabPoint(const QPointF &pos);
        bool canGrabRect(const QRectF &pos);

        // QGraphicsView interface
    protected:
        virtual void drawForeground(QPainter *painter, const QRectF &rect) override;

        // QWidget interface
    protected:
        virtual void mousePressEvent(QMouseEvent *event) override;
        virtual void mouseReleaseEvent(QMouseEvent *event) override;
        virtual void mouseMoveEvent(QMouseEvent *event) override;

        // QObject interface
    public:
        virtual bool eventFilter(QObject *watched, QEvent *event) override;

        // QWidget interface
    protected:
        virtual void resizeEvent(QResizeEvent *event) override;
    };

} // namespace LeGraphicsView

