#pragma once

#include "LeGraphicsView.h"

#include <QGraphicsScene>

#include "Palette.h"

namespace LeGraphicsView
{

    class Item;
    class GraphicsItem;
    class Settings;

    class OLDGRAPHICSVIEW_EXPORT Scene : public QGraphicsScene
    {
        Q_OBJECT

    public:
        explicit Scene(QObject *parent = nullptr);
        Scene(const QRectF &sceneRect, QObject *parent = nullptr);
        Scene(qreal x, qreal y, qreal width, qreal height, QObject *parent = nullptr);

        ~Scene();

        Palette palette() const;

    public slots:
        void setPalette(Palette palette);
        void applySettings(const Settings &settings);

    signals:
        void paletteChanged(Palette palette);

    private:
        Palette m_palette;
    };

}
