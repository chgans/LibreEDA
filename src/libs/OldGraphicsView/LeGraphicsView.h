#pragma once

#include <QtCore/qglobal.h>

#if defined(OLDGRAPHICSVIEW_LIBRARY)
#  define OLDGRAPHICSVIEW_EXPORT Q_DECL_EXPORT
#else
#  define OLDGRAPHICSVIEW_EXPORT Q_DECL_IMPORT
#endif
