#include "Scene.h"
#include "Settings.h"

#include <QPainter>

namespace LeGraphicsView
{

    Scene::Scene(QObject *parent):
        QGraphicsScene(parent)
    {
    }

    Scene::Scene(const QRectF &sceneRect, QObject *parent):
        QGraphicsScene(sceneRect, parent)
    {

    }

    Scene::Scene(qreal x, qreal y, qreal width, qreal height, QObject *parent):
        QGraphicsScene(x, y, width, height, parent)
    {

    }

    Scene::~Scene()
    {
    }

    void Scene::setPalette(Palette palette)
    {
        m_palette = palette;
    }

    Palette Scene::palette() const
    {
        return m_palette;
    }

    void Scene::applySettings(const Settings &settings)
    {
        Q_UNUSED(settings);
    }

}
