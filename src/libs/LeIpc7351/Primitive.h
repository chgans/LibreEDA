#pragma once

#include "GeoFeature.h"

// Most of IPC 7581 Primitives + D-shape
// TBD: Might rename Geometry if we want to include text, and lines/outlines
// OR Primitive is Shapes + Lines + Text + Composite

class Primitive: public GeoFeature
{
    Q_OBJECT

public:
    Primitive();
    ~Primitive();
};
