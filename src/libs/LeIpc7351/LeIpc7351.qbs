import qbs 1.0

LedaLibrary {
    name: "LeIpc7351"

    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    Depends { name: "LeDocumentObject";}

    cpp.defines: base.concat(["LEIPC7351_LIBRARY"])

    files: [
        "Arrangement.cpp",
        "Arrangement.h",
        "DocumentObjectFactory.cpp",
        "DocumentObjectFactory.h",
        "DocumentObjectList.cpp",
        "DocumentObjectList.h",
        "GeoCurve.cpp",
        "GeoCurve.h",
        "GeoFeature.cpp",
        "GeoFeature.h",
        "LandPattern.cpp",
        "LandPattern.h",
        "Layer.cpp",
        "Layer.h",
        "LeIpc7351.h",
        "PackageFeature.cpp",
        "PackageFeature.h",
        "PadStack.cpp",
        "PadStackElement.cpp",
        "PadStackElement.h",
        "PadStack.h",
        "PadStackInstance.cpp",
        "PadStackInstance.h",
        "Primitive.cpp",
        "Primitive.h",
        "Terminal.cpp",
        "Terminal.h",
        "Arrangements/ArrayArrangement.cpp",
        "Arrangements/ArrayArrangement.h",
        "Arrangements/CornerArrangement.cpp",
        "Arrangements/CornerArrangement.h",
        "Arrangements/DualArrangement.cpp",
        "Arrangements/DualArrangement.h",
        "Arrangements/QuadInLineArrangement.cpp",
        "Arrangements/QuadInLineArrangement.h",
        "Primitives/ArcOfCircle.cpp",
        "Primitives/ArcOfCircle.h",
        "Primitives/ButterflyPrimitive.cpp",
        "Primitives/ButterflyPrimitive.h",
        "Primitives/CirclePrimitive.cpp",
        "Primitives/CirclePrimitive.h",
        "Primitives/RectanglePrimitive.cpp",
        "Primitives/RectanglePrimitive.h",
        "Primitives/RegularPolygonPrimitive.cpp",
        "Primitives/RegularPolygonPrimitive.h",
        "Primitives/TrianglePrimitive.cpp",
        "Primitives/TrianglePrimitive.h",
    ]

    Export {
        Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }
    }
}
