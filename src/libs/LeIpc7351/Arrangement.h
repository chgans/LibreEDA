#pragma once

#include "GeoFeature.h"

class Arrangement : public GeoFeature
{
    Q_OBJECT

    Q_PROPERTY(QString namingPrefix READ namingPrefix WRITE setNamingPrefix NOTIFY namingPrefixChanged)
    Q_PROPERTY(L7::NamingScheme namingScheme READ namingScheme WRITE setNamingScheme NOTIFY namingSchemeChanged)
    Q_PROPERTY(QList<Transform> transformations READ transformations NOTIFY transformationsChanged)
    Q_PROPERTY(QList<QString> names READ names NOTIFY namesChanged)

public:
    Arrangement();
    ~Arrangement();

    QString namingPrefix() const;
    L7::NamingScheme namingScheme() const;
    QList<Transform> transformations() const;
    QList<QString> names() const;

public slots:
    void setNamingPrefix(QString namingPrefix);
    void setNamingScheme(L7::NamingScheme namingScheme);

signals:
    void namingPrefixChanged(QString namingPrefix);
    void namingSchemeChanged(L7::NamingScheme namingScheme);
    void transformationsChanged(const QList<Transform> &transformations);
    void namesChanged(QList<QString> names);

protected:
    virtual void calculate() = 0;
    void setTransformations(const QList<Transform> &transformations);
    void setNames(const QList<QString> &names);

private:
    QString m_namingPrefix;
    L7::NamingScheme m_namingScheme;
    QList<Transform> m_transformations;
    QList<QString> m_names;
};

