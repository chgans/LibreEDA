#include "TrianglePrimitive.h"


TrianglePrimitive::TrianglePrimitive()
    : Primitive()
{
}

TrianglePrimitive::~TrianglePrimitive()
{
}

QSizeF TrianglePrimitive::size() const
{
    return m_size;
}

void TrianglePrimitive::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    _IDO_SET_PROPERTY(size);
}
