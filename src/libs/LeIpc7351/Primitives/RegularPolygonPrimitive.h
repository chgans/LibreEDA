#pragma once

#include "LeIpc7351/Primitive.h"

class RegularPolygonPrimitive : public Primitive
{
    Q_OBJECT

    Q_PROPERTY(qreal diameter READ diameter WRITE setDiameter NOTIFY diameterChanged)
    Q_PROPERTY(int sideCount READ sideCount WRITE setSideCount NOTIFY sideCountChanged)

    qreal m_diameter = 0.0;
    int m_sideCount = 6;

public:
    RegularPolygonPrimitive();
    ~RegularPolygonPrimitive();

    qreal diameter() const;
    int sideCount() const;

public slots:
    void setDiameter(qreal diameter);
    void setSideCount(int sideCount);

signals:
    void diameterChanged(qreal diameter);
    void sideCountChanged(int sideCount);
};

