#include "CirclePrimitive.h"

CirclePrimitive::CirclePrimitive()
    : Primitive()
{

}

CirclePrimitive::~CirclePrimitive()
{

}

qreal CirclePrimitive::diameter() const
{
    return m_diameter;
}

void CirclePrimitive::setDiameter(qreal diameter)
{
    if (m_diameter == diameter)
        return;

    _IDO_SET_PROPERTY(diameter);
}
