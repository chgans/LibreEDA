#include "ArcOfCircle.h"

#include "LeIpc7351.h"

ArcOfCircle::ArcOfCircle()
    : Primitive()
{

}

ArcOfCircle::~ArcOfCircle()
{

}

qreal ArcOfCircle::spanAngle() const
{
    return m_spanAngle;
}

qreal ArcOfCircle::startAngle() const
{
    return m_startAngle;
}

qreal ArcOfCircle::diameter() const
{
    return m_diameter;
}


void ArcOfCircle::setSpanAngle(qreal spanAngle)
{
    spanAngle = normaliseAngleSpan(spanAngle);

    if (qFuzzyCompare(m_spanAngle, spanAngle))
        return;

    _IDO_SET_PROPERTY(spanAngle);
}

void ArcOfCircle::setStartAngle(qreal startAngle)
{
    startAngle = normaliseAngle(startAngle);

    if (qFuzzyCompare(m_startAngle, startAngle))
        return;

    _IDO_SET_PROPERTY(startAngle);
}

void ArcOfCircle::setDiameter(qreal diameter)
{
    if (diameter < 0)
        return;

    if (qFuzzyCompare(m_diameter, diameter))
        return;

    _IDO_SET_PROPERTY(diameter);
}

