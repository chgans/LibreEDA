#pragma once

#include "LeIpc7351/Primitive.h"

#include <QPointF>

class CirclePrimitive: public Primitive
{
    Q_OBJECT

    Q_PROPERTY(qreal diameter READ diameter WRITE setDiameter NOTIFY diameterChanged)

public:
    enum
    {
        Type = GeometryBaseType + 0x01
    };

    CirclePrimitive();
    ~CirclePrimitive();

    qreal diameter() const;

signals:
    void diameterChanged(qreal diameter);

public slots:
    void setDiameter(qreal diameter);

private:
    qreal m_diameter;

    // IDocumentObject interface
public:
    int objectType() const { return Type; }
};

