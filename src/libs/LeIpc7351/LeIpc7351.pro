#-------------------------------------------------
#
# Project created by QtCreator 2017-02-11T16:02:46
#
#-------------------------------------------------

QT       -= gui

TARGET = LeIpc7351
TEMPLATE = lib
CONFIG += staticlib c++11

INCLUDEPATH += $$top_srcdir/src/lib

QMAKE_CXXFLAGS += \
-Wshadow \
-Wredundant-decls \
-Wcast-align \
-Wmissing-declarations \
-Wmissing-include-dirs \
-Wswitch-default \
-Wextra \
-Wall \
-Winvalid-pch \
-Wredundant-decls \
-Wmissing-prototypes \
-Wformat=2 \
-Wmissing-format-attribute \
-Wformat-nonliteral \
-Wuninitialized \
-Wpedantic

# -Wzero-as-null-pointer-constant

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Arrangements/CornerArrangement.cpp \
    Arrangements/QuadInLineArrangement.cpp \
    Arrangements/DualArrangement.cpp \
    Arrangements/ArrayArrangement.cpp \
    PadStack.cpp \
    IDocumentObject.cpp \
    Document.cpp \
    IDocumentObjectFactory.cpp \
    Terminal.cpp \
    IDocumentObjectListener.cpp \
    PadStackElement.cpp \
    DocumentObjectFactory.cpp \
    Primitives/CirclePrimitive.cpp \
    Primitive.cpp \
    Primitives/RectanglePrimitive.cpp \
    LandPattern.cpp \
    PadStackInstance.cpp \
    Layer.cpp \
    DocumentObjectList.cpp \
    DocumentStreamWriter.cpp \
    DocumentStreamReader.cpp \
    PackageFeature.cpp \
    Arrangement.cpp \
    GeoFeature.cpp \
    Primitives/RegularPolygonPrimitive.cpp \
    Primitives/TrianglePrimitive.cpp \
    Primitives/ButterflyPrimitive.cpp \
    GeoCurve.cpp \
    Primitives/ArcOfCircle.cpp


HEADERS += LeIpc7351.h \
    Arrangements/CornerArrangement.h \
    Arrangements/QuadInLineArrangement.h \
    Arrangements/DualArrangement.h \
    Arrangements/ArrayArrangement.h \
    IDocumentObject.h \
    Document.h \
    PadStack.h \
    IDocumentObjectFactory.h \
    Terminal.h \
    IDocumentObjectListener.h \
    PadStackElement.h \
    DocumentObjectFactory.h \
    Primitives/CirclePrimitive.h \
    Primitive.h \
    Primitives/RectanglePrimitive.h \
    LandPattern.h \
    PadStackInstance.h \
    Layer.h \
    DocumentObjectList.h \
    DocumentStreamWriter.h \
    DocumentStreamReader.h \
    PackageFeature.h \
    Arrangement.h \
    GeoFeature.h \
    Primitives/RegularPolygonPrimitive.h \
    Primitives/TrianglePrimitive.h \
    Primitives/ButterflyPrimitive.h \
    GeoCurve.h \
    Primitives/ArcOfCircle.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    LeIpc7351.pri
