INCLUDEPATH += $$PWD/..

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/src/lib/LeIpc7351/release/ -lLeIpc7351
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/src/lib/LeIpc7351/debug/ -lLeIpc7351
else:unix: LIBS += -L$$top_builddir/src/lib/LeIpc7351/ -lLeIpc7351

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc7351/release/libLeIpc7351.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc7351/debug/libLeIpc7351.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc7351/release/LeIpc7351.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc7351/debug/LeIpc7351.lib
else:unix: PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc7351/libLeIpc7351.a
