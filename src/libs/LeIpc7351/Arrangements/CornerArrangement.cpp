#include "CornerArrangement.h"

CornerArrangement::CornerArrangement()
    : Arrangement()
{

}

CornerArrangement::~CornerArrangement()
{

}

QSizeF CornerArrangement::size() const
{
    return m_size;
}

void CornerArrangement::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    _IDO_SET_PROPERTY(size);
    calculate();
}

void CornerArrangement::calculate()
{
    QList<Transform> xforms;
    Transform xform;

    // Bottom Left
    xform.mirror = true;
    xform.rotation = 0;
    xform.translation = QPointF(-m_size.width()/2.0,
                                -m_size.height()/2.0);
    xforms.append(xform);
    // Bottom right
    xform.mirror = false;
    xform.rotation = 0;
    xform.translation = QPointF(m_size.width()/2.0,
                                -m_size.height()/2.0);
    xforms.append(xform);
    // Top right
    xform.mirror = true;
    xform.rotation = 180;
    xform.translation = QPointF(m_size.width()/2.0,
                                m_size.height()/2.0);
    xforms.append(xform);
    // Top left
    xform.mirror = false;
    xform.rotation = 180;
    xform.translation = QPointF(-m_size.width()/2.0,
                                m_size.height()/2.0);
    xforms.append(xform);

    setTransformations(xforms);
}
