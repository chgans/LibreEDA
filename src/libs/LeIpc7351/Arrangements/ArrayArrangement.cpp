#include "ArrayArrangement.h"

ArrayArrangement::ArrayArrangement()
    : Arrangement()
    , m_rowCount(0)
    , m_columnCount(0)
    , m_rowPitch(0)
    , m_columnPitch(0)
    , m_staggered(false)
{

}

ArrayArrangement::~ArrayArrangement()
{

}

QSizeF ArrayArrangement::size() const
{
    return m_size;
}

int ArrayArrangement::rowCount() const
{
    return m_rowCount;
}

int ArrayArrangement::columnCount() const
{
    return m_columnCount;
}

qreal ArrayArrangement::rowPitch() const
{
    return m_rowPitch;
}

qreal ArrayArrangement::columnPitch() const
{
    return m_columnPitch;
}

bool ArrayArrangement::isStaggered() const
{
    return m_staggered;
}

void ArrayArrangement::setSize(QSizeF size)
{
    if (m_size == size)
        return;

    _IDO_SET_PROPERTY(size);
    calculate();
}

void ArrayArrangement::setRowCount(int rowCount)
{
    if (m_rowCount == rowCount)
        return;

    _IDO_SET_PROPERTY(rowCount);
    calculate();
}

void ArrayArrangement::setColumnCount(int columnCount)
{
    if (m_columnCount == columnCount)
        return;

    _IDO_SET_PROPERTY(columnCount);
    calculate();
}

void ArrayArrangement::setRowPitch(qreal rowPitch)
{
    if (qFuzzyCompare(m_rowPitch, rowPitch))
        return;

    _IDO_SET_PROPERTY(rowPitch);
    calculate();
}

void ArrayArrangement::setColumnPitch(qreal columnPitch)
{
    if (qFuzzyCompare(m_columnPitch, columnPitch))
        return;

    _IDO_SET_PROPERTY(columnPitch);
    calculate();
}

void ArrayArrangement::setStaggered(bool staggered)
{
    if (m_staggered == staggered)
        return;

    _IDO_SET_PROPERTY(staggered);
    calculate();
}

void ArrayArrangement::calculate()
{
    // FIXME
    setSize(QSizeF(m_columnPitch*(m_columnCount-1),
                   m_rowPitch*(m_rowCount-1)));

    QList<Transform> xforms;
    for (int i = 0; i < m_rowCount; i++)
    {
        for (int j = 0; j < m_columnCount; j++)
        {
            Transform xform;
            xform.rotation = 0;
            xform.mirror = false;
            xform.translation = QPointF(-m_size.width()/2.0 + j*m_columnPitch,
                                        -m_size.height()/2.0 + i*m_rowPitch);
            xforms.append(xform);
        }
    }

    setTransformations(xforms);
}
