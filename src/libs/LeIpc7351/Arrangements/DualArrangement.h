#pragma once

#include <QObject>

#include "LeIpc7351/Arrangement.h"

class DualArrangement: public Arrangement
{
    Q_OBJECT

    Q_PROPERTY(qreal distance READ distance WRITE setDistance NOTIFY distanceChanged)
    Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation NOTIFY orientationChanged)

public:
    explicit DualArrangement();
    ~DualArrangement();

    qreal distance() const;
    Qt::Orientation orientation() const;

public slots:
    void setDistance(qreal distance);
    void setOrientation(Qt::Orientation orientation);

signals:
    void arrangementChanged();
    void distanceChanged(qreal distance);
    void orientationChanged(Qt::Orientation orientation);

private:
    qreal m_distance = 0.0;
    Qt::Orientation m_orientation = Qt::Horizontal;

    // Arrangement interface
protected:
    virtual void calculate() override;
};
