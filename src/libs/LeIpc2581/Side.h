
#pragma once

namespace Ipc2581b
{

enum class Side
{
    Top,
    Internal,
    All,
    Both,
    None,
    Bottom,
};

}