
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Spec.h"

namespace Ipc2581b
{

class LocationParser;
class OutlineParser;
class SpecificationTypeParser;
class XformParser;
class QStringParser;

class SpecParser
{
public:
    SpecParser(
            QStringParser*&
            , SpecificationTypeParser*&
            , XformParser*&
            , LocationParser*&
            , OutlineParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Spec *result();

private:
    QStringParser *&m_nameParser;
    SpecificationTypeParser *&m_specificationTypeParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    OutlineParser *&m_outlineParser;
    QScopedPointer<Spec> m_result;
};

}