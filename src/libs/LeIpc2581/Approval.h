
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class Approval
{
public:
	virtual ~Approval() {}

    QString dateTime;
    QString personRef;

};

}