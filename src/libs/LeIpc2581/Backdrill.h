
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "QString.h"
#include "Property.h"
#include "BackdrillList.h"

namespace Ipc2581b
{

class Backdrill: public SpecificationType
{
public:
	virtual ~Backdrill() {}

    BackdrillList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Backdrill;
    }
};

}