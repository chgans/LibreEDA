
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LayerHole.h"

namespace Ipc2581b
{


class LayerHoleParser
{
public:
    LayerHoleParser(
            );

    bool parse(QXmlStreamReader *reader);
    LayerHole *result();

private:
    QScopedPointer<LayerHole> m_result;
};

}