
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ConductorList.h"

namespace Ipc2581b
{

class ConductorListParser
{
public:
    ConductorListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ConductorList result();

private:
    ConductorList m_result;
};

}