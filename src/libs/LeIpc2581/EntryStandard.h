
#pragma once

#include <QList>
#include "Optional.h"


#include "StandardPrimitive.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryStandard
{
public:
	virtual ~EntryStandard() {}

    QString id;
    StandardPrimitive *standardPrimitive;

};

}