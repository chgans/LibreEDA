#include "StandardPrimitiveParser.h"

#include "HexagonParser.h"
#include "OctagonParser.h"
#include "TriangleParser.h"
#include "DonutParser.h"
#include "EllipseParser.h"
#include "RectCornerParser.h"
#include "ThermalParser.h"
#include "RectChamParser.h"
#include "ButterflyParser.h"
#include "MoireParser.h"
#include "CircleParser.h"
#include "OvalParser.h"
#include "RectCenterParser.h"
#include "ContourParser.h"
#include "DiamondParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

StandardPrimitiveParser::StandardPrimitiveParser(
    ButterflyParser *&_butterflyParser
    , CircleParser *&_circleParser
    , ContourParser *&_contourParser
    , DiamondParser *&_diamondParser
    , DonutParser *&_donutParser
    , EllipseParser *&_ellipseParser
    , HexagonParser *&_hexagonParser
    , MoireParser *&_moireParser
    , OctagonParser *&_octagonParser
    , OvalParser *&_ovalParser
    , RectCenterParser *&_rectCenterParser
    , RectChamParser *&_rectChamParser
    , RectCornerParser *&_rectCornerParser
    , ThermalParser *&_thermalParser
    , TriangleParser *&_triangleParser
):    m_butterflyParser(_butterflyParser)
    , m_circleParser(_circleParser)
    , m_contourParser(_contourParser)
    , m_diamondParser(_diamondParser)
    , m_donutParser(_donutParser)
    , m_ellipseParser(_ellipseParser)
    , m_hexagonParser(_hexagonParser)
    , m_moireParser(_moireParser)
    , m_octagonParser(_octagonParser)
    , m_ovalParser(_ovalParser)
    , m_rectCenterParser(_rectCenterParser)
    , m_rectChamParser(_rectChamParser)
    , m_rectCornerParser(_rectCornerParser)
    , m_thermalParser(_thermalParser)
    , m_triangleParser(_triangleParser)
{

}

bool StandardPrimitiveParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("Butterfly"))
    {
        if(m_butterflyParser->parse(reader))
        {
            m_result = m_butterflyParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Circle"))
    {
        if(m_circleParser->parse(reader))
        {
            m_result = m_circleParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Contour"))
    {
        if(m_contourParser->parse(reader))
        {
            m_result = m_contourParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Diamond"))
    {
        if(m_diamondParser->parse(reader))
        {
            m_result = m_diamondParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Donut"))
    {
        if(m_donutParser->parse(reader))
        {
            m_result = m_donutParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Ellipse"))
    {
        if(m_ellipseParser->parse(reader))
        {
            m_result = m_ellipseParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Hexagon"))
    {
        if(m_hexagonParser->parse(reader))
        {
            m_result = m_hexagonParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Moire"))
    {
        if(m_moireParser->parse(reader))
        {
            m_result = m_moireParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Octagon"))
    {
        if(m_octagonParser->parse(reader))
        {
            m_result = m_octagonParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Oval"))
    {
        if(m_ovalParser->parse(reader))
        {
            m_result = m_ovalParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("RectCenter"))
    {
        if(m_rectCenterParser->parse(reader))
        {
            m_result = m_rectCenterParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("RectCham"))
    {
        if(m_rectChamParser->parse(reader))
        {
            m_result = m_rectChamParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("RectCorner"))
    {
        if(m_rectCornerParser->parse(reader))
        {
            m_result = m_rectCornerParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Thermal"))
    {
        if(m_thermalParser->parse(reader))
        {
            m_result = m_thermalParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Triangle"))
    {
        if(m_triangleParser->parse(reader))
        {
            m_result = m_triangleParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"StandardPrimitive\"").arg(reader->name().toString()));
    return false;
}

StandardPrimitive *StandardPrimitiveParser::result()
{
    return m_result;
}

bool StandardPrimitiveParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("Butterfly"))
        return true;
    if (name == QStringLiteral("Circle"))
        return true;
    if (name == QStringLiteral("Contour"))
        return true;
    if (name == QStringLiteral("Diamond"))
        return true;
    if (name == QStringLiteral("Donut"))
        return true;
    if (name == QStringLiteral("Ellipse"))
        return true;
    if (name == QStringLiteral("Hexagon"))
        return true;
    if (name == QStringLiteral("Moire"))
        return true;
    if (name == QStringLiteral("Octagon"))
        return true;
    if (name == QStringLiteral("Oval"))
        return true;
    if (name == QStringLiteral("RectCenter"))
        return true;
    if (name == QStringLiteral("RectCham"))
        return true;
    if (name == QStringLiteral("RectCorner"))
        return true;
    if (name == QStringLiteral("Thermal"))
        return true;
    if (name == QStringLiteral("Triangle"))
        return true;
    return false;
}

}