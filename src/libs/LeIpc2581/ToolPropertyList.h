
#pragma once

namespace Ipc2581b
{

enum class ToolPropertyList
{
    FinishedSize,
    Other,
    DrillSize,
    BitAngle,
};

}