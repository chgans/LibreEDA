
#pragma once

#include <QList>
#include "Optional.h"


#include "SoftwarePackage.h"
#include "QString.h"

namespace Ipc2581b
{

class FileRevision
{
public:
	virtual ~FileRevision() {}

    QString fileRevisionId;
    QString comment;
    Optional<QString> labelOptional;
    SoftwarePackage *softwarePackage;

};

}