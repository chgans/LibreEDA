
#pragma once

#include <QString>

class QXmlStreamReader;

#include "UnitMode.h"

namespace Ipc2581b
{

class UnitModeParser
{
public:
    UnitModeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    UnitMode result();

private:
    UnitMode m_result;
};

}