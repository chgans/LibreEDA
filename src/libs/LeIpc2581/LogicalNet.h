
#pragma once

#include <QList>
#include "Optional.h"


#include "NetClass.h"
#include "NonstandardAttribute.h"
#include "PinRef.h"
#include "QString.h"

namespace Ipc2581b
{

class LogicalNet
{
public:
	virtual ~LogicalNet() {}

    QString name;
    Optional<NetClass> netClassOptional;
    QList<NonstandardAttribute*> nonstandardAttributeList;
    QList<PinRef*> pinRefList;

};

}