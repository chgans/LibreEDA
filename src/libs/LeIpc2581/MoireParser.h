
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Moire.h"

namespace Ipc2581b
{

class XformParser;
class IntParser;
class DoubleParser;

class MoireParser
{
public:
    MoireParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , IntParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Moire *result();

private:
    DoubleParser *&m_diameterParser;
    DoubleParser *&m_ringWidthParser;
    DoubleParser *&m_ringGapParser;
    IntParser *&m_ringNumberParser;
    DoubleParser *&m_lineWidthParser;
    DoubleParser *&m_lineLengthParser;
    DoubleParser *&m_lineAngleParser;
    XformParser *&m_xformParser;
    QScopedPointer<Moire> m_result;
};

}