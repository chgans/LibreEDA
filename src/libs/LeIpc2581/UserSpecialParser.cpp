#include "UserSpecialParser.h"

#include "FeatureParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

UserSpecialParser::UserSpecialParser(
    FeatureParser *&_featureParser
):    m_featureParser(_featureParser)
{

}

bool UserSpecialParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new UserSpecial());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (m_featureParser->isSubstitution(name))
        {
            if (!m_featureParser->parse(reader))
                return false;
            auto result = m_featureParser->result();
            m_result->featureList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

UserSpecial *UserSpecialParser::result()
{
    return m_result.take();
}

}