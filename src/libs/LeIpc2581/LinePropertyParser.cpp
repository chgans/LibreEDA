#include "LinePropertyParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

LinePropertyParser::LinePropertyParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool LinePropertyParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, LineProperty> map =
    {
        {QStringLiteral("SOLID"), LineProperty::Solid},
        {QStringLiteral("DASHED"), LineProperty::Dashed},
        {QStringLiteral("PHANTOM"), LineProperty::Phantom},
        {QStringLiteral("ERASE"), LineProperty::Erase},
        {QStringLiteral("DOTTED"), LineProperty::Dotted},
        {QStringLiteral("CENTER"), LineProperty::Center},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum LineProperty").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

LineProperty LinePropertyParser::result()
{
    return m_result;
}

}