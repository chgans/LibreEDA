#ifndef ENUMTRANSLATOR_H
#define ENUMTRANSLATOR_H

#include <QMap>
#include <QMetaType>
#include <QString>

#include "BackdrillList.h"
Q_DECLARE_METATYPE(Ipc2581b::BackdrillList)
#include "CadPin.h"
Q_DECLARE_METATYPE(Ipc2581b::CadPin)
#include "ConductorList.h"
Q_DECLARE_METATYPE(Ipc2581b::ConductorList)
#include "ComplianceList.h"
Q_DECLARE_METATYPE(Ipc2581b::ComplianceList)
#include "DielectricList.h"
Q_DECLARE_METATYPE(Ipc2581b::DielectricList)
#include "GeneralList.h"
Q_DECLARE_METATYPE(Ipc2581b::GeneralList)
#include "ImpedanceList.h"
Q_DECLARE_METATYPE(Ipc2581b::ImpedanceList)
#include "LayerFunction.h"
Q_DECLARE_METATYPE(Ipc2581b::LayerFunction)
#include "Mount.h"
Q_DECLARE_METATYPE(Ipc2581b::Mount)
#include "NetClass.h"
Q_DECLARE_METATYPE(Ipc2581b::NetClass)
#include "PackageType.h"
Q_DECLARE_METATYPE(Ipc2581b::PackageType)
#include "PadUse.h"
Q_DECLARE_METATYPE(Ipc2581b::PadUse)
#include "PadUsage.h"
Q_DECLARE_METATYPE(Ipc2581b::PadUsage)
#include "PinElectrical.h"
Q_DECLARE_METATYPE(Ipc2581b::PinElectrical)
#include "PinMount.h"
Q_DECLARE_METATYPE(Ipc2581b::PinMount)
#include "PinOneOrientation.h"
Q_DECLARE_METATYPE(Ipc2581b::PinOneOrientation)
#include "PlatingStatus.h"
Q_DECLARE_METATYPE(Ipc2581b::PlatingStatus)
#include "Polarity.h"
Q_DECLARE_METATYPE(Ipc2581b::Polarity)
#include "PropertyUnit.h"
Q_DECLARE_METATYPE(Ipc2581b::PropertyUnit)
#include "Side.h"
Q_DECLARE_METATYPE(Ipc2581b::Side)
#include "StructureList.h"
Q_DECLARE_METATYPE(Ipc2581b::StructureList)
#include "TransmissionList.h"
Q_DECLARE_METATYPE(Ipc2581b::TransmissionList)
#include "TechnologyList.h"
Q_DECLARE_METATYPE(Ipc2581b::TechnologyList)
#include "TemperatureList.h"
Q_DECLARE_METATYPE(Ipc2581b::TemperatureList)
#include "ToolList.h"
Q_DECLARE_METATYPE(Ipc2581b::ToolList)
#include "ToolPropertyList.h"
Q_DECLARE_METATYPE(Ipc2581b::ToolPropertyList)
#include "Units.h"
Q_DECLARE_METATYPE(Ipc2581b::Units)
#include "VCutList.h"
Q_DECLARE_METATYPE(Ipc2581b::VCutList)

namespace Ipc2581b
{

class EnumTranslator
{
public:
    static QString layerFunctionText(LayerFunction func);
    static QString sideText(Side side);
    static QString polarityText(Polarity polarity);
    static QString packageTypeText(PackageType type);
    static QString cadPinTypeText(CadPin type);
    static QString electricalPinTypeText(PinElectrical type);
    static QString mountingPinTypeText(PinMount type);
    static QString pinOneOrientationText(PinOneOrientation type);
    static QString padUs(Units units);
    static QString padUsageText(PadUsage usage);
    static QString padUseText(PadUse use);
    static QString platingStatusText(PlatingStatus status);
    static QString unitsText(Units units);
    static QString mountText(Mount mount);
    static QString netClassText(NetClass netClass);

    static QString propertyUnitText(PropertyUnit unit);
    static QString backdrillListText(BackdrillList backdrill);
    static QString conductorListText(ConductorList conductor);
    static QString dielectricListText(DielectricList dielectric);
    static QString generalListText(GeneralList general);
    static QString impedanceListText(ImpedanceList impedance);
    static QString structureListText(StructureList structure);
    static QString transmissionListText(TransmissionList transmission);
    static QString complianceListText(ComplianceList compliance);
    static QString technologyListText(TechnologyList technology);
    static QString temperatureListText(TemperatureList temperature);
    static QString toolListText(ToolList tool);
    static QString toolPropertyListText(ToolPropertyList property);
    static QString vCutListText(VCutList vCut);


    static QMap<int, const char *> enumMapping(int typeId);
    static QList<QString> enumNames(int typeId);
    static QList<int> enumValues(int typeId);
    static QString enumName(int typeId, int enumIndex);
    static int enumValue(int typeId, int enumIndex);
    static int enumIndex(int typeId, int enumValue);

};

}

#endif // ENUMTRANSLATOR_H
