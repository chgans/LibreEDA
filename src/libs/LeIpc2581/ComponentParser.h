
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Component.h"

namespace Ipc2581b
{

class XformParser;
class QStringParser;
class MountParser;
class DoubleParser;
class NonstandardAttributeParser;
class LocationParser;

class ComponentParser
{
public:
    ComponentParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , MountParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , NonstandardAttributeParser*&
            , XformParser*&
            , LocationParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Component *result();

private:
    QStringParser *&m_refDesParser;
    QStringParser *&m_packageRefParser;
    QStringParser *&m_partParser;
    QStringParser *&m_layerRefParser;
    MountParser *&m_mountTypeParser;
    DoubleParser *&m_weightParser;
    DoubleParser *&m_heightParser;
    DoubleParser *&m_standoffParser;
    NonstandardAttributeParser *&m_nonstandardAttributeParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    QScopedPointer<Component> m_result;
};

}