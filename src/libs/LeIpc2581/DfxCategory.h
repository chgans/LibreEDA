
#pragma once

namespace Ipc2581b
{

enum class DfxCategory
{
    Component,
    Testing,
    Dataquality,
    Assembly,
    Boardfab,
};

}