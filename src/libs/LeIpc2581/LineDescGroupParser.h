
#pragma once

#include <QString>
class QXmlStreamReader;

#include "LineDescGroup.h"

namespace Ipc2581b
{

class LineDescParser;
class LineDescRefParser;

class LineDescGroupParser
{
public:
    LineDescGroupParser(
            LineDescParser*&
            , LineDescRefParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    LineDescGroup *result();

private:
    LineDescParser *&m_lineDescParser;
    LineDescRefParser *&m_lineDescRefParser;
    LineDescGroup *m_result;
};

}