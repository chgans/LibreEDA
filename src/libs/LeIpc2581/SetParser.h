
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Set.h"

namespace Ipc2581b
{

class FiducialParser;
class ColorGroupParser;
class IdRefParser;
class QStringParser;
class SlotCavityParser;
class HoleParser;
class PolarityParser;
class LineDescGroupParser;
class PadUsageParser;
class PadParser;
class BoolParser;
class FeaturesParser;
class NonstandardAttributeParser;

class SetParser
{
public:
    SetParser(
            QStringParser*&
            , PolarityParser*&
            , PadUsageParser*&
            , BoolParser*&
            , QStringParser*&
            , BoolParser*&
            , QStringParser*&
            , NonstandardAttributeParser*&
            , PadParser*&
            , FiducialParser*&
            , HoleParser*&
            , SlotCavityParser*&
            , IdRefParser*&
            , FeaturesParser*&
            , ColorGroupParser*&
            , LineDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Set *result();

private:
    QStringParser *&m_netParser;
    PolarityParser *&m_polarityParser;
    PadUsageParser *&m_padUsageParser;
    BoolParser *&m_testPointParser;
    QStringParser *&m_geometryParser;
    BoolParser *&m_plateParser;
    QStringParser *&m_componentRefParser;
    NonstandardAttributeParser *&m_nonstandardAttributeParser;
    PadParser *&m_padParser;
    FiducialParser *&m_fiducialParser;
    HoleParser *&m_holeParser;
    SlotCavityParser *&m_slotCavityParser;
    IdRefParser *&m_specRefParser;
    FeaturesParser *&m_featuresParser;
    ColorGroupParser *&m_colorGroupParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    QScopedPointer<Set> m_result;
};

}