
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DocDes.h"

namespace Ipc2581b
{

class QStringParser;

class DocDesParser
{
public:
    DocDesParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DocDes *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_layerRefParser;
    QScopedPointer<DocDes> m_result;
};

}