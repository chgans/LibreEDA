
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "CadHeader.h"

namespace Ipc2581b
{

class UnitsParser;
class SpecParser;
class ChangeRecParser;

class CadHeaderParser
{
public:
    CadHeaderParser(
            UnitsParser*&
            , SpecParser*&
            , ChangeRecParser*&
            );

    bool parse(QXmlStreamReader *reader);
    CadHeader *result();

private:
    UnitsParser *&m_unitsParser;
    SpecParser *&m_specParser;
    ChangeRecParser *&m_changeRecParser;
    QScopedPointer<CadHeader> m_result;
};

}