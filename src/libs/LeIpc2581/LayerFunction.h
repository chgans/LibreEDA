
#pragma once

namespace Ipc2581b
{

enum class LayerFunction
{
    Component,
    EmbeddedComponent,
    Dielbase,
    BoardOutline,
    Assembly,
    StackupComposite,
    Plane,
    Silkscreen,
    Rework,
    Courtyard,
    Solderbump,
    Boardfab,
    Probe,
    Pin,
    VCut,
    Graphic,
    Coatingnoncond,
    Condfilm,
    Other,
    Fixture,
    Capacitive,
    Dieladhv,
    ConductiveAdhesive,
    Signal,
    Coatingcond,
    Conductor,
    ComponentTop,
    Drill,
    Resistive,
    Soldermask,
    Dielcore,
    Glue,
    Mixed,
    Rout,
    Document,
    Pastemask,
    Condfoil,
    Holefill,
    Solderpaste,
    Dielpreg,
    Legend,
    Landpattern,
    ComponentBottom,
};

}