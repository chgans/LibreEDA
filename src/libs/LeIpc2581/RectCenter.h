
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class RectCenter: public StandardPrimitive
{
public:
	virtual ~RectCenter() {}

    Double width;
    Double height;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::RectCenter;
    }
};

}