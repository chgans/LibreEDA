
#pragma once

#include <QList>
#include "Optional.h"


#include "CadProperty.h"
#include "QString.h"

namespace Ipc2581b
{

class NonstandardAttribute
{
public:
	virtual ~NonstandardAttribute() {}

    QString name;
    CadProperty type;
    QString value;

};

}