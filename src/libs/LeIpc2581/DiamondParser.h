
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Diamond.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class DiamondParser
{
public:
    DiamondParser(
            DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Diamond *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Diamond> m_result;
};

}