
#pragma once

#include <QList>
#include "Optional.h"


#include "Double.h"
#include "PlatingStatus.h"
#include "QString.h"

namespace Ipc2581b
{

class PadstackHoleDef
{
public:
	virtual ~PadstackHoleDef() {}

    QString name;
    Double diameter;
    PlatingStatus platingStatus;
    Double plusTol;
    Double minusTol;
    Double x;
    Double y;

};

}