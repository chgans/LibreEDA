
#pragma once

namespace Ipc2581b
{

enum class Units
{
    Millimeter,
    Micron,
    Inch,
};

}