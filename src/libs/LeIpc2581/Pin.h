
#pragma once

#include <QList>
#include "Optional.h"


#include "PinElectrical.h"
#include "Xform.h"
#include "QString.h"
#include "PinMount.h"
#include "StandardShape.h"
#include "CadPin.h"
#include "Location.h"

namespace Ipc2581b
{

class Pin
{
public:
	virtual ~Pin() {}

    QString number;
    Optional<QString> nameOptional;
    CadPin type;
    Optional<PinElectrical> electricalTypeOptional;
    Optional<PinMount> mountTypeOptional;
    Optional<Xform*> xformOptional;
    Optional<Location*> locationOptional;
    StandardShape *standardShape;

};

}