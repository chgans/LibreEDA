
#pragma once

namespace Ipc2581b
{

enum class PropertyUnit
{
    Faranheit,
    Section,
    Degrees,
    Hz,
    Class,
    Ohms,
    Ohm,
    MhoPerCm,
    Inch,
    Percent,
    Rms,
    SiemensPerM,
    Micron,
    Other,
    Gauge,
    Item,
    Rz,
    Rmax,
    Celcius,
    Mm,
};

}