
#pragma once

#include <QString>

class QXmlStreamReader;

#include "DfxCategory.h"

namespace Ipc2581b
{

class DfxCategoryParser
{
public:
    DfxCategoryParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    DfxCategory result();

private:
    DfxCategory m_result;
};

}