
#pragma once

namespace Ipc2581b
{

enum class CertificationStatus
{
    Alpha,
    Selftest,
    Certified,
    Beta,
};

}