
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PolyBegin.h"

namespace Ipc2581b
{

class DoubleParser;

class PolyBeginParser
{
public:
    PolyBeginParser(
            DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PolyBegin *result();

private:
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    QScopedPointer<PolyBegin> m_result;
};

}