
#pragma once

#include <QList>
#include "Optional.h"

#include "PolyStep.h"

#include "Double.h"

namespace Ipc2581b
{

class PolyStepSegment: public PolyStep
{
public:
	virtual ~PolyStepSegment() {}

    Double x;
    Double y;

    virtual PolyStepType polyStepType() const override
    {
        return PolyStepType::PolyStepSegment;
    }
};

}