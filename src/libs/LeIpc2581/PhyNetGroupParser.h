
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PhyNetGroup.h"

namespace Ipc2581b
{

class BoolParser;
class PhyNetParser;
class QStringParser;

class PhyNetGroupParser
{
public:
    PhyNetGroupParser(
            QStringParser*&
            , BoolParser*&
            , PhyNetParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PhyNetGroup *result();

private:
    QStringParser *&m_nameParser;
    BoolParser *&m_optimizedParser;
    PhyNetParser *&m_phyNetParser;
    QScopedPointer<PhyNetGroup> m_result;
};

}