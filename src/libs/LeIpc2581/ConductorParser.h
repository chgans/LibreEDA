
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Conductor.h"

namespace Ipc2581b
{

class PropertyParser;
class ConductorListParser;
class QStringParser;

class ConductorParser
{
public:
    ConductorParser(
            ConductorListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Conductor *result();

private:
    ConductorListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Conductor> m_result;
};

}