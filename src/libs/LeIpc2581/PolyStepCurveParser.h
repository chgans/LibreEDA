
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PolyStepCurve.h"

namespace Ipc2581b
{

class BoolParser;
class DoubleParser;

class PolyStepCurveParser
{
public:
    PolyStepCurveParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PolyStepCurve *result();

private:
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    DoubleParser *&m_centerXParser;
    DoubleParser *&m_centerYParser;
    BoolParser *&m_clockwiseParser;
    QScopedPointer<PolyStepCurve> m_result;
};

}