#include "FillDescGroupParser.h"

#include "FillDescRefParser.h"
#include "FillDescParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

FillDescGroupParser::FillDescGroupParser(
    FillDescParser *&_fillDescParser
    , FillDescRefParser *&_fillDescRefParser
):    m_fillDescParser(_fillDescParser)
    , m_fillDescRefParser(_fillDescRefParser)
{

}

bool FillDescGroupParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("FillDesc"))
    {
        if(m_fillDescParser->parse(reader))
        {
            m_result = m_fillDescParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("FillDescRef"))
    {
        if(m_fillDescRefParser->parse(reader))
        {
            m_result = m_fillDescRefParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"FillDescGroup\"").arg(reader->name().toString()));
    return false;
}

FillDescGroup *FillDescGroupParser::result()
{
    return m_result;
}

bool FillDescGroupParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("FillDesc"))
        return true;
    if (name == QStringLiteral("FillDescRef"))
        return true;
    return false;
}

}