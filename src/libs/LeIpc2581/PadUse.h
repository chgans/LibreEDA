
#pragma once

namespace Ipc2581b
{

enum class PadUse
{
    Other,
    Regular,
    Thermal,
    Antipad,
};

}