
#pragma once

#include <QString>
class QXmlStreamReader;

#include "StandardShape.h"

namespace Ipc2581b
{

class StandardPrimitiveParser;
class StandardPrimitiveRefParser;

class StandardShapeParser
{
public:
    StandardShapeParser(
            StandardPrimitiveParser*&
            , StandardPrimitiveRefParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    StandardShape *result();

private:
    StandardPrimitiveParser *&m_standardPrimitiveParser;
    StandardPrimitiveRefParser *&m_standardPrimitiveRefParser;
    StandardShape *m_result;
};

}