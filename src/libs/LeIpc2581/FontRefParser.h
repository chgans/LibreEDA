
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FontRef.h"

namespace Ipc2581b
{

class QStringParser;

class FontRefParser
{
public:
    FontRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FontRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<FontRef> m_result;
};

}