
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Mode.h"

namespace Ipc2581b
{

class ModeParser
{
public:
    ModeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Mode result();

private:
    Mode m_result;
};

}