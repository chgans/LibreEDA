
#pragma once

#include <QList>
#include "Optional.h"


#include "Double.h"

namespace Ipc2581b
{

class Location
{
public:
	virtual ~Location() {}

    Double x;
    Double y;

};

}