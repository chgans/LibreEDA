
#pragma once


namespace Ipc2581b
{

class FontDefEmbedded;
class FontDefExternal;

class FontDef
{
public:
	virtual ~FontDef() {}

    enum class FontDefType
    {
        FontDefEmbedded
        ,FontDefExternal
    };

    virtual FontDefType fontDefType() const = 0;

    inline bool isFontDefEmbedded() const
    {
        return fontDefType() == FontDefType::FontDefEmbedded;
    }

    inline FontDefEmbedded *toFontDefEmbedded()
    {
        return reinterpret_cast<FontDefEmbedded*>(this);
    }

    inline const FontDefEmbedded *toFontDefEmbedded() const
    {
        return reinterpret_cast<const FontDefEmbedded*>(this);
    }

    inline bool isFontDefExternal() const
    {
        return fontDefType() == FontDefType::FontDefExternal;
    }

    inline FontDefExternal *toFontDefExternal()
    {
        return reinterpret_cast<FontDefExternal*>(this);
    }

    inline const FontDefExternal *toFontDefExternal() const
    {
        return reinterpret_cast<const FontDefExternal*>(this);
    }


};

}

// For user convenience
#include "FontDefEmbedded.h"
#include "FontDefExternal.h"
