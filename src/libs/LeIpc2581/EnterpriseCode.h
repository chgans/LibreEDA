
#pragma once

namespace Ipc2581b
{

enum class EnterpriseCode
{
    Cage,
    Dunns,
};

}