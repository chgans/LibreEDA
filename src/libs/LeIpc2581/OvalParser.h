
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Oval.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class OvalParser
{
public:
    OvalParser(
            DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Oval *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Oval> m_result;
};

}