
#pragma once

#include <QList>
#include "Optional.h"

#include "Simple.h"

#include "LineDescGroup.h"
#include "PolyBegin.h"
#include "PolyStep.h"

namespace Ipc2581b
{

class Polyline: public Simple
{
public:
	virtual ~Polyline() {}

    PolyBegin *polyBegin;
    QList<PolyStep*> polyStepList;
    LineDescGroup *lineDescGroup;

    virtual SimpleType simpleType() const override
    {
        return SimpleType::Polyline;
    }
};

}