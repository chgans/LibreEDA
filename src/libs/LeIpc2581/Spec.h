
#pragma once

#include <QList>
#include "Optional.h"


#include "Location.h"
#include "Outline.h"
#include "SpecificationType.h"
#include "Xform.h"
#include "QString.h"

namespace Ipc2581b
{

class Spec
{
public:
	virtual ~Spec() {}

    QString name;
    QList<SpecificationType*> specificationTypeList;
    Optional<Xform*> xformOptional;
    Optional<Location*> locationOptional;
    Optional<Outline*> outlineOptional;

};

}