
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "EntryUser.h"

namespace Ipc2581b
{

class DictionaryUser
{
public:
	virtual ~DictionaryUser() {}

    Units units;
    QList<EntryUser*> entryUserList;

};

}