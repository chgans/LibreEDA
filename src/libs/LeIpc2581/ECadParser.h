
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "ECad.h"

namespace Ipc2581b
{

class CadHeaderParser;
class CadDataParser;
class QStringParser;

class ECadParser
{
public:
    ECadParser(
            QStringParser*&
            , CadHeaderParser*&
            , CadDataParser*&
            );

    bool parse(QXmlStreamReader *reader);
    ECad *result();

private:
    QStringParser *&m_nameParser;
    CadHeaderParser *&m_cadHeaderParser;
    CadDataParser *&m_cadDataParser;
    QScopedPointer<ECad> m_result;
};

}