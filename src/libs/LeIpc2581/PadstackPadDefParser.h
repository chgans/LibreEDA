
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PadstackPadDef.h"

namespace Ipc2581b
{

class LocationParser;
class XformParser;
class FeatureParser;
class PadUseParser;
class QStringParser;

class PadstackPadDefParser
{
public:
    PadstackPadDefParser(
            QStringParser*&
            , PadUseParser*&
            , QStringParser*&
            , XformParser*&
            , LocationParser*&
            , FeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PadstackPadDef *result();

private:
    QStringParser *&m_layerRefParser;
    PadUseParser *&m_padUseParser;
    QStringParser *&m_commentParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    FeatureParser *&m_featureParser;
    QScopedPointer<PadstackPadDef> m_result;
};

}