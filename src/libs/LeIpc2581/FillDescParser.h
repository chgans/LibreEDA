
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "FillDesc.h"

namespace Ipc2581b
{

class FillPropertyParser;
class ColorGroupParser;
class DoubleParser;

class FillDescParser
{
public:
    FillDescParser(
            FillPropertyParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , ColorGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    FillDesc *result();

private:
    FillPropertyParser *&m_fillPropertyParser;
    DoubleParser *&m_lineWidthParser;
    DoubleParser *&m_pitch1Parser;
    DoubleParser *&m_pitch2Parser;
    DoubleParser *&m_angle1Parser;
    DoubleParser *&m_angle2Parser;
    ColorGroupParser *&m_colorParser;
    QScopedPointer<FillDesc> m_result;
};

}