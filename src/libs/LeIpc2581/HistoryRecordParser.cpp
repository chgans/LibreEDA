#include "HistoryRecordParser.h"

#include "ChangeRecParser.h"
#include "FileRevisionParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

HistoryRecordParser::HistoryRecordParser(
    QStringParser *&_numberParser
    , QStringParser *&_originationParser
    , QStringParser *&_softwareParser
    , QStringParser *&_lastChangeParser
    , QStringParser *&_externalConfigurationEntryParser
    , FileRevisionParser *&_fileRevisionParser
    , ChangeRecParser *&_changeRecParser
):    m_numberParser(_numberParser)
    , m_originationParser(_originationParser)
    , m_softwareParser(_softwareParser)
    , m_lastChangeParser(_lastChangeParser)
    , m_externalConfigurationEntryParser(_externalConfigurationEntryParser)
    , m_fileRevisionParser(_fileRevisionParser)
    , m_changeRecParser(_changeRecParser)
{

}

bool HistoryRecordParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new HistoryRecord());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("number")))
    {
        data = reader->attributes().value(QStringLiteral("number"));
        if (!m_numberParser->parse(reader, data))
            return false;
        m_result->number = m_numberParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("number: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("origination")))
    {
        data = reader->attributes().value(QStringLiteral("origination"));
        if (!m_originationParser->parse(reader, data))
            return false;
        m_result->origination = m_originationParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("origination: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("software")))
    {
        data = reader->attributes().value(QStringLiteral("software"));
        if (!m_softwareParser->parse(reader, data))
            return false;
        m_result->software = m_softwareParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("software: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lastChange")))
    {
        data = reader->attributes().value(QStringLiteral("lastChange"));
        if (!m_lastChangeParser->parse(reader, data))
            return false;
        m_result->lastChange = m_lastChangeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lastChange: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("externalConfigurationEntry")))
    {
        data = reader->attributes().value(QStringLiteral("externalConfigurationEntry"));
        if (!m_externalConfigurationEntryParser->parse(reader, data))
            return false;
        m_result->externalConfigurationEntryOptional = Optional<QString>(m_externalConfigurationEntryParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("FileRevision"))
        {
            if (!m_fileRevisionParser->parse(reader))
                return false;
            auto result = m_fileRevisionParser->result();
            m_result->fileRevision = result;
        }
        else if (name == QStringLiteral("ChangeRec"))
        {
            if (!m_changeRecParser->parse(reader))
                return false;
            auto result = m_changeRecParser->result();
            m_result->changeRecList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

HistoryRecord *HistoryRecordParser::result()
{
    return m_result.take();
}

}