
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "Spec.h"
#include "ChangeRec.h"

namespace Ipc2581b
{

class CadHeader
{
public:
	virtual ~CadHeader() {}

    Units units;
    QList<Spec*> specList;
    QList<ChangeRec*> changeRecList;

};

}