#include "CharacteristicsParser.h"


#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

CharacteristicsParser::CharacteristicsParser(
){

}

bool CharacteristicsParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Characteristics());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Characteristics *CharacteristicsParser::result()
{
    return m_result.take();
}

}