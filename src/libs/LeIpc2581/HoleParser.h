
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Hole.h"

namespace Ipc2581b
{

class IdRefParser;
class DoubleParser;
class PlatingStatusParser;
class QStringParser;

class HoleParser
{
public:
    HoleParser(
            QStringParser*&
            , DoubleParser*&
            , PlatingStatusParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , IdRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Hole *result();

private:
    QStringParser *&m_nameParser;
    DoubleParser *&m_diameterParser;
    PlatingStatusParser *&m_platingStatusParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    IdRefParser *&m_specRefParser;
    QScopedPointer<Hole> m_result;
};

}