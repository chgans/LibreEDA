#include "ThermalShapeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ThermalShapeParser::ThermalShapeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ThermalShapeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ThermalShape> map =
    {
        {QStringLiteral("ROUND"), ThermalShape::Round},
        {QStringLiteral("HEXAGON"), ThermalShape::Hexagon},
        {QStringLiteral("OCTAGON"), ThermalShape::Octagon},
        {QStringLiteral("SQUARE"), ThermalShape::Square},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ThermalShape").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ThermalShape ThermalShapeParser::result()
{
    return m_result;
}

}