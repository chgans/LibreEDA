
#pragma once

#include <QString>

class QXmlStreamReader;

#include "StructureList.h"

namespace Ipc2581b
{

class StructureListParser
{
public:
    StructureListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    StructureList result();

private:
    StructureList m_result;
};

}