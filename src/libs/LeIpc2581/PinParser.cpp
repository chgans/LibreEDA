#include "PinParser.h"

#include "PinElectricalParser.h"
#include "XformParser.h"
#include "QStringParser.h"
#include "PinMountParser.h"
#include "StandardShapeParser.h"
#include "CadPinParser.h"
#include "LocationParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

PinParser::PinParser(
    QStringParser *&_numberParser
    , QStringParser *&_nameParser
    , CadPinParser *&_typeParser
    , PinElectricalParser *&_electricalTypeParser
    , PinMountParser *&_mountTypeParser
    , XformParser *&_xformParser
    , LocationParser *&_locationParser
    , StandardShapeParser *&_standardShapeParser
):    m_numberParser(_numberParser)
    , m_nameParser(_nameParser)
    , m_typeParser(_typeParser)
    , m_electricalTypeParser(_electricalTypeParser)
    , m_mountTypeParser(_mountTypeParser)
    , m_xformParser(_xformParser)
    , m_locationParser(_locationParser)
    , m_standardShapeParser(_standardShapeParser)
{

}

bool PinParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Pin());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("number")))
    {
        data = reader->attributes().value(QStringLiteral("number"));
        if (!m_numberParser->parse(reader, data))
            return false;
        m_result->number = m_numberParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("number: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->nameOptional = Optional<QString>(m_nameParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("type")))
    {
        data = reader->attributes().value(QStringLiteral("type"));
        if (!m_typeParser->parse(reader, data))
            return false;
        m_result->type = m_typeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("type: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("electricalType")))
    {
        data = reader->attributes().value(QStringLiteral("electricalType"));
        if (!m_electricalTypeParser->parse(reader, data))
            return false;
        m_result->electricalTypeOptional = Optional<PinElectrical>(m_electricalTypeParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("mountType")))
    {
        data = reader->attributes().value(QStringLiteral("mountType"));
        if (!m_mountTypeParser->parse(reader, data))
            return false;
        m_result->mountTypeOptional = Optional<PinMount>(m_mountTypeParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("Location"))
        {
            if (!m_locationParser->parse(reader))
                return false;
            auto result = m_locationParser->result();
            m_result->locationOptional = Optional<Location*>(result);
        }
        else if (m_standardShapeParser->isSubstitution(name))
        {
            if (!m_standardShapeParser->parse(reader))
                return false;
            auto result = m_standardShapeParser->result();
            m_result->standardShape = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Pin *PinParser::result()
{
    return m_result.take();
}

}