
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Xform.h"

namespace Ipc2581b
{

class BoolParser;
class DoubleParser;

class XformParser
{
public:
    XformParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Xform *result();

private:
    DoubleParser *&m_xOffsetParser;
    DoubleParser *&m_yOffsetParser;
    DoubleParser *&m_rotationParser;
    BoolParser *&m_mirrorParser;
    DoubleParser *&m_scaleParser;
    QScopedPointer<Xform> m_result;
};

}