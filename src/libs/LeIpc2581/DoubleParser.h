
#pragma once

#include <QString>

class QXmlStreamReader;

namespace Ipc2581b
{

class DoubleParser
{
public:
    DoubleParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    double result();

private:
    double m_result;
};

}