
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Stackup.h"

namespace Ipc2581b
{

class WhereMeasuredParser;
class IdRefParser;
class QStringParser;
class MatDesParser;
class StackupGroupParser;
class DoubleParser;

class StackupParser
{
public:
    StackupParser(
            QStringParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , WhereMeasuredParser*&
            , QStringParser*&
            , MatDesParser*&
            , IdRefParser*&
            , StackupGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Stackup *result();

private:
    QStringParser *&m_nameParser;
    DoubleParser *&m_overallThicknessParser;
    DoubleParser *&m_tolPlusParser;
    DoubleParser *&m_tolMinusParser;
    WhereMeasuredParser *&m_whereMeasuredParser;
    QStringParser *&m_commentParser;
    MatDesParser *&m_matDesParser;
    IdRefParser *&m_specRefParser;
    StackupGroupParser *&m_stackupGroupParser;
    QScopedPointer<Stackup> m_result;
};

}