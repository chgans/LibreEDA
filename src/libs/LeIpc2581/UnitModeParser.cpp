#include "UnitModeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

UnitModeParser::UnitModeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool UnitModeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, UnitMode> map =
    {
        {QStringLiteral("CAPACITANCE"), UnitMode::Capacitance},
        {QStringLiteral("SIZE"), UnitMode::Size},
        {QStringLiteral("AREA"), UnitMode::Area},
        {QStringLiteral("DISTANCE"), UnitMode::Distance},
        {QStringLiteral("RESISTANCE"), UnitMode::Resistance},
        {QStringLiteral("NONE"), UnitMode::None},
        {QStringLiteral("IMPEDANCE"), UnitMode::Impedance},
        {QStringLiteral("PERCENTAGE"), UnitMode::Percentage},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum UnitMode").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

UnitMode UnitModeParser::result()
{
    return m_result;
}

}