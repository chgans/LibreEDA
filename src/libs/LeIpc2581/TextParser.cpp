#include "TextParser.h"

#include "ColorGroupParser.h"
#include "BoundingBoxParser.h"
#include "XformParser.h"
#include "QStringParser.h"
#include "FontRefParser.h"
#include "IntParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

TextParser::TextParser(
    QStringParser *&_textStringParser
    , IntParser *&_fontSizeParser
    , XformParser *&_xformParser
    , BoundingBoxParser *&_boundingBoxParser
    , FontRefParser *&_fontRefParser
    , ColorGroupParser *&_colorGroupParser
):    m_textStringParser(_textStringParser)
    , m_fontSizeParser(_fontSizeParser)
    , m_xformParser(_xformParser)
    , m_boundingBoxParser(_boundingBoxParser)
    , m_fontRefParser(_fontRefParser)
    , m_colorGroupParser(_colorGroupParser)
{

}

bool TextParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Text());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("textString")))
    {
        data = reader->attributes().value(QStringLiteral("textString"));
        if (!m_textStringParser->parse(reader, data))
            return false;
        m_result->textString = m_textStringParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("textString: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("fontSize")))
    {
        data = reader->attributes().value(QStringLiteral("fontSize"));
        if (!m_fontSizeParser->parse(reader, data))
            return false;
        m_result->fontSize = m_fontSizeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("fontSize: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("BoundingBox"))
        {
            if (!m_boundingBoxParser->parse(reader))
                return false;
            auto result = m_boundingBoxParser->result();
            m_result->boundingBox = result;
        }
        else if (name == QStringLiteral("FontRef"))
        {
            if (!m_fontRefParser->parse(reader))
                return false;
            auto result = m_fontRefParser->result();
            m_result->fontRef = result;
        }
        else if (m_colorGroupParser->isSubstitution(name))
        {
            if (!m_colorGroupParser->parse(reader))
                return false;
            auto result = m_colorGroupParser->result();
            m_result->colorGroup = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Text *TextParser::result()
{
    return m_result.take();
}

}