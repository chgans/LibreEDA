
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "General.h"

namespace Ipc2581b
{

class QStringParser;
class PropertyParser;
class GeneralListParser;

class GeneralParser
{
public:
    GeneralParser(
            GeneralListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    General *result();

private:
    GeneralListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<General> m_result;
};

}