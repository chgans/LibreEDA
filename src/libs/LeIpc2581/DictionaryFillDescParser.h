
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryFillDesc.h"

namespace Ipc2581b
{

class UnitsParser;
class EntryFillDescParser;

class DictionaryFillDescParser
{
public:
    DictionaryFillDescParser(
            UnitsParser*&
            , EntryFillDescParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryFillDesc *result();

private:
    UnitsParser *&m_unitsParser;
    EntryFillDescParser *&m_entryFillDescParser;
    QScopedPointer<DictionaryFillDesc> m_result;
};

}