
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "QString.h"
#include "Property.h"
#include "ComplianceList.h"

namespace Ipc2581b
{

class Compliance: public SpecificationType
{
public:
	virtual ~Compliance() {}

    ComplianceList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Compliance;
    }
};

}