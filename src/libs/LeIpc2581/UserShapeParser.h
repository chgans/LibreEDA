
#pragma once

#include <QString>
class QXmlStreamReader;

#include "UserShape.h"

namespace Ipc2581b
{

class UserPrimitiveParser;
class UserPrimitiveRefParser;

class UserShapeParser
{
public:
    UserShapeParser(
            UserPrimitiveParser*&
            , UserPrimitiveRefParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    UserShape *result();

private:
    UserPrimitiveParser *&m_userPrimitiveParser;
    UserPrimitiveRefParser *&m_userPrimitiveRefParser;
    UserShape *m_result;
};

}