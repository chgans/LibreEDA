
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "V_Cut.h"

namespace Ipc2581b
{

class QStringParser;
class PropertyParser;
class VCutListParser;

class V_CutParser
{
public:
    V_CutParser(
            VCutListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    V_Cut *result();

private:
    VCutListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<V_Cut> m_result;
};

}