
#pragma once

#include <QString>

class QXmlStreamReader;

namespace Ipc2581b
{

class IntParser
{
public:
    IntParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    int result();

private:
    int m_result;
};

}