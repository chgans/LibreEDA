
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "StandardPrimitiveRef.h"

namespace Ipc2581b
{

class QStringParser;

class StandardPrimitiveRefParser
{
public:
    StandardPrimitiveRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    StandardPrimitiveRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<StandardPrimitiveRef> m_result;
};

}