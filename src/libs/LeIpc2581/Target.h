
#pragma once

#include <QList>
#include "Optional.h"


#include "Location.h"
#include "StandardShape.h"
#include "Xform.h"

namespace Ipc2581b
{

class Target
{
public:
	virtual ~Target() {}

    Optional<Xform*> xformOptional;
    Location *location;
    StandardShape *standardShape;

};

}