
#pragma once

namespace Ipc2581b
{

enum class PinElectrical
{
    Mechanical,
    Electrical,
    Undefined,
};

}