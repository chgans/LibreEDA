
#pragma once

namespace Ipc2581b
{

enum class BomCategory
{
    Programmable,
    Mechanical,
    Electrical,
    Document,
    Material,
};

}