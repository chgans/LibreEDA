
#pragma once

#include <QString>
class QXmlStreamReader;

#include "FontDef.h"

namespace Ipc2581b
{

class FontDefEmbeddedParser;
class FontDefExternalParser;

class FontDefParser
{
public:
    FontDefParser(
            FontDefEmbeddedParser*&
            , FontDefExternalParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    FontDef *result();

private:
    FontDefEmbeddedParser *&m_fontDefEmbeddedParser;
    FontDefExternalParser *&m_fontDefExternalParser;
    FontDef *m_result;
};

}