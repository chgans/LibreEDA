
#pragma once

#include <QList>
#include "Optional.h"


#include "Units.h"
#include "EntryStandard.h"

namespace Ipc2581b
{

class DictionaryStandard
{
public:
	virtual ~DictionaryStandard() {}

    Units units;
    QList<EntryStandard*> entryStandardList;

};

}