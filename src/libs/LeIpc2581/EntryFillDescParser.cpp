#include "EntryFillDescParser.h"

#include "FillDescParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

EntryFillDescParser::EntryFillDescParser(
    QStringParser *&_idParser
    , FillDescParser *&_fillDescParser
):    m_idParser(_idParser)
    , m_fillDescParser(_fillDescParser)
{

}

bool EntryFillDescParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new EntryFillDesc());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("FillDesc"))
        {
            if (!m_fillDescParser->parse(reader))
                return false;
            auto result = m_fillDescParser->result();
            m_result->fillDesc = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

EntryFillDesc *EntryFillDescParser::result()
{
    return m_result.take();
}

}