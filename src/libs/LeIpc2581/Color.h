
#pragma once

#include <QList>
#include "Optional.h"

#include "ColorGroup.h"

#include "Int.h"

namespace Ipc2581b
{

class Color: public ColorGroup
{
public:
	virtual ~Color() {}

    Int r;
    Int g;
    Int b;

    virtual ColorGroupType colorGroupType() const override
    {
        return ColorGroupType::Color;
    }
};

}