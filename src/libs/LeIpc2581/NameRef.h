
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class NameRef
{
public:
	virtual ~NameRef() {}

    QString name;

};

}