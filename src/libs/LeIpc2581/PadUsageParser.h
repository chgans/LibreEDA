
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PadUsage.h"

namespace Ipc2581b
{

class PadUsageParser
{
public:
    PadUsageParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PadUsage result();

private:
    PadUsage m_result;
};

}