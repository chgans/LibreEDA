
#pragma once

#include <QString>
class QXmlStreamReader;

#include "SpecificationType.h"

namespace Ipc2581b
{

class DielectricParser;
class BackdrillParser;
class ComplianceParser;
class V_CutParser;
class ToolParser;
class ImpedanceParser;
class ConductorParser;
class GeneralParser;
class TechnologyParser;
class TemperatureParser;

class SpecificationTypeParser
{
public:
    SpecificationTypeParser(
            BackdrillParser*&
            , ComplianceParser*&
            , ConductorParser*&
            , DielectricParser*&
            , GeneralParser*&
            , ImpedanceParser*&
            , TechnologyParser*&
            , TemperatureParser*&
            , ToolParser*&
            , V_CutParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    SpecificationType *result();

private:
    BackdrillParser *&m_backdrillParser;
    ComplianceParser *&m_complianceParser;
    ConductorParser *&m_conductorParser;
    DielectricParser *&m_dielectricParser;
    GeneralParser *&m_generalParser;
    ImpedanceParser *&m_impedanceParser;
    TechnologyParser *&m_technologyParser;
    TemperatureParser *&m_temperatureParser;
    ToolParser *&m_toolParser;
    V_CutParser *&m_v_CutParser;
    SpecificationType *m_result;
};

}