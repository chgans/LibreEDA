#include "SimpleParser.h"

#include "LineParser.h"
#include "OutlineParser.h"
#include "ArcParser.h"
#include "PolylineParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

SimpleParser::SimpleParser(
    ArcParser *&_arcParser
    , LineParser *&_lineParser
    , OutlineParser *&_outlineParser
    , PolylineParser *&_polylineParser
):    m_arcParser(_arcParser)
    , m_lineParser(_lineParser)
    , m_outlineParser(_outlineParser)
    , m_polylineParser(_polylineParser)
{

}

bool SimpleParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("Arc"))
    {
        if(m_arcParser->parse(reader))
        {
            m_result = m_arcParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Line"))
    {
        if(m_lineParser->parse(reader))
        {
            m_result = m_lineParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Outline"))
    {
        if(m_outlineParser->parse(reader))
        {
            m_result = m_outlineParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("Polyline"))
    {
        if(m_polylineParser->parse(reader))
        {
            m_result = m_polylineParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"Simple\"").arg(reader->name().toString()));
    return false;
}

Simple *SimpleParser::result()
{
    return m_result;
}

bool SimpleParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("Arc"))
        return true;
    if (name == QStringLiteral("Line"))
        return true;
    if (name == QStringLiteral("Outline"))
        return true;
    if (name == QStringLiteral("Polyline"))
        return true;
    return false;
}

}