#include "TransmissionListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

TransmissionListParser::TransmissionListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool TransmissionListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, TransmissionList> map =
    {
        {QStringLiteral("OTHER"), TransmissionList::Other},
        {QStringLiteral("SINGLE_ENDED"), TransmissionList::SingleEnded},
        {QStringLiteral("EDGE_COUPLED"), TransmissionList::EdgeCoupled},
        {QStringLiteral("BROADSIDE_COUPLED"), TransmissionList::BroadsideCoupled},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum TransmissionList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

TransmissionList TransmissionListParser::result()
{
    return m_result;
}

}