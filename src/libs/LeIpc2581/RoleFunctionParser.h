
#pragma once

#include <QString>

class QXmlStreamReader;

#include "RoleFunction.h"

namespace Ipc2581b
{

class RoleFunctionParser
{
public:
    RoleFunctionParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    RoleFunction result();

private:
    RoleFunction m_result;
};

}