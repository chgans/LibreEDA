
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "SlotCavity.h"

namespace Ipc2581b
{

class SimpleParser;
class DoubleParser;
class PlatingStatusParser;
class ZAxisDimParser;
class QStringParser;

class SlotCavityParser
{
public:
    SlotCavityParser(
            QStringParser*&
            , PlatingStatusParser*&
            , DoubleParser*&
            , DoubleParser*&
            , SimpleParser*&
            , ZAxisDimParser*&
            );

    bool parse(QXmlStreamReader *reader);
    SlotCavity *result();

private:
    QStringParser *&m_nameParser;
    PlatingStatusParser *&m_platingStatusParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    SimpleParser *&m_simpleParser;
    ZAxisDimParser *&m_z_AxisDimParser;
    QScopedPointer<SlotCavity> m_result;
};

}