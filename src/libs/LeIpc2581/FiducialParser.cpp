#include "FiducialParser.h"

#include "LocalFiducialParser.h"
#include "GoodPanelMarkParser.h"
#include "BadBoardMarkParser.h"
#include "GlobalFiducialParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

FiducialParser::FiducialParser(
    BadBoardMarkParser *&_badBoardMarkParser
    , GlobalFiducialParser *&_globalFiducialParser
    , GoodPanelMarkParser *&_goodPanelMarkParser
    , LocalFiducialParser *&_localFiducialParser
):    m_badBoardMarkParser(_badBoardMarkParser)
    , m_globalFiducialParser(_globalFiducialParser)
    , m_goodPanelMarkParser(_goodPanelMarkParser)
    , m_localFiducialParser(_localFiducialParser)
{

}

bool FiducialParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("BadBoardMark"))
    {
        if(m_badBoardMarkParser->parse(reader))
        {
            m_result = m_badBoardMarkParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("GlobalFiducial"))
    {
        if(m_globalFiducialParser->parse(reader))
        {
            m_result = m_globalFiducialParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("GoodPanelMark"))
    {
        if(m_goodPanelMarkParser->parse(reader))
        {
            m_result = m_goodPanelMarkParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("LocalFiducial"))
    {
        if(m_localFiducialParser->parse(reader))
        {
            m_result = m_localFiducialParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"Fiducial\"").arg(reader->name().toString()));
    return false;
}

Fiducial *FiducialParser::result()
{
    return m_result;
}

bool FiducialParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("BadBoardMark"))
        return true;
    if (name == QStringLiteral("GlobalFiducial"))
        return true;
    if (name == QStringLiteral("GoodPanelMark"))
        return true;
    if (name == QStringLiteral("LocalFiducial"))
        return true;
    return false;
}

}