
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class IdRef
{
public:
	virtual ~IdRef() {}

    QString id;

};

}