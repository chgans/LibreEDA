
#pragma once

namespace Ipc2581b
{

enum class LineProperty
{
    Solid,
    Dashed,
    Phantom,
    Erase,
    Dotted,
    Center,
};

}