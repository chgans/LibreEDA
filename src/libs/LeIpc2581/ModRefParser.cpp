#include "ModRefParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ModRefParser::ModRefParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ModRefParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ModRef> map =
    {
        {QStringLiteral("DESIGN"), ModRef::Design},
        {QStringLiteral("ASSEMBLY"), ModRef::Assembly},
        {QStringLiteral("FABRICATION"), ModRef::Fabrication},
        {QStringLiteral("TEST"), ModRef::Test},
        {QStringLiteral("USERDEF"), ModRef::Userdef},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ModRef").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ModRef ModRefParser::result()
{
    return m_result;
}

}