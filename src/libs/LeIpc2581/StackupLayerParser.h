
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "StackupLayer.h"

namespace Ipc2581b
{

class MatDesParser;
class DoubleParser;
class IdRefParser;
class QStringParser;

class StackupLayerParser
{
public:
    StackupLayerParser(
            QStringParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , QStringParser*&
            , MatDesParser*&
            , IdRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    StackupLayer *result();

private:
    QStringParser *&m_layerOrGroupRefParser;
    DoubleParser *&m_thicknessParser;
    DoubleParser *&m_tolPlusParser;
    DoubleParser *&m_tolMinusParser;
    DoubleParser *&m_sequenceParser;
    QStringParser *&m_commentParser;
    MatDesParser *&m_matDesParser;
    IdRefParser *&m_specRefParser;
    QScopedPointer<StackupLayer> m_result;
};

}