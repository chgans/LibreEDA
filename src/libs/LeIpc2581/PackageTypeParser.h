
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PackageType.h"

namespace Ipc2581b
{

class PackageTypeParser
{
public:
    PackageTypeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PackageType result();

private:
    PackageType m_result;
};

}