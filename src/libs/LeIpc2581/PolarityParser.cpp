#include "PolarityParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PolarityParser::PolarityParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PolarityParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Polarity> map =
    {
        {QStringLiteral("NEGATIVE"), Polarity::Negative},
        {QStringLiteral("POSITIVE"), Polarity::Positive},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Polarity").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Polarity PolarityParser::result()
{
    return m_result;
}

}