
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "FillDescGroup.h"
#include "Double.h"
#include "Xform.h"
#include "DonutShape.h"

namespace Ipc2581b
{

class Donut: public StandardPrimitive
{
public:
	virtual ~Donut() {}

    DonutShape shape;
    Double outerDiameter;
    Double innerDiameter;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::Donut;
    }
};

}