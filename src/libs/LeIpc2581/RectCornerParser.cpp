#include "RectCornerParser.h"

#include "LineDescGroupParser.h"
#include "FillDescGroupParser.h"
#include "XformParser.h"
#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

RectCornerParser::RectCornerParser(
    DoubleParser *&_lowerLeftXParser
    , DoubleParser *&_lowerLeftYParser
    , DoubleParser *&_upperRightXParser
    , DoubleParser *&_upperRightYParser
    , XformParser *&_xformParser
    , LineDescGroupParser *&_lineDescGroupParser
    , FillDescGroupParser *&_fillDescGroupParser
):    m_lowerLeftXParser(_lowerLeftXParser)
    , m_lowerLeftYParser(_lowerLeftYParser)
    , m_upperRightXParser(_upperRightXParser)
    , m_upperRightYParser(_upperRightYParser)
    , m_xformParser(_xformParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_fillDescGroupParser(_fillDescGroupParser)
{

}

bool RectCornerParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new RectCorner());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("lowerLeftX")))
    {
        data = reader->attributes().value(QStringLiteral("lowerLeftX"));
        if (!m_lowerLeftXParser->parse(reader, data))
            return false;
        m_result->lowerLeftX = m_lowerLeftXParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lowerLeftX: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lowerLeftY")))
    {
        data = reader->attributes().value(QStringLiteral("lowerLeftY"));
        if (!m_lowerLeftYParser->parse(reader, data))
            return false;
        m_result->lowerLeftY = m_lowerLeftYParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lowerLeftY: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("upperRightX")))
    {
        data = reader->attributes().value(QStringLiteral("upperRightX"));
        if (!m_upperRightXParser->parse(reader, data))
            return false;
        m_result->upperRightX = m_upperRightXParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("upperRightX: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("upperRightY")))
    {
        data = reader->attributes().value(QStringLiteral("upperRightY"));
        if (!m_upperRightYParser->parse(reader, data))
            return false;
        m_result->upperRightY = m_upperRightYParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("upperRightY: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupOptional = Optional<LineDescGroup*>(result);
        }
        else if (m_fillDescGroupParser->isSubstitution(name))
        {
            if (!m_fillDescGroupParser->parse(reader))
                return false;
            auto result = m_fillDescGroupParser->result();
            m_result->fillDescGroupOptional = Optional<FillDescGroup*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

RectCorner *RectCornerParser::result()
{
    return m_result.take();
}

}