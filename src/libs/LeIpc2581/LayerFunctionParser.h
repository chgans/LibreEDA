
#pragma once

#include <QString>

class QXmlStreamReader;

#include "LayerFunction.h"

namespace Ipc2581b
{

class LayerFunctionParser
{
public:
    LayerFunctionParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    LayerFunction result();

private:
    LayerFunction m_result;
};

}