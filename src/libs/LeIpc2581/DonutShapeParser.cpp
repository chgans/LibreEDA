#include "DonutShapeParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

DonutShapeParser::DonutShapeParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool DonutShapeParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, DonutShape> map =
    {
        {QStringLiteral("ROUND"), DonutShape::Round},
        {QStringLiteral("HEXAGON"), DonutShape::Hexagon},
        {QStringLiteral("OCTAGON"), DonutShape::Octagon},
        {QStringLiteral("SQUARE"), DonutShape::Square},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum DonutShape").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

DonutShape DonutShapeParser::result()
{
    return m_result;
}

}