
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "StackupGroup.h"

namespace Ipc2581b
{

class StackupLayerParser;
class IdRefParser;
class QStringParser;
class MatDesParser;
class CADDataLayerRefParser;
class DoubleParser;

class StackupGroupParser
{
public:
    StackupGroupParser(
            QStringParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , QStringParser*&
            , MatDesParser*&
            , IdRefParser*&
            , StackupLayerParser*&
            , CADDataLayerRefParser*&
            );

    bool parse(QXmlStreamReader *reader);
    StackupGroup *result();

private:
    QStringParser *&m_nameParser;
    DoubleParser *&m_thicknessParser;
    DoubleParser *&m_tolPlusParser;
    DoubleParser *&m_tolMinusParser;
    QStringParser *&m_commentParser;
    MatDesParser *&m_matDesParser;
    IdRefParser *&m_specRefParser;
    StackupLayerParser *&m_stackupLayerParser;
    CADDataLayerRefParser *&m_cADDataLayerRefParser;
    QScopedPointer<StackupGroup> m_result;
};

}