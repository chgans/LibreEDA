
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "MatDes.h"

namespace Ipc2581b
{

class QStringParser;

class MatDesParser
{
public:
    MatDesParser(
            QStringParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    MatDes *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_layerRefParser;
    QScopedPointer<MatDes> m_result;
};

}