
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"

namespace Ipc2581b
{

class PinRef
{
public:
	virtual ~PinRef() {}

    Optional<QString> componentRefOptional;
    QString pin;
    Optional<QString> titleOptional;

};

}