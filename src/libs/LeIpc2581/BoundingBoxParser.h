
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "BoundingBox.h"

namespace Ipc2581b
{

class DoubleParser;

class BoundingBoxParser
{
public:
    BoundingBoxParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    BoundingBox *result();

private:
    DoubleParser *&m_lowerLeftXParser;
    DoubleParser *&m_lowerLeftYParser;
    DoubleParser *&m_upperRightXParser;
    DoubleParser *&m_upperRightYParser;
    QScopedPointer<BoundingBox> m_result;
};

}