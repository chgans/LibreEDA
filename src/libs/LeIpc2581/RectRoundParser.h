
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "RectRound.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class BoolParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class RectRoundParser
{
public:
    RectRoundParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            , BoolParser*&
            , BoolParser*&
            , BoolParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    RectRound *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    DoubleParser *&m_radiusParser;
    BoolParser *&m_upperRightParser;
    BoolParser *&m_upperLeftParser;
    BoolParser *&m_lowerRightParser;
    BoolParser *&m_lowerLeftParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<RectRound> m_result;
};

}