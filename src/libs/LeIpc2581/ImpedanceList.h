
#pragma once

namespace Ipc2581b
{

enum class ImpedanceList
{
    Other,
    RefPlaneLayerId,
    Spacing,
    Impedance,
    Linewidth,
};

}