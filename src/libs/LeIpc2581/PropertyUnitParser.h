
#pragma once

#include <QString>

class QXmlStreamReader;

#include "PropertyUnit.h"

namespace Ipc2581b
{

class PropertyUnitParser
{
public:
    PropertyUnitParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    PropertyUnit result();

private:
    PropertyUnit m_result;
};

}