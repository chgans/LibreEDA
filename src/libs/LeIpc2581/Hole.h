
#pragma once

#include <QList>
#include "Optional.h"


#include "IdRef.h"
#include "Double.h"
#include "PlatingStatus.h"
#include "QString.h"

namespace Ipc2581b
{

class Hole
{
public:
	virtual ~Hole() {}

    QString name;
    Double diameter;
    PlatingStatus platingStatus;
    Double plusTol;
    Double minusTol;
    Double x;
    Double y;
    QList<IdRef*> specRefList;

};

}