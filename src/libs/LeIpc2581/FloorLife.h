
#pragma once

namespace Ipc2581b
{

enum class FloorLife
{
    _1Year,
    _48Hours,
    _4Weeks,
    _24Hours,
    Unlimited,
    Bake,
    _72Hours,
    _168Hours,
};

}