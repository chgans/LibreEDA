
#pragma once

#include <QList>
#include "Optional.h"


#include "Bool.h"
#include "NameRef.h"
#include "QString.h"

namespace Ipc2581b
{

class BomHeader
{
public:
	virtual ~BomHeader() {}

    QString assembly;
    QString revision;
    Optional<Bool> affectingOptional;
    QList<NameRef*> stepRefList;

};

}