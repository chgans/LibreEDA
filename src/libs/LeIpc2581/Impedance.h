
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "TransmissionList.h"
#include "Property.h"
#include "StructureList.h"
#include "ImpedanceList.h"
#include "QString.h"

namespace Ipc2581b
{

class Impedance: public SpecificationType
{
public:
	virtual ~Impedance() {}

    ImpedanceList type;
    Optional<QString> commentOptional;
    TransmissionList transmission;
    StructureList structure;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Impedance;
    }
};

}