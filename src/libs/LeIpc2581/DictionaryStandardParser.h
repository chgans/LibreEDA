
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryStandard.h"

namespace Ipc2581b
{

class UnitsParser;
class EntryStandardParser;

class DictionaryStandardParser
{
public:
    DictionaryStandardParser(
            UnitsParser*&
            , EntryStandardParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryStandard *result();

private:
    UnitsParser *&m_unitsParser;
    EntryStandardParser *&m_entryStandardParser;
    QScopedPointer<DictionaryStandard> m_result;
};

}