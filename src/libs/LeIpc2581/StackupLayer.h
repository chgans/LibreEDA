
#pragma once

#include <QList>
#include "Optional.h"


#include "MatDes.h"
#include "Double.h"
#include "IdRef.h"
#include "QString.h"

namespace Ipc2581b
{

class StackupLayer
{
public:
	virtual ~StackupLayer() {}

    QString layerOrGroupRef;
    Double thickness;
    Double tolPlus;
    Double tolMinus;
    Optional<Double> sequenceOptional;
    Optional<QString> commentOptional;
    Optional<MatDes*> matDesOptional;
    QList<IdRef*> specRefList;

};

}