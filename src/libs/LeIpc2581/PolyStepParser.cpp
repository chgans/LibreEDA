#include "PolyStepParser.h"

#include "PolyStepSegmentParser.h"
#include "PolyStepCurveParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

PolyStepParser::PolyStepParser(
    PolyStepSegmentParser *&_polyStepSegmentParser
    , PolyStepCurveParser *&_polyStepCurveParser
):    m_polyStepSegmentParser(_polyStepSegmentParser)
    , m_polyStepCurveParser(_polyStepCurveParser)
{

}

bool PolyStepParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("PolyStepSegment"))
    {
        if(m_polyStepSegmentParser->parse(reader))
        {
            m_result = m_polyStepSegmentParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("PolyStepCurve"))
    {
        if(m_polyStepCurveParser->parse(reader))
        {
            m_result = m_polyStepCurveParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"PolyStep\"").arg(reader->name().toString()));
    return false;
}

PolyStep *PolyStepParser::result()
{
    return m_result;
}

bool PolyStepParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("PolyStepSegment"))
        return true;
    if (name == QStringLiteral("PolyStepCurve"))
        return true;
    return false;
}

}