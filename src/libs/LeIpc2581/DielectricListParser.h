
#pragma once

#include <QString>

class QXmlStreamReader;

#include "DielectricList.h"

namespace Ipc2581b
{

class DielectricListParser
{
public:
    DielectricListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    DielectricList result();

private:
    DielectricList m_result;
};

}