
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PolyStepSegment.h"

namespace Ipc2581b
{

class DoubleParser;

class PolyStepSegmentParser
{
public:
    PolyStepSegmentParser(
            DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PolyStepSegment *result();

private:
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    QScopedPointer<PolyStepSegment> m_result;
};

}