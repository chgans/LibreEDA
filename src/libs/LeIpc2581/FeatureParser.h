
#pragma once

#include <QString>
class QXmlStreamReader;

#include "Feature.h"

namespace Ipc2581b
{

class StandardShapeParser;
class UserShapeParser;

class FeatureParser
{
public:
    FeatureParser(
            StandardShapeParser*&
            , UserShapeParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    Feature *result();

private:
    StandardShapeParser *&m_standardShapeParser;
    UserShapeParser *&m_userShapeParser;
    Feature *m_result;
};

}