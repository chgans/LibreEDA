#include "MountParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

MountParser::MountParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool MountParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Mount> map =
    {
        {QStringLiteral("SMT"), Mount::Smt},
        {QStringLiteral("OTHER"), Mount::Other},
        {QStringLiteral("THMT"), Mount::Thmt},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Mount").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Mount MountParser::result()
{
    return m_result;
}

}