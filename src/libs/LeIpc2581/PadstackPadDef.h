
#pragma once

#include <QList>
#include "Optional.h"


#include "Location.h"
#include "Xform.h"
#include "Feature.h"
#include "PadUse.h"
#include "QString.h"

namespace Ipc2581b
{

class PadstackPadDef
{
public:
	virtual ~PadstackPadDef() {}

    QString layerRef;
    PadUse padUse;
    Optional<QString> commentOptional;
    Optional<Xform*> xformOptional;
    Location *location;
    Feature *feature;

};

}