#include "MarkingUsageParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

MarkingUsageParser::MarkingUsageParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool MarkingUsageParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, MarkingUsage> map =
    {
        {QStringLiteral("ATTRIBUTE_GRAPHICS"), MarkingUsage::AttributeGraphics},
        {QStringLiteral("PARTNAME"), MarkingUsage::Partname},
        {QStringLiteral("NONE"), MarkingUsage::None},
        {QStringLiteral("REFDES"), MarkingUsage::Refdes},
        {QStringLiteral("TARGET"), MarkingUsage::Target},
        {QStringLiteral("PIN_ONE"), MarkingUsage::PinOne},
        {QStringLiteral("POLARITY_MARKING"), MarkingUsage::PolarityMarking},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum MarkingUsage").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

MarkingUsage MarkingUsageParser::result()
{
    return m_result;
}

}