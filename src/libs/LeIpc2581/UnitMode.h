
#pragma once

namespace Ipc2581b
{

enum class UnitMode
{
    Capacitance,
    Size,
    Area,
    Distance,
    Resistance,
    None,
    Impedance,
    Percentage,
};

}