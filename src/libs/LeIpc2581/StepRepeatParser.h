
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "StepRepeat.h"

namespace Ipc2581b
{

class BoolParser;
class DoubleParser;
class IntParser;
class QStringParser;

class StepRepeatParser
{
public:
    StepRepeatParser(
            QStringParser*&
            , DoubleParser*&
            , DoubleParser*&
            , IntParser*&
            , IntParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            );

    bool parse(QXmlStreamReader *reader);
    StepRepeat *result();

private:
    QStringParser *&m_stepRefParser;
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    IntParser *&m_nxParser;
    IntParser *&m_nyParser;
    DoubleParser *&m_dxParser;
    DoubleParser *&m_dyParser;
    DoubleParser *&m_angleParser;
    BoolParser *&m_mirrorParser;
    QScopedPointer<StepRepeat> m_result;
};

}