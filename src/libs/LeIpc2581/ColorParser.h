
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Color.h"

namespace Ipc2581b
{

class IntParser;

class ColorParser
{
public:
    ColorParser(
            IntParser*&
            , IntParser*&
            , IntParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Color *result();

private:
    IntParser *&m_rParser;
    IntParser *&m_gParser;
    IntParser *&m_bParser;
    QScopedPointer<Color> m_result;
};

}