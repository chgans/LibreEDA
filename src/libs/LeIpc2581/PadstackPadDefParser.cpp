#include "PadstackPadDefParser.h"

#include "LocationParser.h"
#include "XformParser.h"
#include "FeatureParser.h"
#include "PadUseParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

PadstackPadDefParser::PadstackPadDefParser(
    QStringParser *&_layerRefParser
    , PadUseParser *&_padUseParser
    , QStringParser *&_commentParser
    , XformParser *&_xformParser
    , LocationParser *&_locationParser
    , FeatureParser *&_featureParser
):    m_layerRefParser(_layerRefParser)
    , m_padUseParser(_padUseParser)
    , m_commentParser(_commentParser)
    , m_xformParser(_xformParser)
    , m_locationParser(_locationParser)
    , m_featureParser(_featureParser)
{

}

bool PadstackPadDefParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new PadstackPadDef());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("layerRef")))
    {
        data = reader->attributes().value(QStringLiteral("layerRef"));
        if (!m_layerRefParser->parse(reader, data))
            return false;
        m_result->layerRef = m_layerRefParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("layerRef: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("padUse")))
    {
        data = reader->attributes().value(QStringLiteral("padUse"));
        if (!m_padUseParser->parse(reader, data))
            return false;
        m_result->padUse = m_padUseParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("padUse: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("comment")))
    {
        data = reader->attributes().value(QStringLiteral("comment"));
        if (!m_commentParser->parse(reader, data))
            return false;
        m_result->commentOptional = Optional<QString>(m_commentParser->result());
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("Location"))
        {
            if (!m_locationParser->parse(reader))
                return false;
            auto result = m_locationParser->result();
            m_result->location = result;
        }
        else if (m_featureParser->isSubstitution(name))
        {
            if (!m_featureParser->parse(reader))
                return false;
            auto result = m_featureParser->result();
            m_result->feature = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

PadstackPadDef *PadstackPadDefParser::result()
{
    return m_result.take();
}

}