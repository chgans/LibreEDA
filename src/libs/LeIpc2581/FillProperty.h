
#pragma once

namespace Ipc2581b
{

enum class FillProperty
{
    Fill,
    Hollow,
    Hatch,
    Void,
    Mesh,
};

}