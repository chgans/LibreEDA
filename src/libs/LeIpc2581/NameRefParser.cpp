#include "NameRefParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

NameRefParser::NameRefParser(
    QStringParser *&_nameParser
):    m_nameParser(_nameParser)
{

}

bool NameRefParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new NameRef());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

NameRef *NameRefParser::result()
{
    return m_result.take();
}

}