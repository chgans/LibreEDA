
#pragma once

#include <QString>

class QXmlStreamReader;

#include "Exposure.h"

namespace Ipc2581b
{

class ExposureParser
{
public:
    ExposureParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    Exposure result();

private:
    Exposure m_result;
};

}