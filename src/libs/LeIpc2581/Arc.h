
#pragma once

#include <QList>
#include "Optional.h"

#include "Simple.h"

#include "LineDescGroup.h"
#include "Bool.h"
#include "Double.h"

namespace Ipc2581b
{

class Arc: public Simple
{
public:
	virtual ~Arc() {}

    Double startX;
    Double startY;
    Double endX;
    Double endY;
    Double centerX;
    Double centerY;
    Bool clockwise;
    LineDescGroup *lineDescGroup;

    virtual SimpleType simpleType() const override
    {
        return SimpleType::Arc;
    }
};

}