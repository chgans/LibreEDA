
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "ColorRef.h"

namespace Ipc2581b
{

class QStringParser;

class ColorRefParser
{
public:
    ColorRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    ColorRef *result();

private:
    QStringParser *&m_idParser;
    QScopedPointer<ColorRef> m_result;
};

}