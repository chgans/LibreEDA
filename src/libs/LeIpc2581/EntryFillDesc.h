
#pragma once

#include <QList>
#include "Optional.h"


#include "FillDesc.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryFillDesc
{
public:
	virtual ~EntryFillDesc() {}

    QString id;
    FillDesc *fillDesc;

};

}