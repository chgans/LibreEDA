
#pragma once

#include <QString>

class QXmlStreamReader;

#include "MarkingUsage.h"

namespace Ipc2581b
{

class MarkingUsageParser
{
public:
    MarkingUsageParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    MarkingUsage result();

private:
    MarkingUsage m_result;
};

}