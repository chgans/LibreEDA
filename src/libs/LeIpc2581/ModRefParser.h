
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ModRef.h"

namespace Ipc2581b
{

class ModRefParser
{
public:
    ModRefParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ModRef result();

private:
    ModRef m_result;
};

}