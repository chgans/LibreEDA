
#pragma once

#include <QList>
#include "Optional.h"

#include "Fiducial.h"

#include "Location.h"
#include "StandardShape.h"
#include "Xform.h"

namespace Ipc2581b
{

class GoodPanelMark: public Fiducial
{
public:
	virtual ~GoodPanelMark() {}

    Optional<Xform*> xformOptional;
    Location *location;
    StandardShape *standardShape;

    virtual FiducialType fiducialType() const override
    {
        return FiducialType::GoodPanelMark;
    }
};

}