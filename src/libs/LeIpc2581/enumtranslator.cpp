#include "enumtranslator.h"

namespace Ipc2581b
{

    class EnumTranslatorPrivate
    {
        friend class EnumTranslator;
        static const char *invalidEnumName;
        static const QMap<int, const char *> LayerFunctionMap;
        static const QMap<int, const char *> SideMap;
        static const QMap<int, const char *> PolarityMap;
        static const QMap<int, const char *> PackageTypeMap;
        static const QMap<int, const char *> CadPinMap;
        static const QMap<int, const char *> PinElectricalMap;
        static const QMap<int, const char *> PinMountMap;
        static const QMap<int, const char *> PinOneOrientationMap;
        static const QMap<int, const char *> PadUsageMap;
        static const QMap<int, const char *> PadUseMap;
        static const QMap<int, const char *> PlatingStatusMap;
        static const QMap<int, const char *> UnitsMap;
        static const QMap<int, const char *> MountMap;
        static const QMap<int, const char *> NetClassMap;
        static const QMap<int, const char *> BackdrillListMap;
        static const QMap<int, const char *> ConductorListMap;
        static const QMap<int, const char *> DielectricListMap;
        static const QMap<int, const char *> GeneralListMap;
        static const QMap<int, const char *> PropertyUnitMap;
        static const QMap<int, const char *> ImpedanceListMap;
        static const QMap<int, const char *> TransmissionListMap;
        static const QMap<int, const char *> StructureListMap;
        static const QMap<int, const char *> ComplianceListMap;
        static const QMap<int, const char *> TechnologyListMap;
        static const QMap<int, const char *> TemperatureListMap;
        static const QMap<int, const char *> ToolListMap;
        static const QMap<int, const char *> ToolPropertyListMap;
        static const QMap<int, const char *> VCutListMap;
    };

    const char *EnumTranslatorPrivate::invalidEnumName = "?";

    const QMap<int, const char *> EnumTranslatorPrivate::LayerFunctionMap = {
        {int(LayerFunction::Conductor), "Conductor"},
        {int(LayerFunction::Plane), "Conductor plane"},
        {int(LayerFunction::Condfoil), "Conductor foil"},
        {int(LayerFunction::Condfilm), "Conductor film"},
        {int(LayerFunction::ConductiveAdhesive), "Conductive adhesive"},
        {int(LayerFunction::Dielbase), "Dielectric base"},
        {int(LayerFunction::Dielcore), "Dielectric core"},
        {int(LayerFunction::Dielpreg), "Dielectric PREG"},
        {int(LayerFunction::Dieladhv), "Dielectric adhesive"},
        {int(LayerFunction::Coatingcond), "Coating conductive"},
        {int(LayerFunction::Coatingnoncond), "Coating non conductive"},
        {int(LayerFunction::Silkscreen), "Silkscreen"},
        {int(LayerFunction::Solderpaste), "Solder paste"},
        {int(LayerFunction::Soldermask), "Solder mask"},
        {int(LayerFunction::Solderbump), "Solder bump"},
        {int(LayerFunction::Pastemask), "Paste mask"},
        {int(LayerFunction::Holefill), "Hole Fill"},
        {int(LayerFunction::Drill), "Drill"},
        {int(LayerFunction::Rout), "Rout"},
        {int(LayerFunction::VCut), "V Cut"},
        {int(LayerFunction::Glue), "Glue"},
        {int(LayerFunction::Signal), "Signal"},
        {int(LayerFunction::Resistive), "Resistive"},
        {int(LayerFunction::Capacitive), "Capacitive"},
        {int(LayerFunction::StackupComposite), "Stackup"},
        {int(LayerFunction::Landpattern), "Land pattern"},
        {int(LayerFunction::Pin), "Pin"},
        {int(LayerFunction::ComponentTop), "Component top"},
        {int(LayerFunction::ComponentBottom), "Component bottom"},
        {int(LayerFunction::EmbeddedComponent), "Embedded component"},
        {int(LayerFunction::Component), "Component"},
        {int(LayerFunction::BoardOutline), "Board outline"},
        {int(LayerFunction::Courtyard), "Court yard"},
        {int(LayerFunction::Assembly), "Assembly"},
        {int(LayerFunction::Boardfab), "Board fabrication"},
        {int(LayerFunction::Legend), "Legend"},
        {int(LayerFunction::Rework), "Rework"},
        {int(LayerFunction::Probe), "Probe"},
        {int(LayerFunction::Graphic), "Graphic"},
        {int(LayerFunction::Document), "Document"},
        {int(LayerFunction::Fixture), "Fixture"},
        {int(LayerFunction::Other), "Other"},
        {int(LayerFunction::Mixed), "Mixed"},
    };

#define ENUM_NAME(enumMap, enumValue) \
    EnumTranslatorPrivate::enumMap ## Map \
    .value(static_cast<int>(enumValue), \
    EnumTranslatorPrivate::invalidEnumName);

    QString EnumTranslator::layerFunctionText(LayerFunction func)
    {
        return ENUM_NAME(LayerFunction, func);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::SideMap = {
        {int(Side::All), "All"},
        {int(Side::Both), "Both"},
        {int(Side::Bottom), "Bottom"},
        {int(Side::Internal), "Internal"},
        {int(Side::None), "None"},
        {int(Side::Top), "Top"},
    };

    QString EnumTranslator::sideText(Side side)
    {
        return ENUM_NAME(Side, side);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PolarityMap = {
        {int(Polarity::Negative), "Negative"},
        {int(Polarity::Positive), "Positive"},
    };
    QString EnumTranslator::polarityText(Polarity polarity)
    {
        return ENUM_NAME(Polarity, polarity);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PackageTypeMap = {
        {int(PackageType::Tantalum), "Tantalum"},
        {int(PackageType::ChipScale), "Chip Scale"},
        {int(PackageType::SquareQuadFlatpack), "Square Quad Flatpack"},
        {int(PackageType::PowerTransistor), "Power Transistor"},
        {int(PackageType::Pga), "PGA"},
        {int(PackageType::Molded), "Molded"},
        {int(PackageType::Sot143), "SOT-143"},
        {int(PackageType::Sot89), "SOT-89"},
        {int(PackageType::ConnectorTh), "Connector TH"},
        {int(PackageType::Transformer), "Transformer"},
        {int(PackageType::Network), "Network"},
        {int(PackageType::AxialLeaded), "Axial Leaded"},
        {int(PackageType::Chip), "Chip"},
        {int(PackageType::Sot52), "SOT-52"},
        {int(PackageType::Embedded), "Embedded"},
        {int(PackageType::RelaySm), "Relay SM"},
        {int(PackageType::CeramicQuadFlatpack), "Ceramic Quad Flatpack"},
        {int(PackageType::BareDie), "Bare Die"},
        {int(PackageType::Other), "Other"},
        {int(PackageType::Mcm), "Mcm"},
        {int(PackageType::Soic), "SOIC"},
        {int(PackageType::RelayTh), "Relay TH"},
        {int(PackageType::PlasticBga), "Plastic Bga"},
        {int(PackageType::HermeticHybred), "Hermetic Hybred"},
        {int(PackageType::LeadlessCeramicChipCarrier), "Leadless Ceramic Chip Carrier"},
        {int(PackageType::Sod123), "SOD123"},
        {int(PackageType::CeramicDip), "Ceramic DIP"},
        {int(PackageType::Sot23), "SOT23"},
        {int(PackageType::FinepitchBga), "Fine-pitch BGA"},
        {int(PackageType::PlasticDip), "Plastic DIP"},
        {int(PackageType::Coil), "Coil"},
        {int(PackageType::RadialLeaded), "Radial Leaded"},
        {int(PackageType::TrimpotTh), "Trimpot TH"},
        {int(PackageType::PlasticSip), "Plastic SIP"},
        {int(PackageType::Sopic), "SOPIC"},
        {int(PackageType::SwitchTh), "Switch TH"},
        {int(PackageType::CeramicSip), "Ceramic SIP"},
        {int(PackageType::CeramicFlatpack), "Ceramic Flatpack"},
        {int(PackageType::ToType), "TO-Type"},
        {int(PackageType::CeramicBga), "Ceramic BGA"},
        {int(PackageType::Melf), "MELF"},
        {int(PackageType::PlasticChipCarrier), "Plastic Chip Carrier"},
        {int(PackageType::ConnectorSm), "Connector SM"},
        {int(PackageType::Soj), "SOJ"},
        {int(PackageType::ChokeSwitchSm), "Choke Switch SM"},
        {int(PackageType::RectangularQuadFlatpack), "Rectangular Quad Flatpack"},
        {int(PackageType::Flipchip), "Flipchip"},
        {int(PackageType::Ssoic), "SSOIC"},
        {int(PackageType::TrimpotSm), "Trimpot SM"}
    };
    QString EnumTranslator::packageTypeText(PackageType type)
    {
        return ENUM_NAME(PackageType, type);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::CadPinMap = {
        {int(CadPin::Thru), "Through"},
        {int(CadPin::Blind), "Blind"},
        {int(CadPin::Surface), "Surface"},
    };
    QString EnumTranslator::cadPinTypeText(CadPin type)
    {
        return ENUM_NAME(CadPin, type);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PinElectricalMap = {
        {int(PinElectrical::Electrical), "Electrical"},
        {int(PinElectrical::Mechanical), "Mechanical"},
        {int(PinElectrical::Undefined), "Undefined"},
    };
    QString EnumTranslator::electricalPinTypeText(PinElectrical type)
    {
        return ENUM_NAME(PinElectrical, type);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PinMountMap = {
        {int(PinMount::Hole), "Hole"},
        {int(PinMount::Nonboard), "Non board"},
        {int(PinMount::Pressfit), "Press-fit"},
        {int(PinMount::SurfaceMountPad), "SM Pad"},
        {int(PinMount::SurfaceMountPin), "SM Pin"},
        {int(PinMount::ThroughHoleHole), "TH Hole"},
        {int(PinMount::ThroughHolePin), "TH Pin"},
        {int(PinMount::Undefined), "Undefined"},
    };
    QString EnumTranslator::mountingPinTypeText(PinMount type)
    {
        return ENUM_NAME(PinMount, type);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PinOneOrientationMap = {
        {int(PinOneOrientation::Left), "Left"},
        {int(PinOneOrientation::LeftCenter), "Left center"},
        {int(PinOneOrientation::LowerLeft), "Lower left"},
        {int(PinOneOrientation::Other), "Other"},
        {int(PinOneOrientation::UpperCenter), "Upper center"},
        {int(PinOneOrientation::UpperLeft), "Upper left"},
    };
    QString EnumTranslator::pinOneOrientationText(PinOneOrientation type)
    {
        return ENUM_NAME(PinOneOrientation, type);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PadUsageMap = {
        {int(PadUsage::Mask), "Mask"},
        {int(PadUsage::None), "None"},
        {int(PadUsage::Plane), "Plane"},
        {int(PadUsage::Termination), "Termination"},
        {int(PadUsage::ToolingHole), "Tooling hole"},
        {int(PadUsage::Via), "Via"},
    };
    QString EnumTranslator::padUsageText(PadUsage usage)
    {
        return ENUM_NAME(PadUsage, usage);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PadUseMap = {
        {int(PadUse::Regular), "Regular"},
        {int(PadUse::Antipad), "Anti-pad"},
        {int(PadUse::Other), "Other"},
        {int(PadUse::Thermal), "Thermal"},
    };
    QString EnumTranslator::padUseText(PadUse use)
    {
        return ENUM_NAME(PadUse, use);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PlatingStatusMap = {
        {int(PlatingStatus::Nonplated), "Non plated"},
        {int(PlatingStatus::Plated), "Plated"},
        {int(PlatingStatus::Via), "Via"},
    };
    QString EnumTranslator::platingStatusText(PlatingStatus status)
    {
        return ENUM_NAME(PlatingStatus, status);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::UnitsMap = {
        {int(Units::Inch), "Inch"},
        {int(Units::Micron), "Micro-metre"},
        {int(Units::Millimeter), "Milli-metre"},
    };
    QString EnumTranslator::unitsText(Units units)
    {
        return ENUM_NAME(Units, units);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::MountMap = {
        {int(Mount::Smt), "Surface"},
        {int(Mount::Thmt), "Through"},
        {int(Mount::Other), "Other"},
    };
    QString EnumTranslator::mountText(Mount mount)
    {
        return ENUM_NAME(Mount, mount);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::NetClassMap = {
        {int(NetClass::Clk), "Clock"},
        {int(NetClass::Fixed), "Fixed"},
        {int(NetClass::Ground), "Ground"},
        {int(NetClass::Power), "Power"},
        {int(NetClass::Signal), "Signal"},
        {int(NetClass::Unused), "Unused"},
    };
    QString EnumTranslator::netClassText(NetClass netClass)
    {
        return ENUM_NAME(NetClass, netClass);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::PropertyUnitMap = {
        {int(PropertyUnit::Item), "Item"},
        {int(PropertyUnit::Mm), "mm"},
        {int(PropertyUnit::Rz), "Rz"},
        {int(PropertyUnit::Other), "Other"},
        {int(PropertyUnit::Inch), "″"},
        {int(PropertyUnit::Class), "Class"},
        {int(PropertyUnit::Degrees), "°"},
        {int(PropertyUnit::Section), "Section"},
        {int(PropertyUnit::Faranheit), "°F"},
        {int(PropertyUnit::Percent), "%"},
        {int(PropertyUnit::SiemensPerM), "S/m"},
        {int(PropertyUnit::MhoPerCm), "MΩ/cm"},
        {int(PropertyUnit::Rms), "RMS"},
        {int(PropertyUnit::Ohms), "Ω"},
        {int(PropertyUnit::Ohm), "Ω"},
        {int(PropertyUnit::Gauge), "Gauge"},
        {int(PropertyUnit::Rmax), "Rmax"},
        {int(PropertyUnit::Micron), "µm"},
        {int(PropertyUnit::Hz), "Hz"},
        {int(PropertyUnit::Celcius), "°C"},
    };
    QString EnumTranslator::propertyUnitText(PropertyUnit unit)
    {
        return ENUM_NAME(PropertyUnit, unit);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::BackdrillListMap = {
        {int(BackdrillList::MaxStubLength), "Max. stub length"},
        {int(BackdrillList::MustNotCutLayer), "Must not cut layer"},
        {int(BackdrillList::Other), "Other"},
        {int(BackdrillList::StartLayer), "Start layer"},
    };
    QString EnumTranslator::backdrillListText(BackdrillList backdrill)
    {
        return ENUM_NAME(BackdrillList, backdrill);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::ConductorListMap = {
        {int(ConductorList::Conductivity), "Conductivity"},
        {int(ConductorList::EtchFactor), "Etch factor"},
        {int(ConductorList::FinishedHeight), "Finished height"},
        {int(ConductorList::Other), "Other"},
        {int(ConductorList::SurfaceRoughnessDownfacing), "Surface roughness, up"},
        {int(ConductorList::SurfaceRoughnessTreated), "Surface roughness, treated"},
        {int(ConductorList::SurfaceRoughnessUpfacing), "Surface roughness, down"},
    };
    QString EnumTranslator::conductorListText(ConductorList conductor)
    {
        return ENUM_NAME(ConductorList, conductor);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::DielectricListMap = {
        {int(DielectricList::DielectricConstant), "Dielectric constant"},
        {int(DielectricList::GlassStyle), "Glass style"},
        {int(DielectricList::GlassType), "Glass type"},
        {int(DielectricList::LossTangent), "Loss tangent"},
        {int(DielectricList::Other), "Other"},
        {int(DielectricList::ProcessabilityTemp), "Processability temperature"},
        {int(DielectricList::ResinContent), "Resin content"},
    };
    QString EnumTranslator::dielectricListText(DielectricList dielectric)
    {
        return ENUM_NAME(DielectricList, dielectric);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::GeneralListMap = {
        {int(GeneralList::Electrical), "Electrical"},
        {int(GeneralList::Instruction), "Instruction"},
        {int(GeneralList::Material), "Material"},
        {int(GeneralList::Other), "Other"},
        {int(GeneralList::Standard), "Standard"},
        {int(GeneralList::Thermal), "Thermal"},
    };
    QString EnumTranslator::generalListText(GeneralList general)
    {
        return ENUM_NAME(GeneralList, general);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::ImpedanceListMap = {
        {int(ImpedanceList::Impedance), "Impedance"},
        {int(ImpedanceList::Linewidth), "Line width"},
        {int(ImpedanceList::Other), "Other"},
        {int(ImpedanceList::RefPlaneLayerId), "Ref. plane"},
        {int(ImpedanceList::Spacing), "Spacing"},
    };
    QString EnumTranslator::impedanceListText(ImpedanceList impedance)
    {
        return ENUM_NAME(ImpedanceList, impedance);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::StructureListMap = {
        {int(StructureList::CoplanarWaveguideDualMaskedCovered), "Coplanar waveguide, dual, covered"},
        {int(StructureList::CoplanarWaveguideEmbedded), "Coplanar waveguide, embedded"},
        {int(StructureList::CoplanarWaveguideMaskCovered), "Coplanar waveguide, covered"},
        {int(StructureList::CoplanarWaveguideNoMask), "Coplanar waveguide"},
        {int(StructureList::CoplanarWaveguideStripline), "Coplanar waveguide, strip"},
        {int(StructureList::MicrostripDualMaskedCovered), "Microstrip, dual, covered"},
        {int(StructureList::MicrostripMaskCovered), "Microstrip, covered"},
        {int(StructureList::MicrostripNoMask), "Microstrip"},
        {int(StructureList::Other), "Other"},
        {int(StructureList::PlaneLessStripline), "Planeless strip"},
        {int(StructureList::Stripline), "Strip"},
    };
    QString EnumTranslator::structureListText(StructureList structure)
    {
        return ENUM_NAME(StructureList, structure);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::TransmissionListMap = {
        {int(TransmissionList::BroadsideCoupled), "Brodside coupled"},
        {int(TransmissionList::EdgeCoupled), "Edge coupled"},
        {int(TransmissionList::Other), "Other"},
        {int(TransmissionList::SingleEnded), "Single"},
    };
    QString EnumTranslator::transmissionListText(TransmissionList transmission)
    {
        return ENUM_NAME(TransmissionList, transmission);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::ComplianceListMap = {
        {int(ComplianceList::ConflictMinerals), "Conflict material"},
        {int(ComplianceList::HalogenFree), "Halogen free"},
        {int(ComplianceList::Other), "Other"},
        {int(ComplianceList::Reach), "REACH"},
        {int(ComplianceList::Rohs), "ROHS"},
        {int(ComplianceList::Weee), "WEEE"},
    };
    QString EnumTranslator::complianceListText(ComplianceList compliance)
    {
        return ENUM_NAME(ComplianceList, compliance);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::TechnologyListMap = {
        {int(TechnologyList::EmbeddedComponent), "Embedded comp."},
        {int(TechnologyList::Flex), "Flex."},
        {int(TechnologyList::Hdi), "HDI"},
        {int(TechnologyList::Other), "Other"},
        {int(TechnologyList::Rigid), "Rigid"},
        {int(TechnologyList::RigidFlex), "Rigid/Flex."},
    };
    QString EnumTranslator::technologyListText(TechnologyList technology)
    {
        return ENUM_NAME(TechnologyList, technology);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::TemperatureListMap = {
        {int(TemperatureList::ExpansionXYAxis), "Expansion X-Y"},
        {int(TemperatureList::ExpansionZAxis), "Expansion Z"},
        {int(TemperatureList::Other), "Other"},
        {int(TemperatureList::ThermalDelamination), "Thermal delamination"},
    };
    QString EnumTranslator::temperatureListText(TemperatureList temperature)
    {
        return ENUM_NAME(TemperatureList, temperature);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::ToolListMap = {
        {int(ToolList::Carbide), "Carbide"},
        {int(ToolList::Extension), "Extension"},
        {int(ToolList::Flatnose), "Flat nose"},
        {int(ToolList::Laser), "Laser"},
        {int(ToolList::Router), "Router"},
        {int(ToolList::VCutter), "V cutter"},
    };
    QString EnumTranslator::toolListText(ToolList tool)
    {
        return ENUM_NAME(ToolList, tool);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::ToolPropertyListMap = {
        {int(ToolPropertyList::BitAngle), "Bit angle"},
        {int(ToolPropertyList::DrillSize), "Drill size"},
        {int(ToolPropertyList::FinishedSize), "Finished size"},
        {int(ToolPropertyList::Other), "Other"},
    };
    QString EnumTranslator::toolPropertyListText(ToolPropertyList property)
    {
        return ENUM_NAME(ToolPropertyList, property);
    }

    const QMap<int, const char *> EnumTranslatorPrivate::VCutListMap = {
        {int(VCutList::Angle), "Angle"},
        {int(VCutList::Offset), "Offset"},
        {int(VCutList::Other), "Other"},
        {int(VCutList::ThicknessRemaining), "Thickness remaining"},
    };
    QString EnumTranslator::vCutListText(VCutList vCut)
    {
        return ENUM_NAME(VCutList, vCut);
    }

    QMap<int, const char*> EnumTranslator::enumMapping(int typeId)
    {
        if (typeId == qMetaTypeId<LayerFunction>())
            return EnumTranslatorPrivate::LayerFunctionMap;
        if (typeId == qMetaTypeId<Side>())
            return EnumTranslatorPrivate::SideMap;
        if (typeId == qMetaTypeId<Polarity>())
            return EnumTranslatorPrivate::PolarityMap;
        if (typeId == qMetaTypeId<PackageType>())
            return EnumTranslatorPrivate::PackageTypeMap;
        if (typeId == qMetaTypeId<CadPin>())
            return EnumTranslatorPrivate::CadPinMap;
        if (typeId == qMetaTypeId<PinElectrical>())
            return EnumTranslatorPrivate::PinElectricalMap;
        if (typeId == qMetaTypeId<PinMount>())
            return EnumTranslatorPrivate::PinMountMap;
        if (typeId == qMetaTypeId<PinOneOrientation>())
            return EnumTranslatorPrivate::PinOneOrientationMap;
        if (typeId == qMetaTypeId<PadUsage>())
            return EnumTranslatorPrivate::PadUsageMap;
        if (typeId == qMetaTypeId<PadUse>())
            return EnumTranslatorPrivate::PadUseMap;
        if (typeId == qMetaTypeId<PlatingStatus>())
            return EnumTranslatorPrivate::PlatingStatusMap;
        if (typeId == qMetaTypeId<Units>())
            return EnumTranslatorPrivate::UnitsMap;
        if (typeId == qMetaTypeId<Mount>())
            return EnumTranslatorPrivate::MountMap;
        if (typeId == qMetaTypeId<NetClass>())
            return EnumTranslatorPrivate::NetClassMap;
        return QMap<int, const char*>();
    }

    QList<QString> EnumTranslator::enumNames(int typeId)
    {
        QList<QString> result;
        for (int key: enumMapping(typeId).keys())
            result << enumMapping(typeId).value(key);
        return result;
    }

    QString EnumTranslator::enumName(int typeId, int enumIndex)
    {
        const QList<const char*> names = enumMapping(typeId).values();
        if (enumIndex < 0 || enumIndex >= names.count())
            return EnumTranslatorPrivate::invalidEnumName;
        return names.at(enumIndex);
    }

    int EnumTranslator::enumValue(int typeId, int enumIndex)
    {
        const QList<int> values = enumMapping(typeId).keys();
        if (enumIndex < 0 || enumIndex >= values.count())
            return -1;
        return values.at(enumIndex);
    }

    int EnumTranslator::enumIndex(int typeId, int enumValue)
    {
        for (int i = 0; i < enumMapping(typeId).count(); i++)
            if (enumMapping(typeId).keys().at(i) == enumValue)
                return i;
        return -1;
    }

}
