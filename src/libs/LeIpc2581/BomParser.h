
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Bom.h"

namespace Ipc2581b
{

class BomItemParser;
class BomHeaderParser;
class QStringParser;

class BomParser
{
public:
    BomParser(
            QStringParser*&
            , BomHeaderParser*&
            , BomItemParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Bom *result();

private:
    QStringParser *&m_nameParser;
    BomHeaderParser *&m_bomHeaderParser;
    BomItemParser *&m_bomItemParser;
    QScopedPointer<Bom> m_result;
};

}