#include "ImpedanceListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ImpedanceListParser::ImpedanceListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ImpedanceListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, ImpedanceList> map =
    {
        {QStringLiteral("OTHER"), ImpedanceList::Other},
        {QStringLiteral("REF_PLANE_LAYER_ID"), ImpedanceList::RefPlaneLayerId},
        {QStringLiteral("SPACING"), ImpedanceList::Spacing},
        {QStringLiteral("IMPEDANCE"), ImpedanceList::Impedance},
        {QStringLiteral("LINEWIDTH"), ImpedanceList::Linewidth},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum ImpedanceList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

ImpedanceList ImpedanceListParser::result()
{
    return m_result;
}

}