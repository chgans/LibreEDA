
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "CADDataLayerRef.h"

namespace Ipc2581b
{

class QStringParser;

class CADDataLayerRefParser
{
public:
    CADDataLayerRefParser(
            QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    CADDataLayerRef *result();

private:
    QStringParser *&m_layerIdParser;
    QScopedPointer<CADDataLayerRef> m_result;
};

}