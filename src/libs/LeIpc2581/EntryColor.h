
#pragma once

#include <QList>
#include "Optional.h"


#include "Color.h"
#include "QString.h"

namespace Ipc2581b
{

class EntryColor
{
public:
	virtual ~EntryColor() {}

    QString id;
    Color *color;

};

}