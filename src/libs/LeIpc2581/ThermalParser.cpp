#include "ThermalParser.h"

#include "XformParser.h"
#include "LineDescGroupParser.h"
#include "DoubleParser.h"
#include "ThermalShapeParser.h"
#include "FillDescGroupParser.h"
#include "IntParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ThermalParser::ThermalParser(
    ThermalShapeParser *&_shapeParser
    , DoubleParser *&_outerDiameterParser
    , DoubleParser *&_innerDiameterParser
    , IntParser *&_spokeCountParser
    , DoubleParser *&_gapParser
    , DoubleParser *&_spokeStartAngleParser
    , XformParser *&_xformParser
    , LineDescGroupParser *&_lineDescGroupParser
    , FillDescGroupParser *&_fillDescGroupParser
):    m_shapeParser(_shapeParser)
    , m_outerDiameterParser(_outerDiameterParser)
    , m_innerDiameterParser(_innerDiameterParser)
    , m_spokeCountParser(_spokeCountParser)
    , m_gapParser(_gapParser)
    , m_spokeStartAngleParser(_spokeStartAngleParser)
    , m_xformParser(_xformParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_fillDescGroupParser(_fillDescGroupParser)
{

}

bool ThermalParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Thermal());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("shape")))
    {
        data = reader->attributes().value(QStringLiteral("shape"));
        if (!m_shapeParser->parse(reader, data))
            return false;
        m_result->shape = m_shapeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("shape: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("outerDiameter")))
    {
        data = reader->attributes().value(QStringLiteral("outerDiameter"));
        if (!m_outerDiameterParser->parse(reader, data))
            return false;
        m_result->outerDiameter = m_outerDiameterParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("outerDiameter: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("innerDiameter")))
    {
        data = reader->attributes().value(QStringLiteral("innerDiameter"));
        if (!m_innerDiameterParser->parse(reader, data))
            return false;
        m_result->innerDiameter = m_innerDiameterParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("innerDiameter: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("spokeCount")))
    {
        data = reader->attributes().value(QStringLiteral("spokeCount"));
        if (!m_spokeCountParser->parse(reader, data))
            return false;
        m_result->spokeCountOptional = Optional<Int>(m_spokeCountParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("gap")))
    {
        data = reader->attributes().value(QStringLiteral("gap"));
        if (!m_gapParser->parse(reader, data))
            return false;
        m_result->gapOptional = Optional<Double>(m_gapParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("spokeStartAngle")))
    {
        data = reader->attributes().value(QStringLiteral("spokeStartAngle"));
        if (!m_spokeStartAngleParser->parse(reader, data))
            return false;
        m_result->spokeStartAngle = m_spokeStartAngleParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("spokeStartAngle: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroupOptional = Optional<LineDescGroup*>(result);
        }
        else if (m_fillDescGroupParser->isSubstitution(name))
        {
            if (!m_fillDescGroupParser->parse(reader))
                return false;
            auto result = m_fillDescGroupParser->result();
            m_result->fillDescGroupOptional = Optional<FillDescGroup*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Thermal *ThermalParser::result()
{
    return m_result.take();
}

}