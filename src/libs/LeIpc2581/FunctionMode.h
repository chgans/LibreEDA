
#pragma once

#include <QList>
#include "Optional.h"


#include "QString.h"
#include "Int.h"
#include "Mode.h"

namespace Ipc2581b
{

class FunctionMode
{
public:
	virtual ~FunctionMode() {}

    Mode mode;
    Int level;
    Optional<QString> commentOptional;

};

}