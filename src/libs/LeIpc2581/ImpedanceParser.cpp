#include "ImpedanceParser.h"

#include "TransmissionListParser.h"
#include "PropertyParser.h"
#include "StructureListParser.h"
#include "ImpedanceListParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

ImpedanceParser::ImpedanceParser(
    ImpedanceListParser *&_typeParser
    , QStringParser *&_commentParser
    , TransmissionListParser *&_transmissionParser
    , StructureListParser *&_structureParser
    , PropertyParser *&_propertyParser
):    m_typeParser(_typeParser)
    , m_commentParser(_commentParser)
    , m_transmissionParser(_transmissionParser)
    , m_structureParser(_structureParser)
    , m_propertyParser(_propertyParser)
{

}

bool ImpedanceParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Impedance());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("type")))
    {
        data = reader->attributes().value(QStringLiteral("type"));
        if (!m_typeParser->parse(reader, data))
            return false;
        m_result->type = m_typeParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("type: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("comment")))
    {
        data = reader->attributes().value(QStringLiteral("comment"));
        if (!m_commentParser->parse(reader, data))
            return false;
        m_result->commentOptional = Optional<QString>(m_commentParser->result());
    }
    if (reader->attributes().hasAttribute(QStringLiteral("transmission")))
    {
        data = reader->attributes().value(QStringLiteral("transmission"));
        if (!m_transmissionParser->parse(reader, data))
            return false;
        m_result->transmission = m_transmissionParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("transmission: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("structure")))
    {
        data = reader->attributes().value(QStringLiteral("structure"));
        if (!m_structureParser->parse(reader, data))
            return false;
        m_result->structure = m_structureParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("structure: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Property"))
        {
            if (!m_propertyParser->parse(reader))
                return false;
            auto result = m_propertyParser->result();
            m_result->propertyList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Impedance *ImpedanceParser::result()
{
    return m_result.take();
}

}