
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Donut.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class FillDescGroupParser;
class DoubleParser;
class XformParser;
class DonutShapeParser;

class DonutParser
{
public:
    DonutParser(
            DonutShapeParser*&
            , DoubleParser*&
            , DoubleParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Donut *result();

private:
    DonutShapeParser *&m_shapeParser;
    DoubleParser *&m_outerDiameterParser;
    DoubleParser *&m_innerDiameterParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<Donut> m_result;
};

}