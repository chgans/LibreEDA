#include "FontDefEmbeddedParser.h"

#include "LineDescGroupParser.h"
#include "GlyphParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

FontDefEmbeddedParser::FontDefEmbeddedParser(
    QStringParser *&_nameParser
    , LineDescGroupParser *&_lineDescGroupParser
    , GlyphParser *&_glyphParser
):    m_nameParser(_nameParser)
    , m_lineDescGroupParser(_lineDescGroupParser)
    , m_glyphParser(_glyphParser)
{

}

bool FontDefEmbeddedParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new FontDefEmbedded());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("name")))
    {
        data = reader->attributes().value(QStringLiteral("name"));
        if (!m_nameParser->parse(reader, data))
            return false;
        m_result->name = m_nameParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("name: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (m_lineDescGroupParser->isSubstitution(name))
        {
            if (!m_lineDescGroupParser->parse(reader))
                return false;
            auto result = m_lineDescGroupParser->result();
            m_result->lineDescGroup = result;
        }
        else if (name == QStringLiteral("Glyph"))
        {
            if (!m_glyphParser->parse(reader))
                return false;
            auto result = m_glyphParser->result();
            m_result->glyphList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

FontDefEmbedded *FontDefEmbeddedParser::result()
{
    return m_result.take();
}

}