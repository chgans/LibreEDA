
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Contour.h"

namespace Ipc2581b
{

class PolygonParser;

class ContourParser
{
public:
    ContourParser(
            PolygonParser*&
            , PolygonParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Contour *result();

private:
    PolygonParser *&m_polygonParser;
    PolygonParser *&m_cutoutParser;
    QScopedPointer<Contour> m_result;
};

}