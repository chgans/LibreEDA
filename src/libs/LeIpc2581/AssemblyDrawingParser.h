
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "AssemblyDrawing.h"

namespace Ipc2581b
{

class OutlineParser;
class MarkingParser;

class AssemblyDrawingParser
{
public:
    AssemblyDrawingParser(
            OutlineParser*&
            , MarkingParser*&
            );

    bool parse(QXmlStreamReader *reader);
    AssemblyDrawing *result();

private:
    OutlineParser *&m_outlineParser;
    MarkingParser *&m_markingParser;
    QScopedPointer<AssemblyDrawing> m_result;
};

}