#include "BomCategoryParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

BomCategoryParser::BomCategoryParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool BomCategoryParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, BomCategory> map =
    {
        {QStringLiteral("PROGRAMMABLE"), BomCategory::Programmable},
        {QStringLiteral("MECHANICAL"), BomCategory::Mechanical},
        {QStringLiteral("ELECTRICAL"), BomCategory::Electrical},
        {QStringLiteral("DOCUMENT"), BomCategory::Document},
        {QStringLiteral("MATERIAL"), BomCategory::Material},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum BomCategory").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

BomCategory BomCategoryParser::result()
{
    return m_result;
}

}