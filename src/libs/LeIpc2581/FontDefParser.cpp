#include "FontDefParser.h"

#include "FontDefEmbeddedParser.h"
#include "FontDefExternalParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

FontDefParser::FontDefParser(
    FontDefEmbeddedParser *&_fontDefEmbeddedParser
    , FontDefExternalParser *&_fontDefExternalParser
):    m_fontDefEmbeddedParser(_fontDefEmbeddedParser)
    , m_fontDefExternalParser(_fontDefExternalParser)
{

}

bool FontDefParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("FontDefEmbedded"))
    {
        if(m_fontDefEmbeddedParser->parse(reader))
        {
            m_result = m_fontDefEmbeddedParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("FontDefExternal"))
    {
        if(m_fontDefExternalParser->parse(reader))
        {
            m_result = m_fontDefExternalParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"FontDef\"").arg(reader->name().toString()));
    return false;
}

FontDef *FontDefParser::result()
{
    return m_result;
}

bool FontDefParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("FontDefEmbedded"))
        return true;
    if (name == QStringLiteral("FontDefExternal"))
        return true;
    return false;
}

}