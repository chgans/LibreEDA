
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "BadBoardMark.h"

namespace Ipc2581b
{

class LocationParser;
class StandardShapeParser;
class XformParser;

class BadBoardMarkParser
{
public:
    BadBoardMarkParser(
            XformParser*&
            , LocationParser*&
            , StandardShapeParser*&
            );

    bool parse(QXmlStreamReader *reader);
    BadBoardMark *result();

private:
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    StandardShapeParser *&m_standardShapeParser;
    QScopedPointer<BadBoardMark> m_result;
};

}