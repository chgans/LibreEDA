
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Backdrill.h"

namespace Ipc2581b
{

class QStringParser;
class PropertyParser;
class BackdrillListParser;

class BackdrillParser
{
public:
    BackdrillParser(
            BackdrillListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Backdrill *result();

private:
    BackdrillListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Backdrill> m_result;
};

}