
#pragma once

#include "Feature.h"

namespace Ipc2581b
{

class UserPrimitiveRef;
class UserPrimitive;

class UserShape: public Feature
{
public:
	virtual ~UserShape() {}

    enum class UserShapeType
    {
        UserPrimitiveRef
        ,UserPrimitive
    };

    virtual UserShapeType userShapeType() const = 0;

    inline bool isUserPrimitiveRef() const
    {
        return userShapeType() == UserShapeType::UserPrimitiveRef;
    }

    inline UserPrimitiveRef *toUserPrimitiveRef()
    {
        return reinterpret_cast<UserPrimitiveRef*>(this);
    }

    inline const UserPrimitiveRef *toUserPrimitiveRef() const
    {
        return reinterpret_cast<const UserPrimitiveRef*>(this);
    }

    inline bool isUserPrimitive() const
    {
        return userShapeType() == UserShapeType::UserPrimitive;
    }

    inline UserPrimitive *toUserPrimitive()
    {
        return reinterpret_cast<UserPrimitive*>(this);
    }

    inline const UserPrimitive *toUserPrimitive() const
    {
        return reinterpret_cast<const UserPrimitive*>(this);
    }

    virtual FeatureType featureType() const override
    {
        return FeatureType::UserShape;
    }

};

}

// For user convenience
#include "UserPrimitiveRef.h"
#include "UserPrimitive.h"
