#include "MoireParser.h"

#include "XformParser.h"
#include "IntParser.h"
#include "DoubleParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

MoireParser::MoireParser(
    DoubleParser *&_diameterParser
    , DoubleParser *&_ringWidthParser
    , DoubleParser *&_ringGapParser
    , IntParser *&_ringNumberParser
    , DoubleParser *&_lineWidthParser
    , DoubleParser *&_lineLengthParser
    , DoubleParser *&_lineAngleParser
    , XformParser *&_xformParser
):    m_diameterParser(_diameterParser)
    , m_ringWidthParser(_ringWidthParser)
    , m_ringGapParser(_ringGapParser)
    , m_ringNumberParser(_ringNumberParser)
    , m_lineWidthParser(_lineWidthParser)
    , m_lineLengthParser(_lineLengthParser)
    , m_lineAngleParser(_lineAngleParser)
    , m_xformParser(_xformParser)
{

}

bool MoireParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Moire());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("diameter")))
    {
        data = reader->attributes().value(QStringLiteral("diameter"));
        if (!m_diameterParser->parse(reader, data))
            return false;
        m_result->diameter = m_diameterParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("diameter: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("ringWidth")))
    {
        data = reader->attributes().value(QStringLiteral("ringWidth"));
        if (!m_ringWidthParser->parse(reader, data))
            return false;
        m_result->ringWidth = m_ringWidthParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("ringWidth: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("ringGap")))
    {
        data = reader->attributes().value(QStringLiteral("ringGap"));
        if (!m_ringGapParser->parse(reader, data))
            return false;
        m_result->ringGap = m_ringGapParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("ringGap: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("ringNumber")))
    {
        data = reader->attributes().value(QStringLiteral("ringNumber"));
        if (!m_ringNumberParser->parse(reader, data))
            return false;
        m_result->ringNumber = m_ringNumberParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("ringNumber: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lineWidth")))
    {
        data = reader->attributes().value(QStringLiteral("lineWidth"));
        if (!m_lineWidthParser->parse(reader, data))
            return false;
        m_result->lineWidth = m_lineWidthParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lineWidth: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lineLength")))
    {
        data = reader->attributes().value(QStringLiteral("lineLength"));
        if (!m_lineLengthParser->parse(reader, data))
            return false;
        m_result->lineLength = m_lineLengthParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lineLength: Attribute is required"));
        return false;
    }
    if (reader->attributes().hasAttribute(QStringLiteral("lineAngle")))
    {
        data = reader->attributes().value(QStringLiteral("lineAngle"));
        if (!m_lineAngleParser->parse(reader, data))
            return false;
        m_result->lineAngle = m_lineAngleParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("lineAngle: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Moire *MoireParser::result()
{
    return m_result.take();
}

}