
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "EntryStandard.h"

namespace Ipc2581b
{

class StandardPrimitiveParser;
class QStringParser;

class EntryStandardParser
{
public:
    EntryStandardParser(
            QStringParser*&
            , StandardPrimitiveParser*&
            );

    bool parse(QXmlStreamReader *reader);
    EntryStandard *result();

private:
    QStringParser *&m_idParser;
    StandardPrimitiveParser *&m_standardPrimitiveParser;
    QScopedPointer<EntryStandard> m_result;
};

}