
#pragma once

namespace Ipc2581b
{

enum class ConductorList
{
    FinishedHeight,
    SurfaceRoughnessUpfacing,
    SurfaceRoughnessDownfacing,
    Other,
    SurfaceRoughnessTreated,
    Conductivity,
    EtchFactor,
};

}