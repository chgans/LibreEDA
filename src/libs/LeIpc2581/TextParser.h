
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Text.h"

namespace Ipc2581b
{

class ColorGroupParser;
class BoundingBoxParser;
class XformParser;
class QStringParser;
class FontRefParser;
class IntParser;

class TextParser
{
public:
    TextParser(
            QStringParser*&
            , IntParser*&
            , XformParser*&
            , BoundingBoxParser*&
            , FontRefParser*&
            , ColorGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Text *result();

private:
    QStringParser *&m_textStringParser;
    IntParser *&m_fontSizeParser;
    XformParser *&m_xformParser;
    BoundingBoxParser *&m_boundingBoxParser;
    FontRefParser *&m_fontRefParser;
    ColorGroupParser *&m_colorGroupParser;
    QScopedPointer<Text> m_result;
};

}