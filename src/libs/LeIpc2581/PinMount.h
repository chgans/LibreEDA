
#pragma once

namespace Ipc2581b
{

enum class PinMount
{
    Nonboard,
    Pressfit,
    Hole,
    SurfaceMountPin,
    ThroughHoleHole,
    SurfaceMountPad,
    ThroughHolePin,
    Undefined,
};

}