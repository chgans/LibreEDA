
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "HistoryRecord.h"

namespace Ipc2581b
{

class ChangeRecParser;
class FileRevisionParser;
class QStringParser;

class HistoryRecordParser
{
public:
    HistoryRecordParser(
            QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , QStringParser*&
            , FileRevisionParser*&
            , ChangeRecParser*&
            );

    bool parse(QXmlStreamReader *reader);
    HistoryRecord *result();

private:
    QStringParser *&m_numberParser;
    QStringParser *&m_originationParser;
    QStringParser *&m_softwareParser;
    QStringParser *&m_lastChangeParser;
    QStringParser *&m_externalConfigurationEntryParser;
    FileRevisionParser *&m_fileRevisionParser;
    ChangeRecParser *&m_changeRecParser;
    QScopedPointer<HistoryRecord> m_result;
};

}