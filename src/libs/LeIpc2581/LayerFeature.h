
#pragma once

#include <QList>
#include "Optional.h"


#include "Set.h"
#include "QString.h"

namespace Ipc2581b
{

class LayerFeature
{
public:
	virtual ~LayerFeature() {}

    QString layerRef;
    QList<Set*> setList;

};

}