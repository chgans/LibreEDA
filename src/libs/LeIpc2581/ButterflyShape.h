
#pragma once

namespace Ipc2581b
{

enum class ButterflyShape
{
    Round,
    Square,
};

}