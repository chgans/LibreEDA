
#pragma once

#include <QList>
#include "Optional.h"

#include "StandardPrimitive.h"

#include "LineDescGroup.h"
#include "Bool.h"
#include "FillDescGroup.h"
#include "Xform.h"
#include "Double.h"

namespace Ipc2581b
{

class RectCham: public StandardPrimitive
{
public:
	virtual ~RectCham() {}

    Double width;
    Double height;
    Double chamfer;
    Optional<Bool> upperRightOptional;
    Optional<Bool> upperLeftOptional;
    Optional<Bool> lowerRightOptional;
    Optional<Bool> lowerLeftOptional;
    Optional<Xform*> xformOptional;
    Optional<LineDescGroup*> lineDescGroupOptional;
    Optional<FillDescGroup*> fillDescGroupOptional;

    virtual StandardPrimitiveType standardPrimitiveType() const override
    {
        return StandardPrimitiveType::RectCham;
    }
};

}