#include "StandardShapeParser.h"

#include "StandardPrimitiveParser.h"
#include "StandardPrimitiveRefParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

StandardShapeParser::StandardShapeParser(
    StandardPrimitiveParser *&_standardPrimitiveParser
    , StandardPrimitiveRefParser *&_standardPrimitiveRefParser
):    m_standardPrimitiveParser(_standardPrimitiveParser)
    , m_standardPrimitiveRefParser(_standardPrimitiveRefParser)
{

}

bool StandardShapeParser::parse(QXmlStreamReader *reader)
{
    if (m_standardPrimitiveParser->isSubstitution(reader->name()))
    {
        if(m_standardPrimitiveParser->parse(reader))
        {
            m_result = m_standardPrimitiveParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("StandardPrimitiveRef"))
    {
        if(m_standardPrimitiveRefParser->parse(reader))
        {
            m_result = m_standardPrimitiveRefParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"StandardShape\"").arg(reader->name().toString()));
    return false;
}

StandardShape *StandardShapeParser::result()
{
    return m_result;
}

bool StandardShapeParser::isSubstitution(const QStringRef &name) const
{
    if (m_standardPrimitiveParser->isSubstitution(name))
        return true;
    if (name == QStringLiteral("StandardPrimitiveRef"))
        return true;
    return false;
}

}