#include "BackdrillListParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

BackdrillListParser::BackdrillListParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool BackdrillListParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, BackdrillList> map =
    {
        {QStringLiteral("MUST_NOT_CUT_LAYER"), BackdrillList::MustNotCutLayer},
        {QStringLiteral("START_LAYER"), BackdrillList::StartLayer},
        {QStringLiteral("MAX_STUB_LENGTH"), BackdrillList::MaxStubLength},
        {QStringLiteral("OTHER"), BackdrillList::Other},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum BackdrillList").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

BackdrillList BackdrillListParser::result()
{
    return m_result;
}

}