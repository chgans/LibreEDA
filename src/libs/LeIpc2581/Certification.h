
#pragma once

#include <QList>
#include "Optional.h"


#include "CertificationStatus.h"
#include "CertificationCategory.h"

namespace Ipc2581b
{

class Certification
{
public:
	virtual ~Certification() {}

    Optional<CertificationStatus> certificationStatusOptional;
    Optional<CertificationStatus> certificationOptional;
    Optional<CertificationCategory> certificationCategoryOptional;

};

}