
#pragma once

#include <QList>
#include "Optional.h"

#include "ZAxisDim.h"

#include "Double.h"

namespace Ipc2581b
{

class MaterialCut: public ZAxisDim
{
public:
	virtual ~MaterialCut() {}

    Double depth;
    Double plusTol;
    Double minusTol;

    virtual ZAxisDimType zAxisDimType() const override
    {
        return ZAxisDimType::MaterialCut;
    }
};

}