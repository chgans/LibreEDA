
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "RefDes.h"

namespace Ipc2581b
{

class BoolParser;
class QStringParser;

class RefDesParser
{
public:
    RefDesParser(
            QStringParser*&
            , QStringParser*&
            , BoolParser*&
            , QStringParser*&
            );

    bool parse(QXmlStreamReader *reader);
    RefDes *result();

private:
    QStringParser *&m_nameParser;
    QStringParser *&m_packageRefParser;
    BoolParser *&m_populateParser;
    QStringParser *&m_layerRefParser;
    QScopedPointer<RefDes> m_result;
};

}