
#pragma once

#include <QList>
#include "Optional.h"


#include "Bool.h"
#include "Double.h"
#include "Int.h"
#include "QString.h"

namespace Ipc2581b
{

class StepRepeat
{
public:
	virtual ~StepRepeat() {}

    QString stepRef;
    Double x;
    Double y;
    Int nx;
    Int ny;
    Double dx;
    Double dy;
    Double angle;
    Bool mirror;

};

}