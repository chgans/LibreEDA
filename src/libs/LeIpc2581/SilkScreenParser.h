
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "SilkScreen.h"

namespace Ipc2581b
{

class OutlineParser;
class MarkingParser;

class SilkScreenParser
{
public:
    SilkScreenParser(
            OutlineParser*&
            , MarkingParser*&
            );

    bool parse(QXmlStreamReader *reader);
    SilkScreen *result();

private:
    OutlineParser *&m_outlineParser;
    MarkingParser *&m_markingParser;
    QScopedPointer<SilkScreen> m_result;
};

}