#include "CADDataLayerRefParser.h"

#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

CADDataLayerRefParser::CADDataLayerRefParser(
    QStringParser *&_layerIdParser
):    m_layerIdParser(_layerIdParser)
{

}

bool CADDataLayerRefParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new CADDataLayerRef());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("layerId")))
    {
        data = reader->attributes().value(QStringLiteral("layerId"));
        if (!m_layerIdParser->parse(reader, data))
            return false;
        m_result->layerId = m_layerIdParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("layerId: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

CADDataLayerRef *CADDataLayerRefParser::result()
{
    return m_result.take();
}

}