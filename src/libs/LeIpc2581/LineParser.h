
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Line.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class DoubleParser;

class LineParser
{
public:
    LineParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , LineDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Line *result();

private:
    DoubleParser *&m_startXParser;
    DoubleParser *&m_startYParser;
    DoubleParser *&m_endXParser;
    DoubleParser *&m_endYParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    QScopedPointer<Line> m_result;
};

}