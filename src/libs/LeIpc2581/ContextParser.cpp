#include "ContextParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

ContextParser::ContextParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool ContextParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Context> map =
    {
        {QStringLiteral("DOCUMENTATION"), Context::Documentation},
        {QStringLiteral("BOARDPANEL"), Context::Boardpanel},
        {QStringLiteral("ASSEMBLY"), Context::Assembly},
        {QStringLiteral("COUPON"), Context::Coupon},
        {QStringLiteral("BOARD"), Context::Board},
        {QStringLiteral("ASSEMBLYPALLET"), Context::Assemblypallet},
        {QStringLiteral("TOOLING"), Context::Tooling},
        {QStringLiteral("MISCELLANEOUS"), Context::Miscellaneous},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Context").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Context ContextParser::result()
{
    return m_result;
}

}