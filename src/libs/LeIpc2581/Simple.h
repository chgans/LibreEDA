
#pragma once

#include "UserPrimitive.h"

namespace Ipc2581b
{

class Arc;
class Line;
class Outline;
class Polyline;

class Simple: public UserPrimitive
{
public:
	virtual ~Simple() {}

    enum class SimpleType
    {
        Arc
        ,Line
        ,Outline
        ,Polyline
    };

    virtual SimpleType simpleType() const = 0;

    inline bool isArc() const
    {
        return simpleType() == SimpleType::Arc;
    }

    inline Arc *toArc()
    {
        return reinterpret_cast<Arc*>(this);
    }

    inline const Arc *toArc() const
    {
        return reinterpret_cast<const Arc*>(this);
    }

    inline bool isLine() const
    {
        return simpleType() == SimpleType::Line;
    }

    inline Line *toLine()
    {
        return reinterpret_cast<Line*>(this);
    }

    inline const Line *toLine() const
    {
        return reinterpret_cast<const Line*>(this);
    }

    inline bool isOutline() const
    {
        return simpleType() == SimpleType::Outline;
    }

    inline Outline *toOutline()
    {
        return reinterpret_cast<Outline*>(this);
    }

    inline const Outline *toOutline() const
    {
        return reinterpret_cast<const Outline*>(this);
    }

    inline bool isPolyline() const
    {
        return simpleType() == SimpleType::Polyline;
    }

    inline Polyline *toPolyline()
    {
        return reinterpret_cast<Polyline*>(this);
    }

    inline const Polyline *toPolyline() const
    {
        return reinterpret_cast<const Polyline*>(this);
    }

    virtual UserPrimitiveType userPrimitiveType() const override
    {
        return UserPrimitiveType::Simple;
    }

};

}

// For user convenience
#include "Arc.h"
#include "Line.h"
#include "Outline.h"
#include "Polyline.h"
