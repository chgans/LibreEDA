
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PhyNet.h"

namespace Ipc2581b
{

class PhyNetPointParser;
class QStringParser;

class PhyNetParser
{
public:
    PhyNetParser(
            QStringParser*&
            , PhyNetPointParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PhyNet *result();

private:
    QStringParser *&m_nameParser;
    PhyNetPointParser *&m_phyNetPointParser;
    QScopedPointer<PhyNet> m_result;
};

}