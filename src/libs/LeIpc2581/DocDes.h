
#pragma once

#include <QList>
#include "Optional.h"

#include "BomDes.h"

#include "QString.h"

namespace Ipc2581b
{

class DocDes: public BomDes
{
public:
	virtual ~DocDes() {}

    QString name;
    Optional<QString> layerRefOptional;

    virtual BomDesType bomDesType() const override
    {
        return BomDesType::DocDes;
    }
};

}