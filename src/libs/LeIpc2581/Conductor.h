
#pragma once

#include <QList>
#include "Optional.h"

#include "SpecificationType.h"

#include "Property.h"
#include "ConductorList.h"
#include "QString.h"

namespace Ipc2581b
{

class Conductor: public SpecificationType
{
public:
	virtual ~Conductor() {}

    ConductorList type;
    Optional<QString> commentOptional;
    QList<Property*> propertyList;

    virtual SpecificationTypeType specificationTypeType() const override
    {
        return SpecificationTypeType::Conductor;
    }
};

}