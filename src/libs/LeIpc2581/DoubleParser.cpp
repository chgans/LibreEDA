#include "DoubleParser.h"
#include <QXmlStreamReader>

namespace Ipc2581b
{

DoubleParser::DoubleParser()
{

}

bool DoubleParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    Q_UNUSED(reader);
    bool ok;
    m_result = data.toDouble(&ok);
    if (!ok)
        reader->raiseError(QString("\"%1\": Invalid value for a double").arg(data.toString()));
    return ok;
}

double DoubleParser::result()
{
    return m_result;
}

}