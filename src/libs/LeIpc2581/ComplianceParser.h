
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Compliance.h"

namespace Ipc2581b
{

class QStringParser;
class PropertyParser;
class ComplianceListParser;

class ComplianceParser
{
public:
    ComplianceParser(
            ComplianceListParser*&
            , QStringParser*&
            , PropertyParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Compliance *result();

private:
    ComplianceListParser *&m_typeParser;
    QStringParser *&m_commentParser;
    PropertyParser *&m_propertyParser;
    QScopedPointer<Compliance> m_result;
};

}