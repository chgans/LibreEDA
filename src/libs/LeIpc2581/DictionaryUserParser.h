
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryUser.h"

namespace Ipc2581b
{

class UnitsParser;
class EntryUserParser;

class DictionaryUserParser
{
public:
    DictionaryUserParser(
            UnitsParser*&
            , EntryUserParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryUser *result();

private:
    UnitsParser *&m_unitsParser;
    EntryUserParser *&m_entryUserParser;
    QScopedPointer<DictionaryUser> m_result;
};

}