#include "UserShapeParser.h"

#include "UserPrimitiveParser.h"
#include "UserPrimitiveRefParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

UserShapeParser::UserShapeParser(
    UserPrimitiveParser *&_userPrimitiveParser
    , UserPrimitiveRefParser *&_userPrimitiveRefParser
):    m_userPrimitiveParser(_userPrimitiveParser)
    , m_userPrimitiveRefParser(_userPrimitiveRefParser)
{

}

bool UserShapeParser::parse(QXmlStreamReader *reader)
{
    if (m_userPrimitiveParser->isSubstitution(reader->name()))
    {
        if(m_userPrimitiveParser->parse(reader))
        {
            m_result = m_userPrimitiveParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("UserPrimitiveRef"))
    {
        if(m_userPrimitiveRefParser->parse(reader))
        {
            m_result = m_userPrimitiveRefParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"UserShape\"").arg(reader->name().toString()));
    return false;
}

UserShape *UserShapeParser::result()
{
    return m_result;
}

bool UserShapeParser::isSubstitution(const QStringRef &name) const
{
    if (m_userPrimitiveParser->isSubstitution(name))
        return true;
    if (name == QStringLiteral("UserPrimitiveRef"))
        return true;
    return false;
}

}