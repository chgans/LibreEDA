#include "NetClassParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

NetClassParser::NetClassParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool NetClassParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, NetClass> map =
    {
        {QStringLiteral("GROUND"), NetClass::Ground},
        {QStringLiteral("POWER"), NetClass::Power},
        {QStringLiteral("UNUSED"), NetClass::Unused},
        {QStringLiteral("FIXED"), NetClass::Fixed},
        {QStringLiteral("CLK"), NetClass::Clk},
        {QStringLiteral("SIGNAL"), NetClass::Signal},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum NetClass").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

NetClass NetClassParser::result()
{
    return m_result;
}

}