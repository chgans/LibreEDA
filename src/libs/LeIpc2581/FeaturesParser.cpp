#include "FeaturesParser.h"

#include "LocationParser.h"
#include "XformParser.h"
#include "FeatureParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

FeaturesParser::FeaturesParser(
    XformParser *&_xformParser
    , LocationParser *&_locationParser
    , FeatureParser *&_featureParser
):    m_xformParser(_xformParser)
    , m_locationParser(_locationParser)
    , m_featureParser(_featureParser)
{

}

bool FeaturesParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new Features());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Xform"))
        {
            if (!m_xformParser->parse(reader))
                return false;
            auto result = m_xformParser->result();
            m_result->xformOptional = Optional<Xform*>(result);
        }
        else if (name == QStringLiteral("Location"))
        {
            if (!m_locationParser->parse(reader))
                return false;
            auto result = m_locationParser->result();
            m_result->locationList.append(result);
        }
        else if (m_featureParser->isSubstitution(name))
        {
            if (!m_featureParser->parse(reader))
                return false;
            auto result = m_featureParser->result();
            m_result->feature = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

Features *FeaturesParser::result()
{
    return m_result.take();
}

}