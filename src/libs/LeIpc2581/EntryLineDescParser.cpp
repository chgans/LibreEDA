#include "EntryLineDescParser.h"

#include "LineDescParser.h"
#include "QStringParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

EntryLineDescParser::EntryLineDescParser(
    QStringParser *&_idParser
    , LineDescParser *&_lineDescParser
):    m_idParser(_idParser)
    , m_lineDescParser(_lineDescParser)
{

}

bool EntryLineDescParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new EntryLineDesc());

    /* Attributes */

    QStringRef data;
    if (reader->attributes().hasAttribute(QStringLiteral("id")))
    {
        data = reader->attributes().value(QStringLiteral("id"));
        if (!m_idParser->parse(reader, data))
            return false;
        m_result->id = m_idParser->result();
    }
    else
    {
        reader->raiseError(QStringLiteral("id: Attribute is required"));
        return false;
    }

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("LineDesc"))
        {
            if (!m_lineDescParser->parse(reader))
                return false;
            auto result = m_lineDescParser->result();
            m_result->lineDesc = result;
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

EntryLineDesc *EntryLineDescParser::result()
{
    return m_result.take();
}

}