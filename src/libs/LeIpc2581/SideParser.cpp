#include "SideParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

SideParser::SideParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool SideParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, Side> map =
    {
        {QStringLiteral("TOP"), Side::Top},
        {QStringLiteral("INTERNAL"), Side::Internal},
        {QStringLiteral("ALL"), Side::All},
        {QStringLiteral("BOTH"), Side::Both},
        {QStringLiteral("NONE"), Side::None},
        {QStringLiteral("BOTTOM"), Side::Bottom},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum Side").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

Side SideParser::result()
{
    return m_result;
}

}