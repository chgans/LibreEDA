
#pragma once

#include <QMetaType>

#include "LeIpc2581/Contour.h"
Q_DECLARE_METATYPE(const Ipc2581b::Contour*)

#include "LeIpc2581/Feature.h"
Q_DECLARE_METATYPE(const Ipc2581b::Feature*)

#include "LeIpc2581/Features.h"
Q_DECLARE_METATYPE(const Ipc2581b::Features*)

#include "LeIpc2581/Fiducial.h"
Q_DECLARE_METATYPE(const Ipc2581b::Fiducial*)

#include "LeIpc2581/Hole.h"
Q_DECLARE_METATYPE(const Ipc2581b::Hole*)

#include "LeIpc2581/Marking.h"
Q_DECLARE_METATYPE(const Ipc2581b::Marking*)

#include "LeIpc2581/Outline.h"
Q_DECLARE_METATYPE(const Ipc2581b::Outline*)

#include "LeIpc2581/Pad.h"
Q_DECLARE_METATYPE(const Ipc2581b::Pad*)

#include "LeIpc2581/PadstackPadDef.h"
Q_DECLARE_METATYPE(const Ipc2581b::PadstackPadDef*)

#include "LeIpc2581/PadstackHoleDef.h"
Q_DECLARE_METATYPE(const Ipc2581b::PadstackHoleDef*)

#include "LeIpc2581/Pin.h"
Q_DECLARE_METATYPE(const Ipc2581b::Pin*)

#include "LeIpc2581/SlotCavity.h"
Q_DECLARE_METATYPE(const Ipc2581b::SlotCavity*)

#include "LeIpc2581/Target.h"
Q_DECLARE_METATYPE(const Ipc2581b::Target*)
