
#pragma once

namespace Ipc2581b
{

enum class MarkingUsage
{
    AttributeGraphics,
    Partname,
    None,
    Refdes,
    Target,
    PinOne,
    PolarityMarking,
};

}