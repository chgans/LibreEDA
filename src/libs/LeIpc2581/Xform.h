
#pragma once

#include <QList>
#include "Optional.h"


#include "Bool.h"
#include "Double.h"

namespace Ipc2581b
{

class Xform
{
public:
	virtual ~Xform() {}

    Optional<Double> xOffsetOptional;
    Optional<Double> yOffsetOptional;
    Optional<Double> rotationOptional;
    Optional<Bool> mirrorOptional;
    Optional<Double> scaleOptional;

};

}