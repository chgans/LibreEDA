
#pragma once

#include <QString>

class QXmlStreamReader;

#include "DonutShape.h"

namespace Ipc2581b
{

class DonutShapeParser
{
public:
    DonutShapeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    DonutShape result();

private:
    DonutShape m_result;
};

}