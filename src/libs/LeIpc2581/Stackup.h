
#pragma once

#include <QList>
#include "Optional.h"


#include "WhereMeasured.h"
#include "IdRef.h"
#include "QString.h"
#include "MatDes.h"
#include "StackupGroup.h"
#include "Double.h"

namespace Ipc2581b
{

class Stackup
{
public:
	virtual ~Stackup() {}

    Optional<QString> nameOptional;
    Double overallThickness;
    Double tolPlus;
    Double tolMinus;
    WhereMeasured whereMeasured;
    Optional<QString> commentOptional;
    Optional<MatDes*> matDesOptional;
    QList<IdRef*> specRefList;
    QList<StackupGroup*> stackupGroupList;

};

}