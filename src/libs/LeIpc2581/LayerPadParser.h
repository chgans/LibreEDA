
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "LayerPad.h"

namespace Ipc2581b
{


class LayerPadParser
{
public:
    LayerPadParser(
            );

    bool parse(QXmlStreamReader *reader);
    LayerPad *result();

private:
    QScopedPointer<LayerPad> m_result;
};

}