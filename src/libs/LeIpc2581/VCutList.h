
#pragma once

namespace Ipc2581b
{

enum class VCutList
{
    Other,
    Offset,
    ThicknessRemaining,
    Angle,
};

}