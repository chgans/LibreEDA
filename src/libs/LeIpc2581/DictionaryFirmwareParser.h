
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryFirmware.h"

namespace Ipc2581b
{


class DictionaryFirmwareParser
{
public:
    DictionaryFirmwareParser(
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryFirmware *result();

private:
    QScopedPointer<DictionaryFirmware> m_result;
};

}