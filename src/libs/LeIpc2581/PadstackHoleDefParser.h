
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "PadstackHoleDef.h"

namespace Ipc2581b
{

class DoubleParser;
class PlatingStatusParser;
class QStringParser;

class PadstackHoleDefParser
{
public:
    PadstackHoleDefParser(
            QStringParser*&
            , DoubleParser*&
            , PlatingStatusParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            );

    bool parse(QXmlStreamReader *reader);
    PadstackHoleDef *result();

private:
    QStringParser *&m_nameParser;
    DoubleParser *&m_diameterParser;
    PlatingStatusParser *&m_platingStatusParser;
    DoubleParser *&m_plusTolParser;
    DoubleParser *&m_minusTolParser;
    DoubleParser *&m_xParser;
    DoubleParser *&m_yParser;
    QScopedPointer<PadstackHoleDef> m_result;
};

}