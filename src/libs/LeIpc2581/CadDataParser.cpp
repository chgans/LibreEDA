#include "CadDataParser.h"

#include "LayerParser.h"
#include "StackupParser.h"
#include "StepParser.h"

#include <QXmlStreamReader>
#include <QString>

namespace Ipc2581b
{

CadDataParser::CadDataParser(
    LayerParser *&_layerParser
    , StackupParser *&_stackupParser
    , StepParser *&_stepParser
):    m_layerParser(_layerParser)
    , m_stackupParser(_stackupParser)
    , m_stepParser(_stepParser)
{

}

bool CadDataParser::parse(QXmlStreamReader *reader)
{
    /* Pre */

    m_result.reset(new CadData());

    /* Attributes */

    QStringRef data;

    /* Elements */

    while (reader->readNextStartElement())
    {
        auto name = reader->name();
        if (name == QStringLiteral("Layer"))
        {
            if (!m_layerParser->parse(reader))
                return false;
            auto result = m_layerParser->result();
            m_result->layerList.append(result);
        }
        else if (name == QStringLiteral("Stackup"))
        {
            if (!m_stackupParser->parse(reader))
                return false;
            auto result = m_stackupParser->result();
            m_result->stackupList.append(result);
        }
        else if (name == QStringLiteral("Step"))
        {
            if (!m_stepParser->parse(reader))
                return false;
            auto result = m_stepParser->result();
            m_result->stepList.append(result);
        }
        else
            reader->skipCurrentElement();
    }

    /* Post */

    // TODO: Check multiplicity of elements/attributes

    return true;
}

CadData *CadDataParser::result()
{
    return m_result.take();
}

}