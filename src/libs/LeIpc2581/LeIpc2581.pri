INCLUDEPATH += $$PWD/..

win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/src/lib/LeIpc2581/release/ -lLeIpc2581
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/src/lib/LeIpc2581/debug/ -lLeIpc2581
else:unix: LIBS += -L$$top_builddir/src/lib/LeIpc2581/ -lLeIpc2581

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc2581/release/libLeIpc2581.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc2581/debug/libLeIpc2581.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc2581/release/LeIpc2581.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc2581/debug/LeIpc2581.lib
else:unix: PRE_TARGETDEPS += $$top_builddir/src/lib/LeIpc2581/libLeIpc2581.a
