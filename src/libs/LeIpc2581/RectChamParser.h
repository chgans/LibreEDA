
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "RectCham.h"

namespace Ipc2581b
{

class LineDescGroupParser;
class BoolParser;
class FillDescGroupParser;
class XformParser;
class DoubleParser;

class RectChamParser
{
public:
    RectChamParser(
            DoubleParser*&
            , DoubleParser*&
            , DoubleParser*&
            , BoolParser*&
            , BoolParser*&
            , BoolParser*&
            , BoolParser*&
            , XformParser*&
            , LineDescGroupParser*&
            , FillDescGroupParser*&
            );

    bool parse(QXmlStreamReader *reader);
    RectCham *result();

private:
    DoubleParser *&m_widthParser;
    DoubleParser *&m_heightParser;
    DoubleParser *&m_chamferParser;
    BoolParser *&m_upperRightParser;
    BoolParser *&m_upperLeftParser;
    BoolParser *&m_lowerRightParser;
    BoolParser *&m_lowerLeftParser;
    XformParser *&m_xformParser;
    LineDescGroupParser *&m_lineDescGroupParser;
    FillDescGroupParser *&m_fillDescGroupParser;
    QScopedPointer<RectCham> m_result;
};

}