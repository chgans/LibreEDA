#include "LineDescGroupParser.h"

#include "LineDescParser.h"
#include "LineDescRefParser.h"

#include <QXmlStreamReader>
#include <QMap>
#include <QSet>

namespace Ipc2581b
{

LineDescGroupParser::LineDescGroupParser(
    LineDescParser *&_lineDescParser
    , LineDescRefParser *&_lineDescRefParser
):    m_lineDescParser(_lineDescParser)
    , m_lineDescRefParser(_lineDescRefParser)
{

}

bool LineDescGroupParser::parse(QXmlStreamReader *reader)
{
    if (reader->name() == QStringLiteral("LineDesc"))
    {
        if(m_lineDescParser->parse(reader))
        {
            m_result = m_lineDescParser->result();
            return true;
        }
        else
            return false;
    }
    if (reader->name() == QStringLiteral("LineDescRef"))
    {
        if(m_lineDescRefParser->parse(reader))
        {
            m_result = m_lineDescRefParser->result();
            return true;
        }
        else
            return false;
    }
    reader->raiseError(QStringLiteral("\"%1\": not a valid substitution for goup \"LineDescGroup\"").arg(reader->name().toString()));
    return false;
}

LineDescGroup *LineDescGroupParser::result()
{
    return m_result;
}

bool LineDescGroupParser::isSubstitution(const QStringRef &name) const
{
    if (name == QStringLiteral("LineDesc"))
        return true;
    if (name == QStringLiteral("LineDescRef"))
        return true;
    return false;
}

}