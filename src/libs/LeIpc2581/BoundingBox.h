
#pragma once

#include <QList>
#include "Optional.h"


#include "Double.h"

namespace Ipc2581b
{

class BoundingBox
{
public:
	virtual ~BoundingBox() {}

    Double lowerLeftX;
    Double lowerLeftY;
    Double upperRightX;
    Double upperRightY;

};

}