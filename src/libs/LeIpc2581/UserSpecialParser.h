
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "UserSpecial.h"

namespace Ipc2581b
{

class FeatureParser;

class UserSpecialParser
{
public:
    UserSpecialParser(
            FeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    UserSpecial *result();

private:
    FeatureParser *&m_featureParser;
    QScopedPointer<UserSpecial> m_result;
};

}