
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "DictionaryLineDesc.h"

namespace Ipc2581b
{

class UnitsParser;
class EntryLineDescParser;

class DictionaryLineDescParser
{
public:
    DictionaryLineDescParser(
            UnitsParser*&
            , EntryLineDescParser*&
            );

    bool parse(QXmlStreamReader *reader);
    DictionaryLineDesc *result();

private:
    UnitsParser *&m_unitsParser;
    EntryLineDescParser *&m_entryLineDescParser;
    QScopedPointer<DictionaryLineDesc> m_result;
};

}