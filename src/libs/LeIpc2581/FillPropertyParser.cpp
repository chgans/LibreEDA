#include "FillPropertyParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

FillPropertyParser::FillPropertyParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool FillPropertyParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, FillProperty> map =
    {
        {QStringLiteral("FILL"), FillProperty::Fill},
        {QStringLiteral("HOLLOW"), FillProperty::Hollow},
        {QStringLiteral("HATCH"), FillProperty::Hatch},
        {QStringLiteral("VOID"), FillProperty::Void},
        {QStringLiteral("MESH"), FillProperty::Mesh},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum FillProperty").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

FillProperty FillPropertyParser::result()
{
    return m_result;
}

}