
#pragma once

#include <QScopedPointer>
class QXmlStreamReader;

#include "Marking.h"

namespace Ipc2581b
{

class MarkingUsageParser;
class LocationParser;
class XformParser;
class FeatureParser;

class MarkingParser
{
public:
    MarkingParser(
            MarkingUsageParser*&
            , XformParser*&
            , LocationParser*&
            , FeatureParser*&
            );

    bool parse(QXmlStreamReader *reader);
    Marking *result();

private:
    MarkingUsageParser *&m_markingUsageParser;
    XformParser *&m_xformParser;
    LocationParser *&m_locationParser;
    FeatureParser *&m_featureParser;
    QScopedPointer<Marking> m_result;
};

}