
#pragma once

#include <QString>
class QXmlStreamReader;

#include "ColorGroup.h"

namespace Ipc2581b
{

class ColorParser;
class ColorRefParser;

class ColorGroupParser
{
public:
    ColorGroupParser(
            ColorParser*&
            , ColorRefParser*&
            );

    bool isSubstitution(const QStringRef &name) const;
    bool parse(QXmlStreamReader *reader);
    ColorGroup *result();

private:
    ColorParser *&m_colorParser;
    ColorRefParser *&m_colorRefParser;
    ColorGroup *m_result;
};

}