#include "PadUseParser.h"

#include <QXmlStreamReader>
#include <QMap>

namespace Ipc2581b
{

PadUseParser::PadUseParser()
{

}

// TODO: investigate perf QMap vs QList vs QstringRef::toString()
bool PadUseParser::parse(QXmlStreamReader *reader, const QStringRef &data)
{
    static const QMap<QString, PadUse> map =
    {
        {QStringLiteral("OTHER"), PadUse::Other},
        {QStringLiteral("REGULAR"), PadUse::Regular},
        {QStringLiteral("THERMAL"), PadUse::Thermal},
        {QStringLiteral("ANTIPAD"), PadUse::Antipad},
    };
    if (!map.contains(data.toString()))
    {
        reader->raiseError(QStringLiteral("\"%1\": invalid value for enum PadUse").arg(data.toString()));
        return false;
    }
    m_result = map.value(data.toString());
    return true;
}

PadUse PadUseParser::result()
{
    return m_result;
}

}