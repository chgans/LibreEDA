
#pragma once


namespace Ipc2581b
{

class PolyStepSegment;
class PolyStepCurve;

class PolyStep
{
public:
	virtual ~PolyStep() {}

    enum class PolyStepType
    {
        PolyStepSegment
        ,PolyStepCurve
    };

    virtual PolyStepType polyStepType() const = 0;

    inline bool isPolyStepSegment() const
    {
        return polyStepType() == PolyStepType::PolyStepSegment;
    }

    inline PolyStepSegment *toPolyStepSegment()
    {
        return reinterpret_cast<PolyStepSegment*>(this);
    }

    inline const PolyStepSegment *toPolyStepSegment() const
    {
        return reinterpret_cast<const PolyStepSegment*>(this);
    }

    inline bool isPolyStepCurve() const
    {
        return polyStepType() == PolyStepType::PolyStepCurve;
    }

    inline PolyStepCurve *toPolyStepCurve()
    {
        return reinterpret_cast<PolyStepCurve*>(this);
    }

    inline const PolyStepCurve *toPolyStepCurve() const
    {
        return reinterpret_cast<const PolyStepCurve*>(this);
    }


};

}

// For user convenience
#include "PolyStepSegment.h"
#include "PolyStepCurve.h"
