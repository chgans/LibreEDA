
#pragma once

#include <QString>

class QXmlStreamReader;

#include "VCutList.h"

namespace Ipc2581b
{

class VCutListParser
{
public:
    VCutListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    VCutList result();

private:
    VCutList m_result;
};

}