
#pragma once

#include <QString>

class QXmlStreamReader;

#include "IsoCode.h"

namespace Ipc2581b
{

class IsoCodeParser
{
public:
    IsoCodeParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    IsoCode result();

private:
    IsoCode m_result;
};

}