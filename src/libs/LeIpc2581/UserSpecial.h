
#pragma once

#include <QList>
#include "Optional.h"

#include "UserPrimitive.h"

#include "Feature.h"

namespace Ipc2581b
{

class UserSpecial: public UserPrimitive
{
public:
	virtual ~UserSpecial() {}

    QList<Feature*> featureList;

    virtual UserPrimitiveType userPrimitiveType() const override
    {
        return UserPrimitiveType::UserSpecial;
    }
};

}