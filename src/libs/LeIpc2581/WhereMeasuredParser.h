
#pragma once

#include <QString>

class QXmlStreamReader;

#include "WhereMeasured.h"

namespace Ipc2581b
{

class WhereMeasuredParser
{
public:
    WhereMeasuredParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    WhereMeasured result();

private:
    WhereMeasured m_result;
};

}