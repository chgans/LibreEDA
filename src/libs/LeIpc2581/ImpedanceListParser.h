
#pragma once

#include <QString>

class QXmlStreamReader;

#include "ImpedanceList.h"

namespace Ipc2581b
{

class ImpedanceListParser
{
public:
    ImpedanceListParser();

    bool parse(QXmlStreamReader *reader, const QStringRef &data);
    ImpedanceList result();

private:
    ImpedanceList m_result;
};

}