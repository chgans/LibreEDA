
#pragma once

#include <QList>
#include "Optional.h"


#include "Simple.h"
#include "Double.h"
#include "PlatingStatus.h"
#include "ZAxisDim.h"
#include "QString.h"

namespace Ipc2581b
{

class SlotCavity
{
public:
	virtual ~SlotCavity() {}

    QString name;
    PlatingStatus platingStatus;
    Double plusTol;
    Double minusTol;
    QList<Simple*> simpleList;
    Optional<ZAxisDim*> z_AxisDimOptional;

};

}