import qbs

Project {
    name: "Libraries"
    references: [
        "core", // TODO: Split into LeCore and LeGui
	"LeDocumentObject",
	"LeGerber", // TODO: move GUI part out?
        "OldGraphicsView",
        "LeGraphicsView",
        "LeIpc2581",
        "LeIpc7351",
        "utils", // TODO: Move to LeCore/LeGui for simplifaction's sake
        "xdl" // TODO: Rename LeXdl
    ]
}
